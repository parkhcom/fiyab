<?php
namespace Products\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 
 

class  ApplicationShopTable //implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_shop', $dbAdapter);
    } 
    
    public function fetch(array $where = null)
    {
        $select = new Select('application_shop');
        if ($where) {
            $select->where($where);
        } 
        $select->join('application_shop_owners', 'application_shop_owners.shop_id = application_shop.id', array('user_id'),'left');
        $select->join('application_users', 'application_users.id = application_shop_owners.user_id', array('username'),'left');
        return $this->tableGateway->selectWith($select)->toArray();
    }


    public function add(array $data)
    { 
        $this->tableGateway->insert([ 
            'name' => $data['name'], 
            'address' => $data['address'], 
            'telf' => $data['telf'],
            'tels' => $data['tels'],
            'url' => $data['url'],
            'fax' => $data['fax'] 
        ]);
        return $this->tableGateway->getLastInsertValue();
    }

    public function update(int $id, array $data)
    {
        if ($id > 0 && is_numeric($id)) { 
            return $this->tableGateway->update([
                'name' => $data['name'],
                'address' => $data['address'],
                'telf' => $data['telf'],
                'tels' => $data['tels'],
                'url' => $data['url'],
                'fax' => $data['fax'] 
            ], [
                'id' => $id
            ]);
        }
    }

    public function delete(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'id' => $id
            ]);
        }
    }
}