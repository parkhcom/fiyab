<?php
namespace Products\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 
use Application\Interfaces\CrudInterface;

class ApplicationProductListTable //implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_product_list', $dbAdapter);
    }

    public function fetch(array $where = null)
    {
        $select = new Select('application_product_list');
        if ($where) {
            $select->where($where);
        } 
        return $this->tableGateway->selectWith($select)->toArray();
    }
    public function fetchBYname($pname)
    {
        $select = new Select('application_product_list');
        $select->where->equalTo('pname' , $pname  ); 
        return $this->tableGateway->selectWith($select)->current();
    }


    public function add(array $data)
    {  
        $this->tableGateway->insert([
            'pname' => $data['pname'],   
            'attribute_parrent_id' => $data['attribute_parrent_id'],
            'active' => 1,
            'group_id' => $data['group_id'], 
            'meta_description' => $data['meta_description'],
            'meta_keyword' => $data['meta_keyword'] 
        ]);
        return $this->tableGateway->getLastInsertValue();
    }

    public function update(int $id, array $data)
    {
        if ($id > 0 && is_numeric($id)) { 
            return $this->tableGateway->update([
                'pname' => $data['pname'], 
                'attribute_parrent_id' => $data['attribute_parrent_id'],
                'active' => $data['active'],
                'group_id' => $data['group_id'], 
                'active' => $data['active'], 
                'meta_description' => $data['meta_description'],
                'meta_keyword' => $data['meta_keyword'] 
            ], [
                'id' => $id
            ]);
        }
    }

    public function actination(int $id, $status)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'active' => $status
            ]);
        }
    }
    public function delete(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'id' => $id
            ]);
        }
    }
}