<?php
namespace Products\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 
use Application\Interfaces\CrudInterface;

class  ApplicationShopOwnersTable //implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_shop_owners', $dbAdapter);
    } 
    
    public function fetch(array $where = null)
    {
        $select = new Select('application_shop_owners');
        if ($where) {
            $select->where($where);
        } 
        return $this->tableGateway->selectWith($select)->toArray();
    }
    public function fetchWithShop(array $where = null)
    {
        $select = new Select('application_shop_owners');
        $select->join('application_shop', 'application_shop.id = application_shop_owners.shop_id', array('name'),'left');
        if ($where) {
            $select->where($where);
        } 
        return $this->tableGateway->selectWith($select)->toArray();
    }


    public function add(array $data)
    { 
        $this->tableGateway->insert([ 
            'user_id' => $data['user_id'],
            'shop_id' => $data['shop_id'], 
        ]);
        return $this->tableGateway->getLastInsertValue();
    }

    public function update(int $id, array $data)
    {
        if ($id > 0 && is_numeric($id)) { 
            return $this->tableGateway->update([
                'user_id' => $data['user_id'],
                'shop_id' => $data['shop_id'], 
            ], [
                'id' => $id
            ]);
        }
    }

    public function deleteByShopId(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'shop_id' => $id
            ]);
        }
    }
    
    public function delete(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'id' => $id
            ]);
        }
    }
}