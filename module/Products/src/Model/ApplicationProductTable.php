<?php
namespace Products\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 
use Application\Interfaces\CrudInterface;

class ApplicationProductTable //implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_product', $dbAdapter);
    }

    public function fetch(array $where = null)
    {
        $select = new Select('application_product');
        if ($where) {
            $select->where($where);
        } 
        return $this->tableGateway->selectWith($select)->toArray();
    }


    public function add(array $data)
    {  
        $this->tableGateway->insert([
            'shop_id' => $data['shop_id'],
            'pname' => $data['name'], 
            'downloadable' => 0, //$data['is_accessory'],
            'is_accessory' => $data['group_ids'],
            'attribute_parrent_id' => $data['attribute_parrent_id'],
            'status' => $data['status'],
            'group_id' => $data['group_id'],
            'pname_id' => $data['pname_id'],
            'meta_description' => $data['meta_description'],
            'meta_keyword' => $data['meta_keyword'] 
        ]);
        return $this->tableGateway->getLastInsertValue();
    }

    public function update(int $id, array $data)
    {
        if ($id > 0 && is_numeric($id)) { 
            return $this->tableGateway->update([
                'shop_id' => $data['shop_id'],
                'pname' => $data['name'],
                'downloadable' => 0, //$data['is_accessory'],
                'is_accessory' => $data['group_ids'],
                'attribute_parrent_id' => $data['attribute_parrent_id'],
                'status' => $data['status'],
                'group_id' => $data['group_id'],
                'pname_id' => $data['pname_id'],
                'meta_description' => $data['meta_description'],
                'meta_keyword' => $data['meta_keyword'] 
            ], [
                'id' => $id
            ]);
        }
    }

    public function delete(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'id' => $id
            ]);
        }
    }
}