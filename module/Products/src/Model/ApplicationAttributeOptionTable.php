<?php
namespace Products\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 
use Zend\Db\Sql\Where;

class ApplicationAttributeOptionTable //implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_attributes_option', $dbAdapter);
    }

    public function fetchById(int $id)
    {
        $select = new Select('application_attributes_option'); 
        $select->where->equalTo('id', $id); 
        return $this->tableGateway->selectWith($select)->curent();
    }
    public function fetch(array $where = null)
    {
        $select = new Select('application_attributes_option');
        if ($where) {
            $select->where($where);
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }
 
    public function fetchByAjaxCall(int $offset, int $limit, string $lang)
    {
        $select = new Select('application_attributes_option');
        $select->offset($offset);
        $select->limit($limit);
        $select->where->equalTo('lang', $lang);
        $select->where->equalTo('status', 1);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchSearchQueries($query, $limit)
    {
        $select = new Select('application_attributes_option');
        $select->where->expression("MATCH(title, content) AGAINST (?)", $query);
        $select->limit($limit);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function add(array $data)
    {  
        $this->tableGateway->insert([
            'attribute_id' => $data['attribute_id'],
            'value' => (int) $data['value'], 
            'label' => $data['label'], 
            'image' => $data['image'] 
        ]);
        return $this->tableGateway->getLastInsertValue();
    }
 
    
    public function update(int $id, array $data)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->update([
                'attribute_id' => $data['attribute_id'],
                'value' => (int) $data['value'],
                'label' => $data['label'],
                'image' => $data['image'] 
            ], [
                'id' => $id
            ]);
        }
    }

    public function delete(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'id' => $id
            ]);
        }
    }
    
    public function deleteAttributeId(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'attribute_id' => $id
            ]);
        }
    }
    
}