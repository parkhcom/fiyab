<?php
namespace Products\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;


class ApplicationProductGroupTable  
{

    public $tableGateway;
    
    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_product_group', $dbAdapter);
    }
    
    
    public function fetch(array $where = null)
    {
        $select = new Select('application_product_group');
        if ($where) {
            $select->where($where);
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }
    public function getGroupName(array $where = null)
    {
        $select = new Select('application_product_group');
        if ($where) {
            $select->where($where);
        }
        return $this->tableGateway->selectWith($select)->current();
    }
    
    public function add(array $data)
    {
        $this->tableGateway->insert([ 
            'name'=> $data['name'],
            'keywords'=> $data['keywords'],
            'description'=> $data['description'],
            'is_parent'=> $data['is_parent'],
            'active'=> 1,
            'parent_id'=> $data['parent_id'], 
            'avatar'=> $data['avatar'],  
            'icon'=> $data['icon'],  
            'order'=> $data['order'],
            'compareable'=> $data['compareable']
        ]);
        return $this->tableGateway->getLastInsertValue();
    }
   
    public function update(int $id, array $data)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->update([  
                'name'=> $data['name'],
                'keywords'=> $data['keywords'],
                'description'=> $data['description'],
                'is_parent'=> $data['is_parent'], 
                'icon'=> $data['icon'], 
                'active'=> $data['active'],
                'avatar'=> $data['avatar'], 
                'parent_id'=> $data['parent_id'],  
                'order'=> $data['order'],
                'compareable'=> $data['compareable']
            ], [
                'id' => $id
            ]);
        }
    }
    
    public function delete(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'id' => $id
            ]);
        }
    }
  
}