<?php
namespace Products\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 

class ApplicationAttributeTable //implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_attributes', $dbAdapter);
    }

    public function fetch(array $where = null)
    {
        $select = new Select('application_attributes');
        if ($where) {
            $select->where($where);
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchByAjaxCall(int $offset, int $limit, string $lang)
    {
        $select = new Select('application_attributes');
        $select->offset($offset);
        $select->limit($limit);
        $select->where->equalTo('lang', $lang);
        $select->where->equalTo('status', 1);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchSearchQueries($query, $limit)
    {
        $select = new Select('application_attributes');
        $select->where->expression("MATCH(title, content) AGAINST (?)", $query);
        $select->limit($limit);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function add(array $data)
    { 
        $this->tableGateway->insert([
            'is_parent' => $data['is_parent'],
            'parent_id' => (int) $data['parent_id'], 
            'name' => $data['name'],
            'active' => 1,
            'type' => $data['type'],
            'latin_name' => $data['latin_name'], 
            'assign_to' => $data['assign_to'] 
        ]);
        return $this->tableGateway->getLastInsertValue();
    }

    public function checkExist( $permissionName, int $permissionId = null)
    {
        if ($permissionName) {
            if ($permissionId) {
                return $this->tableGateway->select([
                    'name' => $permissionName,
                    'id != ?' => $permissionId
                ])->toArray();
            } else {
                return $this->tableGateway->select([
                    'name' => $permissionName
                ])->toArray();
            }
        }
        return false;
    }
    
    public function update(int $id, array $data)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->update([
                'is_parent' => $data['is_parent'],
                'parent_id' => (int) $data['parent_id'],
//                 'shop_id' => $data['shop_id'],
                'name' => $data['name'],
                'active' => $data['active'],
                'type' => $data['type'],
                'latin_name' => $data['latin_name'],
                'assign_to' => $data['assign_to'] 
            ], [
                'id' => $id
            ]);
        }
    }
    public function updateType(int $id,  $type)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->update([ 
                'type' => $type, 
            ], [
                'id' => $id
            ]);
        }
    }

    public function delete(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'id' => $id
            ]);
        }
    }
}