<?php
namespace Products\Model;

use Application\Entity\User;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Select;

class ApplicationUsersTable
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_users', $dbAdapter);
    }

    public function fetch(array $where = null)
    {
        $select = new Select('application_users');
        if ($where) {
            $select->where($where);
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchUser($userId)
    {
        if ($userId > 0 && is_numeric($userId)) {
            $result = $this->tableGateway->select([
                'id' => $userId
            ]);
            return $result->current();
        }
        return false;
    }

    public function saveUser(array $user)
    {
        $this->tableGateway->insert([
            'email'=> $user['email'],
            'password'=> $user['password'],
            'fullname'=> $user['fullname'],
            'user_group_id'=> $user['user_group_id'],
            'salt'=> $user['salt'],
            'username'=> $user['username'],
            'status'=> 1  
        ]);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updateLastLoginTime($userId)
    {
        if ($userId > 0 && is_numeric($userId)) {
            return $this->tableGateway->update(array(
                'last_login' => time()
            ), array(
                'id' => $userId
            ));
        }
    }
}