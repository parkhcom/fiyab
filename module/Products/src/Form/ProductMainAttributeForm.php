<?php
namespace Products\Form;

use Zend\Form\Element;
use Product\Model\AttributeOptionTable;
use Zend\Form\Form;
use Products\Model\ApplicationAttributeOptionTable;
 
class ProductMainAttributeForm extends Form
{

    public function __construct($attributes, $currentAttributes = array(), $sm)
    {
        parent::__construct("productmainattribute");
        
        foreach ($attributes as $attribute) {
            
            $selected = false;
            $attributeValue = null;
            $attributeValueOp = array();
            $isForce = false;
            $preview = false;
            if ($currentAttributes) {
                foreach ($currentAttributes as $currentAttribute) {
                    
                    if ($attribute["id"] == $currentAttribute["attribute_id"]) {
                        $selected = true;
                        $attributeValue = $currentAttribute['value'];
                        if (isset($currentAttribute['attribute_option_id']))
                            $attributeValueOp = explode(',', $currentAttribute['attribute_option_id']);
                        
                        if ($currentAttribute['preview']) {
                            $preview = true;
                        }
                    }
                }
            }
            $attributeElementName = "attribute[{$attribute['id']}]";
            $attributeElement = new Element\Checkbox($attributeElementName);
            $attributeElement->setUseHiddenElement(false);
            $attributeElement->setLabel($attribute["name"]);
            $attributeElement->setChecked($selected);
            $attributeElement->setAttribute("id", $attribute['id']);
            $attributeElement->setAttribute("data-isparent", $attribute['is_parent']);
            $attributeElement->setAttribute("data-parentid", $attribute['parent_id']);
            
            if ($preview) {
                $attributeElement->setAttribute("onclick", "return false");
                $attributeElement->setAttribute("readonly", true);
                $attributeHiddenElementName = "attribute[{$attribute['id']}]";
            }
            $this->add($attributeElement);
            
            $attributeValueName = "attribute_value[{$attribute['id']}]";
            
            $object = new Element\Text($attributeValueName);
          
            switch ($attribute["type"]) {
                case "text":
                    $object->setValue($attributeValue);
                    $object->setAttribute("id", "attribute1_" . $attribute["id"]);
                    $object->setAttribute('class', 'form-control');
               
                    break;
                case "textarea":
                    $object = new Element\Textarea($attributeValueName);
                    $object->setValue($attributeValue);
                    $object->setAttribute("id", "attribute1_" . $attribute["id"]);
                    $object->setAttribute('class', 'form-control');
                    break;
           
                case "checkbox":
                    $object = new Element\Checkbox($attributeValueName);
                    $object->setAttribute("id", "attribute1_" . $attribute["id"]);
                    $object->setValue($attributeValue);
                    
                    $object->setValueOptions(array(
                        '0' => t('not have'),
                        '1' => t('have')
                    ));
                    
                    break;
          
                
                case "select":
                    $options_values = array();
                    $attributeOptionsTable = new ApplicationAttributeOptionTable($sm);
                    $where = [
                        'id' => $attribute["id"]
                    ];
                    $options = $attributeOptionsTable->fetch($where);
            
                    foreach ($options as $option) {
                        $options_values[$option["id"]] = $option["label"];
                    }
                    $object->setAttribute('class', 'form-control');
                    $object = new Element\Select($attributeValueName);
                    $object->setValueOptions($options_values);
                    $object->setValue($attributeValue);
                    $object->setAttribute("id", "attribute1_" . $attribute["id"]);
                    
                    break;
                
                case "multiselect":
                    $options_values = array();
                    $attributeOptionsTable = new AttributeOptionTable();
                    $options = $attributeOptionsTable->findByAttributeId($attribute["id"]);
                    foreach ($options as $option) {
                        $options_values[$option["id"]] = $option["label"];
                    }
                    $attributeValue = explode(',', $attributeValue);
                    $optionData = array();
                    foreach ($attributeValue as $data) {
                        $optionData[$data] = $data;
                    }
                    $object = new Element\Select($attributeValueName);
                    $object->setValueOptions($options_values);
                    $object->setAttribute('multiple', 'multiple');
                    $object->setAttribute('class', 'multiselect2');
                    $object->setValue($optionData);
                    $object->setAttribute("id", "attribute1_" . $attribute["id"]);
                    
                    break;
                
            }
            
            $this->add($object);
            
            $previewElementName = "preview[{$attribute['id']}]";
            $previewElement = new Element\Checkbox($previewElementName);
            $previewElement->setChecked($preview);
            $previewElement->setAttribute("id", "attribute2_" . $attribute["id"]);
            $this->add($previewElement);
        }
        
        $submit = new Element\Submit("submit");
        $submit->setAttribute("class", "btn btn-primary");
        $submit->setValue(t("Assign Attributes"));
        $this->add($submit);
    }
}