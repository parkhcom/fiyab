<?php
namespace Products\Form;
 

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;

class ApplicationAttributeForm extends Form
{

    public function __construct($attributes, $attributeOptions = array())
    {
        parent::__construct("product");
        
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        
        $attrName = new Element\Text("name");
        $attrName->setLabel("Attribute Name: ");
        $attrName->setAttribute("class", "form-control validate[required]");
        
        $attrParent = new Element\Select("parent_id");
        $parentAttributes = array();
        $parentAttributes["0"] = "---";
        if ($attributes) {
            foreach ($attributes as $attr) {
                $parentAttributes[$attr["id"]] = $attr["name"];
            }
        }
        
        $attrParent->setValueOptions($parentAttributes);
        $attrParent->setLabel("Parent: ");
        $attrParent->setAttribute("id", "parentIdSelect");
        $attrParent->setAttribute("class", "form-control");
        $this->add($attrParent);
        
        $attrIsParent = new Element\Checkbox("is_parent");
        $attrIsParent->setLabel("Is Parent: ");
        
        $types = [
            'text' => t('text'),
            'select'=> t('select'),
            'checkbox' => t('checkbox'),
            'multiselect' => t('multiselect'), 
            'textarea'=> t('textarea')
        ]; 
        
        $attrType = new Element\Select("type");
        $attrType->setLabel("Attribute Type: ");
        $attrType->setValueOptions($types);
        $attrType->setAttribute("onchange", "show_add_option_button()");
        $attrType->setAttribute("id", "type");
        $attrType->setAttribute("class", "form-control validate[required]");
        
        
        
        $latinName = new Element\Text("latin_name");
        $latinName->setLabel("English Name: ");
        $latinName->setAttribute("class", "form-control");
         
        
        $active = new Element\Checkbox("active");
        $active->setLabel(t("Status: ")); 
        $this->add($active);
        
        if ($attributeOptions && count($attributeOptions) > 1) {
            foreach ($attributeOptions as $attributeOption) {
                $id = rand(1000, 2000);
                $option = new Element\Text("options[]");
                $value = new Element\Text("values[]");
                
                $option->setValue($attributeOption["label"]);
                $value->setValue($attributeOption["value"]);
                
                $option->setLabel("Option: ");
                $value->setLabel("Value: ");
                
                $option->setAttribute("id", $id);
                $value->setAttribute("id", $id);
                
                $removeBtn = new Element\Button("remove");
                $removeBtn->setAttribute("id", $id);
                $removeBtn->setAttribute("class", "btn btn-danger");
                $removeBtn->setLabel("Remove");
                $removeBtn->setAttribute("onclick", "remove_item(this.id)");
                
                $this->add($option);
                $this->add($value);
                $this->add($removeBtn);
            }
        }
        
        $selectAssign = new Element\Select("assign_to");
        $selectAssign->setLabel("Assign to: ");
        $all = [
           1=> 'all',
           2=> 'selected' 
        ];
        $selectAssign->setValueOptions($all);
        $selectAssign->setAttribute("id", "assignto");
        $selectAssign->setAttribute("class", "form-control");
        $this->add($selectAssign);
        
        $token = new Element\Csrf("csrf");
        $submit = new Element\Submit("submitnew");
        $submit->setValue(t("Register & New"));
        $submit->setAttribute("class", "btn btn-success");
        $this->add($submit);
        
        $submit = new Element\Submit("submit");
        $submit->setValue(t("Register"));
        $submit->setAttribute("class", "btn btn-primary");
        $this->add($attrName)
            ->add($attrIsParent)
            ->add($attrType)
            -> add($token)
            ->add($latinName)
            ->add($submit);
        $this->addInputFilter();
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
            'name' => 'name',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => t('Required Field')
                        ]
                    ]
                ]
            ]
        ]);
    }
}