<?php
namespace Products\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class ShopForm extends Form
{

    public function __construct($userTableData, $isEditAction = false)
    {
        parent::__construct('products-shop-form');
        $this->setAttribute('id', 'form');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        
        $name = new Element\Text('name');
        $name->setLabel(t("Name"));
        $name->setAttributes([
            'id' => 'name',
            'placeholder' => t("Your Shop Name"),
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ]);
        
        $address = new Element\Textarea('address');
        $address->setLabel(t("address"));
        $address->setAttributes([
            'id' => 'address',
            'class' => 'form-control validate[required]',
            'placeholder' => t("Your Shop Address"),
            'required' => 'required'
        ]);
        
        if ($userTableData) {
            $userArray = [];
            if ($userTableData) {
                foreach ($userTableData as $users) {
                    
                    $userArray[$users['id']] = $users['username'];
                }
            }
            
            $owner = new Element\Select('user_id');
            $owner->setLabel(t("Shop Owner"));
            $owner->setValueOptions($userArray);
            $owner->setAttributes([
                'id' => 'owner',
                'style' => 'width:300px;text-align:center;  line-height:100px;',
                'class' => 'validate[required]',
                'required' => 'required'
            ]);
            $this->add($owner);
        }
        $tel = new Element\Text('telf');
        $tel->setLabel(t("First Phone"));
        $this->add($tel);
        $tel->setAttributes([
            'id' => 'telf',
            'class' => 'form-control  validate[required,custom[phone]]',
            'placeholder' => t("Your Shop Phone"),
            'required' => 'required'
        ]);
        
        $tel = new Element\Text('tels');
        $tel->setLabel(t("Second Phone"));
        $tel->setAttributes([
            'id' => 'telt',
            'placeholder' => t("Your Shop Another Phone"),
            'class' => 'form-control validate[custom[phone]]'
        ]);
        $this->add($tel);
        
        $url = new Element\Text('url');
        $url->setLabel(t("Shop Url"));
        $url->setAttributes([
            'id' => 'url',
            'placeholder' => t("Your EShop Address"),
            'class' => 'form-control validate[custom[url]] '
        ]);
        $this->add($url);
        
        $fax = new Element\Text('fax');
        $fax->setLabel(t("Fax"));
        $fax->setAttributes([
            'id' => 'fax',
            'class' => 'form-control '
        ]);
        $this->add($fax);
        
        $csrf = new Element\Csrf('csrf');
        $csrf->setCsrfValidatorOptions([
            'timeout' => 600
        ]);
        $submit = new Element\Submit('submit');
        $submit->setValue(t('Submit'));
        
        $submit->setAttributes([
            'class' => 'btn btn-info submitBtn',
            'id' => 'submitBtn'
        ]);
        $this->add($submit);
        if ($isEditAction) {
            $status = new Element\Checkbox('status');
            $status->setLabel(t("status"));
            $status->setAttributes([
                'id' => 'status'
            ]);
            $this->add($status);
        } else {
            $submit = new Element\Submit('submitandnew');
            $submit->setValue(t('Submit And New'));
            
            $submit->setAttributes([
                'class' => 'btn btn-info submitBtn',
                'id' => 'submitBtnNew'
            ]);
            $this->add($submit);
        }
        $this->add($name);
        $this->add($csrf);
        $this->add($address);
        $this->add($tel);
        $this->add($fax);
        $this->addInputFilter();
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
            'name' => 'name',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => t('Required Field')
                        ]
                    ]
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'telf',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => t('Required Field')
                        ]
                    ]
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'user_id',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
    }
}
