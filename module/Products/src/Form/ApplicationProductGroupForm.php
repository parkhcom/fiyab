<?php
namespace Products\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class ApplicationProductGroupForm extends Form
{

    public function __construct($groups,  $isEditAction = false)
    {
        parent::__construct('products-group-form');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('id', 'form');
        
        $this->setAttributes(array(
            'action' => '',
            'enctype'   =>  'multipart/form-data',
        ));
        
        $groupName = new Element\Text("name");
        $groupName->setLabel("Group Name: ");
        $groupName->setAttribute("class", "form-control  validate[required]");
//         $groupName->setAttribute("id", "namechecker");
        $groupName->setAttribute("data-url", "/ajax/check-name-exist");
        
 
        $keywords = new Element\Text("keywords");
        $keywords->setLabel("Keywords: ");
        $keywords->setAttribute("class", "form-control");
        
        $icon = new Element\Text("icon");
        $icon->setLabel("Icon: ");
        $icon->setAttribute("class", "form-control");
        
        $order = new Element\Text("order");
        $order->setLabel("Order: ");
        $order->setAttribute("class", "form-control  validate[ custom[integer] ] ");
  
        
        $compareable = new Element\Checkbox("compareable");
        $compareable->setLabel("Compare: ");
        
        $description = new Element\Textarea("description");
        $description->setLabel("Description: ");
        $description->setAttribute("class", "form-control");
        
        $image = new Element\File('avatar');
        $image->setAttributes(array(
            'id' => 'fileUpload'
        ));
        $image->setLabel(t("Avatar: "));
 
        $groupParent = new Element\Select("parent_id");
        $parentGroups = [];
        $parentGroups["0"] = "---";
        if ($groups) {
            foreach ($groups as $group) {
                if(isset($group["parent_name"]))     $parentGroups[$group["id"]] = $group["parent_name"] . ' >> ' .$group["name"];
                if(!isset($group["parent_name"]))     $parentGroups[$group["id"]] = $group["name"];
            }
          
        }
        $groupParent->setValueOptions($parentGroups);
        $groupParent->setLabel("Parent: ");
        $groupParent->setAttribute('id', 'groupProduct');
        $groupParent->setAttribute("style", 'width:300px;');
        $groupParent->setAttribute("class", ' form-control validate[required]');
        
        $groupIsParent = new Element\Checkbox("is_parent");
        $groupIsParent->setLabel("Is Parent: ");
        $options = [];
        
        $token = new Element\Csrf("csrf");
        
        $submit = new Element\Submit("submit");
        $submit->setValue(t("Submit"));
        $submit->setAttribute("class", "btn btn-primary submitBtn");
        $this->add($submit);
        if ($isEditAction) {
            $status = new Element\Checkbox('active');
            $status->setLabel(t("status"));
            $status->setAttributes([
                'id' => 'status'
            ]);
            $this->add($status);
        } else {
            $submit = new Element\Submit('submitandnew');
            $submit->setValue(t('Submit And New'));
            
            $submit->setAttributes([
                'class' => 'btn btn-info submitBtn',
                'id' => 'submitBtnNew'
            ]);
            $this->add($submit);
        }
        
        $this->add($image); 
        $this->add($groupName);
        $this->add($order);
        $this->add($icon);
        $this->add($compareable);
        $this->add($groupParent);
        $this->add($groupIsParent);
        $this->add($keywords);
        $this->add($description);
        $this->add($token);
        
        $this->addInputFilter();
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
            'name' => 'name',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => t('Required Field')
                        ]
                    ]
                ]
            ]
        ]);
        $inputFilter->add([
            'type' => 'Zend\InputFilter\FileInput',
            'name' => 'avatar',
            'required' => false,
            'validators' => [
                [
                    'name' => 'FileUploadFile'
                ],
                [
                    'name' => 'FileIsImage'
                ],
                [
                    'name' => 'FileImageSize',
                    'options' => [
                        'minWidth' => 128,
                        'minHeight' => 128,
                        'maxWidth' => 4096,
                        'maxHeight' => 4096
                    ]
                ]
            ],
            'filters' => [
                [
                    'name' => 'FileRenameUpload',
                    'options' => [
                        'target' => __DIR__ . '/../../../../public/upload/productGroup',
                        'useUploadName' => true,
                        'useUploadExtension' => true,
                        'overwrite' => false,
                        'randomize' => true
                    ]
                ]
            ]
        ]);
    }
}

	 
 