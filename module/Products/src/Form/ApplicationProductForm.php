<?php
namespace Products\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use yii\base\Arrayable;

class ApplicationProductForm extends Form
{

    public function __construct($groups = [], $attribute =  [], $isEditAction = false)
    {
        parent::__construct('products-group-form');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('id', 'form');
        $this->setAttribute('action', '');
 
        
        $groupName = new Element\Text("pname");
        $groupName->setLabel(t("Name: "));
        $groupName->setAttribute("class", "form-control  validate[required]");
        $groupName->setAttribute("id", "namechecker");
        $groupName->setAttribute("data-url", "/ajax/check-pname-exist");
        $this->add($groupName);
        
        $downloadable = new Element\Text("downloadable");
        $downloadable->setLabel(t("Downloadable: "));
        $downloadable->setAttribute("class", "form-control");
        $this->add($downloadable);
        
        $description = new Element\Textarea("meta_description");
        $description->setLabel(t("Meta Description: "));
        $description->setAttribute("class", "form-control");
        $description->setAttribute("id", "description");
        $this->add($description);
        
        $description = new Element\Textarea("meta_keyword");
        $description->setLabel(t("Meta Keyword: "));
        $description->setAttribute("class", "form-control");
        $description->setAttribute("id", "description");
        $this->add($description);
        
        
        $groupParent = new Element\Select("group_id");
        $parentGroups = []; 
        if ($groups) {
            foreach ($groups as $group) {
                $parentGroups[$group["id"]] = $group["name"];
            }
        }
        $groupParent->setValueOptions($parentGroups);
        $groupParent->setLabel(t("Parent: "));
        $groupParent->setAttribute('id', 'groupProduct');
        $groupParent->setAttribute("style", 'width:100%;');
        $groupParent->setAttribute("class", ' validate[required]');
        $groupParent->setAttribute("multiple", false);
        $this->add($groupParent);
        
        $options = [];
        if ($attribute) {
            foreach ($attribute as $k => $tax) {
                $options[$tax['id']] = $tax['name'];
            }
        } 
        
        $attributeSet = new Element\Select('attribute_parrent_id');
        $attributeSet->setLabel(t("Attribute Set List"));
        $attributeSet->setValueOptions($options);
        $attributeSet->setAttribute("multiple", false);
        $attributeSet->setAttribute("style", 'width:100%;');
        $attributeSet->setAttribute("id", 'setAttribute');
        $attributeSet->setAttribute("class", 'validate[required]');
        $this->add($attributeSet);
        
        $token = new Element\Csrf("csrf");
        $this->add($token);
        
        $submit = new Element\Submit("submit");
        $submit->setValue(t("Submit"));
        $submit->setAttribute("class", "btn btn-primary submitBtn");
        $this->add($submit);
        if ($isEditAction) {
            $status = new Element\Checkbox('active');
            $status->setLabel(t("status"));
            $status->setAttributes([
                'id' => 'status'
            ]);
            $this->add($status);
        } else {
            $submit = new Element\Submit('submitnew');
            $submit->setValue(t('Submit And New'));
            $submit->setAttributes([
                'class' => 'btn btn-info submitBtn',
                'id' => 'submitBtnNew'
            ]);
            $this->add($submit);
            $submit = new Element\Submit('submitexit');
            $submit->setValue(t('Submit And Exit'));
            $submit->setAttributes([
                'class' => 'btn btn-info submitBtnExit',
                'id' => 'submitBtnExit'
            ]);
            $this->add($submit);
        }
        $this->addInputFilter();
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
            'name' => 'pname',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => t('Required Field')
                        ]
                    ]
                ]
            ]
        ]);
    }
}

	 
 