<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Products\Controller;

use Zend\View\Model\ViewModel;
use Application\Service\BaseAdminController;
use Zend\Mvc\MvcEvent;
use Products\Model\ApplicationProductTable;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use Products\Form\ApplicationProductForm;
use Products\Model\ApplicationProductGroupTable;
use Products\Model\ApplicationAttributeTable;
use Products\Model\ApplicationShopOwnersTable;
use Products\Model\ApplicationProductListTable;
use Products\Model\ApplicationAttributeOptionTable;
use Products\Model\ApplicationProductAttributeTable;
use Zend\Db\Adapter\Driver\ConnectionInterface;
use Rap2hpoutre\FastExcel\FastExcel;
use Products\Model\ApplicationShopTable;
use Products\Model\ApplicationShopProductTable;
use Products\Model\ApplicationShopProductAttributeTable;
use Products\Model\ApplicationShopProductPriceTable;

class ProductController extends BaseAdminController
{

    public $fullAttribute = [];

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e, 'products-product-');
        if (! $this->userData)
            return $this->redirect()->toRoute('product');
        
        parent::onDispatch($e);
    }

    public function setforsellAction()
    {
        $id = $this->params('id', - 1);
        $shopid = $this->params('shopid', - 1);
        
        $applicationProductTable = new ApplicationProductListTable($this->getServiceLocator());
        $productData = $applicationProductTable->fetch([
            'id' => $id
        ]);
        
        $groupsConfig = $this->getServiceLocator()->get('config')['groups_config']['shopkeeper'];
        $shopOwnersTable = new ApplicationShopTable($this->getServiceLocator());
        $where = [];
        if ($this->userData->user_group_id == $groupsConfig) {
            $where = [
                'user_id' => $this->userData->id
            ];
        }
        $shopOwnersTableData = $shopOwnersTable->fetch($where);
        
        // CHECK SHOP IS EXIST
        if (! $shopOwnersTableData) {
            $this->flashMessenger()->addErrorMessage(t('Please add Your Shop First'));
            return $this->redirect()->toRoute('products-shop', [
                'controller' => 'shop',
                'action' => 'list'
            ]);
        }
        
        // CHECK PRODUCT IS EXIST
        if (! $productData) {
            $this->flashMessenger()->addErrorMessage(t('No Such Field Exist'));
            return $this->redirect()->toRoute('products-product', [
                'controller' => 'product',
                'action' => 'list'
            ]);
        }
        
        // CHECK ATTRIBUTE IS EXIST
        $productData = $productData[0];
        $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
        $applicationProductAttributeTable = new ApplicationProductAttributeTable($this->getServiceLocator());
        $applicationAttributeOptionTable = new ApplicationAttributeOptionTable($this->getServiceLocator());
        $applicationShopProductTable = new ApplicationShopProductTable($this->getServiceLocator());
        $applicationShopProductAttributeTable = new ApplicationShopProductAttributeTable($this->getServiceLocator());
        $applicationShopProductPriceTable = new ApplicationShopProductPriceTable($this->getServiceLocator());
        
        // GET DATA OF THIS USER PRODUCT ATTRIBUTE AND PRICE OF PRODUCT IN STORES
        
        // GET SHOPS
        $where = [
            'product_id' => $id,
            'user_id' => $id,
            'shop_id' => $shopid
        ];
        $shopWithThisProduct = $applicationShopProductTable->fetch($where);
        
        if ($shopWithThisProduct) {
            $view['myshopes'] = $shopid;
            
            $view['status'] = $shopWithThisProduct[0]['status'];
            // GET price
            $where = [
                'id_product_shop' => $shopWithThisProduct[0]['id']
            ];
            $priceWithThisProduct = $applicationShopProductPriceTable->fetchPrice($where);
            if ($priceWithThisProduct)
                $view['price'] = $priceWithThisProduct['price'];
            
            // GET ATTRIBUTE IN THIS STORE
            
            $where = [
                'id_product_shop' => $shopWithThisProduct[0]['id']
            ];
            $applicationShopAttrib = $applicationShopProductAttributeTable->fetch($where);
            
            if ($applicationShopAttrib) {
                $applicationShopAttribs = [];
                foreach ($applicationShopAttrib as $datAttrib) {
                    $applicationShopAttribs[$datAttrib['attribute_id']][] = $datAttrib['attribute_option_id'];
                }
                $view['myshopProductAttributes'] = $applicationShopAttribs;
            }
        }
        
        $productAttributeData = $applicationAttributeTable->fetch([
            'id' => $productData['attribute_parrent_id']
        ]);
        
        if (! $productAttributeData) {
            $this->flashMessenger()->addErrorMessage(t('NO ATTRIBUTE IS EXIST'));
            return $this->redirect()->toRoute('products-product', [
                'controller' => 'product',
                'action' => 'list'
            ]);
        }
        
        $this->fullAttribute[] = $productAttributeData;
        $this->categoryTree($productAttributeData[0]['id'], $productAttributeData[0]['name']);
        
        $attributes = call_user_func_array('array_merge', $this->fullAttribute);
        
        asort($attributes);
        // GET OPTION ATTRIBUTES
        $mainAttribute = [];
        if ($attributes) {
            foreach ($attributes as &$attribs) {
                if (! $attribs['is_parent']) {
                    $dataWhere = [
                        'attribute_id' => $attribs['id']
                    ];
                    $attribs['options'] = $applicationAttributeOptionTable->fetch($dataWhere);
                }
            }
        }
        // GET PRODUCT ATTRIBUES
        $productAttributeArray = [];
        $dataWhere = [
            'product_id' => $id
        ];
        $productAttribute = $applicationProductAttributeTable->fetch($dataWhere);
        if ($productAttribute) {
            foreach ($productAttribute as $datAttr) {
                $productAttributeArray[$datAttr['attribute_id']] = $datAttr;
            }
        }
        
        $view['shopData'] = $shopOwnersTableData;
        $view['productAttribute'] = $productAttributeArray;
        
        // CHANGE KEY BY ID
        $mainAttributes = array_reduce($attributes, function ($result, $item) {
            $result[$item['id']] = $item;
            return $result;
        }, array());
        
        $view['attributes'] = $mainAttributes;
        $view['name'] = $productData['pname'];
        
        if ($this->request->isPost()) {
            $postData = $this->request->getPost();
            
            try {
                $connection = $this->dbAdapter->getDriver()->getConnection();
                $connection->beginTransaction();
                // RESET ATTRIBUTES
                if ($postData) {
                    $postData['status'] = ($postData['status'] == 'on' ? 1 : 0);
                    if (isset($postData['price']) && is_numeric($postData['price'])) {
                        $datAttrProduct = [
                            'product_id' => $id,
                            'shop_id' => $shopid
                        ];
                        $isExistProduct = $applicationShopProductTable->fetch($datAttrProduct);
                        $datAttrProduct = [
                            'product_id' => $id,
                            'shop_id' => $shopid,
                            'user_id' => $this->userData->id,
                            'status' => $postData['status']
                        ];
                        if ($isExistProduct) {
                            $isExistProduct = $isExistProduct[0];
                            if (! $postData['status']) {
                                // DISABLE PRODUCT AND EXIT IF STATUS = 0
                                $isExistProduct = $applicationShopProductTable->update($isExistProduct['id'], $datAttrProduct);
                                $this->flashMessenger()->addErrorMessage(t('Status Has Been Change To Inactive'));
                                return $this->redirect()->toRoute('products-product', [
                                    'controller' => 'product',
                                    'action' => 'list',
                                    'lang' => $this->tools->getLang()
                                ]);
                            }
                            $newId = $isExistProduct['id'];
                        } else {
                            $newId = $applicationShopProductTable->add($datAttrProduct);
                        }
                        // ADD PRICE DATA
                        $datPrice = [
                            'price' => $postData['price'],
                            'id_product_shop' => $newId
                        ];
                        $applicationShopProductPriceTable->add($datPrice);
                        // ADD ATTRIBUTE DATA
                        if ($postData['attribute_value']) {
                            $applicationShopProductAttributeTable->delete($newId);
                            foreach ($postData['attribute_value'] as $attibutId => $attributesOptions) {
                                foreach ($attributesOptions as $attributesOpt) {
                                    $datAttrinbute = [
                                        'attribute_id' => $attibutId,
                                        'attribute_option_id' => $attributesOpt,
                                        'id_product_shop' => $newId
                                    ];
                                    $isExistAttribute = $applicationShopProductAttributeTable->fetch($datAttrinbute);
                                    if (! $isExistAttribute) {
                                        $applicationShopProductAttributeTable->add($datAttrinbute);
                                    }
                                }
                            }
                        }
                    } else {
                        $view['error'] = t('Price is requierd');
                        $connection->rollback();
                    }
                }
                $this->flashMessenger()->addSuccessMessage(t('Data inserted successfully'));
                $connection->commit();
            } catch (\Exception $e) {
                if ($connection instanceof \Zend\Db\Adapter\Driver\ConnectionInterface) {
                    $this->flashMessenger()->addErrorMessage(t('Problem Occured'));
                    $connection->rollback();
                }
            }
            return $this->redirect()->toRoute('products-product', [
                'controller' => 'product',
                'action' => 'list',
                'lang' => $this->tools->getLang()
            ]);
        }
        
        $this->layout()->headTitle = t("Manage Shopkeeper Attribute");
        
        return new ViewModel($view);
    }

    public function importExcelAction()
    {
        $data = [];
        ini_set('max_execution_time', 30000);
        $config = $this->getServiceLocator()->get('config');
        $xlsDir = $config['base_route'] . "/uploads/excelfile/huawei.xlsx";
        
        // GET FILE NAME
        $path_parts = pathinfo($xlsDir);
        
        $collectionData = (new FastExcel())->import($xlsDir);
        $applicationProductGroupTable = new ApplicationProductGroupTable($this->getServiceLocator());
        $where = [
            'name' => $path_parts['filename'],
            'parent_id' => 1
        ];
        $isGroupExist = $applicationProductGroupTable->fetch($where);
        
        if ($isGroupExist) {
            $groupId = $isGroupExist[0]['id'];
        } else {
            $dataArrayGroup = array(
                'name' => $path_parts['filename'],
                'keywords' => '',
                'description' => '',
                'is_parent' => 1,
                'active' => 1,
                'parent_id' => 1,
                'avatar' => '',
                'order' => 1,
                'compareable' => 1
            );
            $groupId = $applicationProductGroupTable->add($dataArrayGroup);
        }
        
        if ($path_parts['filename']) {
            
            $controller = $this;
            $users = (new FastExcel())->import($xlsDir, 
                function ($lineData) use ($controller, $groupId) {
                    $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
                    $aplicationAttributeOptionTable = new ApplicationAttributeOptionTable($this->getServiceLocator());
                    $applicationProductListTable = new ApplicationProductListTable($this->getServiceLocator());
                    $applicationProductAttributeTable = new ApplicationProductAttributeTable($this->getServiceLocator());
                    $attributeId = 0;
                    $attributeIdChild = 0;
                    if ($lineData) {
                        foreach ($lineData as $key => $line) {
                            $line = trim($line);
                            if ($key == 'attributename') {
                                $dataArrayAttributes = [
                                    'is_parent' => 1,
                                    'parent_id' => 1,
                                    'name' => $line,
                                    'active' => 1,
                                    'type' => 'text',
                                    'latin_name' => 'FromExcel',
                                    'assign_to' => 'all'
                                ];
                                $where = [
                                    'is_parent' => 1,
                                    'name' => $line,
                                    'type' => 'text'
                                ];
                                $isExist = $applicationAttributeTable->fetch($where);
                                if ($isExist) {
                                    $attributeId = $isExist[0]['id'];
                                } else {
                                    $attributeId = $applicationAttributeTable->add($dataArrayAttributes);
                                }
                            } else if ($key == 'attributevalue') {
                                $dataArrayAttributes = [
                                    'is_parent' => 0,
                                    'parent_id' => $attributeId,
                                    'name' => $line,
                                    'active' => 1,
                                    'type' => 'text',
                                    'latin_name' => 'FromExcel',
                                    'assign_to' => 'all'
                                ];
                                $where = [
                                    'is_parent' => 1,
                                    'name' => $line,
                                    'type' => 'text'
                                ];
                                $isExist = $applicationAttributeTable->fetch($where);
                                if ($isExist) {
                                    $attributeIdChild = $isExist[0]['id'];
                                } else {
                                    $attributeIdChild = $applicationAttributeTable->add($dataArrayAttributes);
                                }
                            } else if ($key == 'type') {
                                $applicationAttributeTable->updateType($attributeIdChild, $line);
                            } else {
                                $optionId = 0;
                                // add product attriubute value
                                $where = [
                                    'id' => $attributeIdChild
                                ];
                                $isExist = $applicationAttributeTable->fetch($where);
                                if ($isExist[0]['type'] == 'select') {
                                    
                                    // INSERT OPTION TO OTION TABLE IF IS NOT EXIST
                                    $isOptionExist = $aplicationAttributeOptionTable->fetch([
                                        'label' => $line
                                    ]);
                                    
                                    $isOptionExistAttribute = $aplicationAttributeOptionTable->fetch([
                                        'attribute_id' => $attributeIdChild
                                    ]);
                                    
                                    $index = count($isOptionExistAttribute) - 1;
                                    if ($isOptionExist) {
                                        $optionId = $isOptionExist[0]['id'];
                                    } else {
                                        $value = 1;
                                        if ($isOptionExistAttribute) {
                                            $value = $isOptionExistAttribute[$index]['value'] + 1;
                                        }
                                        $dataOptionAttributes = [
                                            'attribute_id' => $attributeIdChild,
                                            'value' => $value,
                                            'label' => $line,
                                            'image' => ''
                                        ];
                                        $optionId = $aplicationAttributeOptionTable->add($dataOptionAttributes);
                                    }
                                }
                                if ($isExist[0]['type'] == 'select') {
                                    
                                    // INSERT OPTION TO OTION TABLE IF IS NOT EXIST
                                    $isOptionExist = $aplicationAttributeOptionTable->fetch([
                                        'label' => $line
                                    ]);
                                    
                                    $isOptionExistAttribute = $aplicationAttributeOptionTable->fetch([
                                        'attribute_id' => $attributeIdChild
                                    ]);
                                    
                                    $index = count($isOptionExistAttribute) - 1;
                                    if ($isOptionExist) {
                                        $optionId = $isOptionExist[0]['id'];
                                    } else {
                                        $value = 1;
                                        if ($isOptionExistAttribute) {
                                            $value = $isOptionExistAttribute[$index]['value'] + 1;
                                        }
                                        $dataOptionAttributes = [
                                            'attribute_id' => $attributeIdChild,
                                            'value' => $value,
                                            'label' => $line,
                                            'image' => ''
                                        ];
                                        $optionId = $aplicationAttributeOptionTable->add($dataOptionAttributes);
                                    }
                                }
                                // MULTI SELECT TYPE ATTRIBUTES
                                if ($isExist[0]['type'] == 'multiselect') {
                                    $optionId = [];
                                    // INSERT OPTION TO OTION TABLE IF IS NOT EXIST
                                    $multiattr = explode('-', $line);
                                    // var_dump($multiattr);die;
                                    foreach ($multiattr as $attr) {
                                        $attr = trim($attr);
                                        $isOptionExist = $aplicationAttributeOptionTable->fetch([
                                            'label' => $attr
                                        ]);
                                        
                                        $isOptionExistAttribute = $aplicationAttributeOptionTable->fetch([
                                            'attribute_id' => $attributeIdChild
                                        ]);
                                        
                                        $index = count($isOptionExistAttribute) - 1;
                                        
                                        if ($isOptionExist) {
                                            $optionIds[] = $isOptionExist[0]['id'];
                                        } else {
                                            $value = 1;
                                            if ($isOptionExistAttribute) {
                                                $value = $isOptionExistAttribute[$index]['value'] + 1;
                                            }
                                            $dataOptionAttributes = [
                                                'attribute_id' => $attributeIdChild,
                                                'value' => $value,
                                                'label' => $attr,
                                                'image' => ''
                                            ];
                                            
                                            $addedOptionId = $aplicationAttributeOptionTable->add($dataOptionAttributes);
                                            
                                            $optionId[] = $addedOptionId;
                                        }
                                    }
                                    $optionId = implode(',', $optionId);
                                }
                                $dataArrayAttributes = [
                                    'pname' => $key,
                                    'attribute_parrent_id' => 1,
                                    'active' => 1,
                                    'group_id' => $groupId,
                                    'meta_description' => '',
                                    'meta_keyword' => $key
                                ];
                                
                                $isProductExist = $applicationProductListTable->fetchBYname($key);
                                
                                if ($isProductExist) {
                                    $productId = $isProductExist['id'];
                                } else {
                                    $productId = $applicationProductListTable->add($dataArrayAttributes);
                                }
                                if ($line && $line !== '') {
                                    if ($optionId) {
                                        $dataAttr = [
                                            'attribute_id' => $attributeIdChild,
                                            'preview' => 0,
                                            'product_id' => $productId,
                                            'attribute_option_id' => $optionId,
                                            'value' => ''
                                        ];
                                    } else {
                                        $dataAttr = [
                                            'attribute_id' => $attributeIdChild,
                                            'preview' => 0,
                                            'product_id' => $productId,
                                            'attribute_option_id' => $optionId,
                                            'value' => $line
                                        ];
                                    }
                                    $applicationProductAttributeTable->add($dataAttr);
                                }
                            }
                        }
                    }
                });
        }
        var_dump($users);
        
        die();
    }

    public function categoryTree($parent_id, $name)
    {
        $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
        $productAttributeData = $applicationAttributeTable->fetch([
            'parent_id' => $parent_id
        ]);
        $this->fullAttribute[$parent_id] = $productAttributeData;
        if ($productAttributeData) {
            foreach ($productAttributeData as $attibute) {
                if ($attibute['is_parent']) {
                    $this->categoryTree($attibute['id'], $attibute['name']);
                }
            }
        }
        return $this->fullAttribute;
    }

    public function assignAttributeAction()
    {
        $id = $this->params('id', - 1);
        $applicationProductTable = new ApplicationProductListTable($this->getServiceLocator());
        $productData = $applicationProductTable->fetch([
            'id' => $id
        ]);
        // CHECK PRODUCT IS EXIST
        if (! $productData) {
            $this->flashMessenger()->addErrorMessage(t('No Such Field Exist'));
            return $this->redirect()->toRoute('products-product', [
                'controller' => 'product',
                'action' => 'list'
            ]);
        }
        
        // CHECK ATTRIBUTE IS EXIST
        $productData = $productData[0];
        $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
        $applicationProductAttributeTable = new ApplicationProductAttributeTable($this->getServiceLocator());
        $applicationAttributeOptionTable = new ApplicationAttributeOptionTable($this->getServiceLocator());
        $productAttributeData = $applicationAttributeTable->fetch([
            'id' => $productData['attribute_parrent_id']
        ]);
        
        if (! $productAttributeData) {
            $this->flashMessenger()->addErrorMessage(t('NO ATTRIBUTE IS EXIST'));
            return $this->redirect()->toRoute('products-product', [
                'controller' => 'product',
                'action' => 'list'
            ]);
        }
        
        $this->fullAttribute[] = $productAttributeData;
        $this->categoryTree($productAttributeData[0]['id'], $productAttributeData[0]['name']);
        
        $attributes = call_user_func_array('array_merge', $this->fullAttribute);
        
        asort($attributes);
        // GET OPTION ATTRIBUTES
        $mainAttribute = [];
        if ($attributes) {
            foreach ($attributes as &$attribs) {
                if (! $attribs['is_parent']) {
                    $dataWhere = [
                        'attribute_id' => $attribs['id']
                    ];
                    $attribs['options'] = $applicationAttributeOptionTable->fetch($dataWhere);
                }
            }
        }
        // GET PRODUCT ATTRIBUES
        $productAttributeArray = [];
        $dataWhere = [
            'product_id' => $id
        ];
        $productAttribute = $applicationProductAttributeTable->fetch($dataWhere);
        if ($productAttribute) {
            foreach ($productAttribute as $datAttr) {
                $productAttributeArray[$datAttr['attribute_id']] = $datAttr;
            }
        }
        $view['productAttribute'] = $productAttributeArray;
        
        // CHANGE KEY BY ID
        $mainAttributes = array_reduce($attributes, function ($result, $item) {
            $result[$item['id']] = $item;
            return $result;
        }, array());
        
        $view['attributes'] = $mainAttributes;
        $view['name'] = $productData['pname'];
        
        if ($this->request->isPost()) {
            $postData = $this->request->getPost();
            
            try {
                $connection = $this->dbAdapter->getDriver()->getConnection();
                $connection->beginTransaction();
                // RESET ATTRIBUTES
                $dataWhere = [
                    'product_id' => $id
                ];
                $applicationProductAttributeTable->deleteByProductId($dataWhere);
                if ($postData) {
                    if (isset($postData['attribute_value'])) {
                        foreach ($postData['attribute_value'] as $attributeId => $data) {
                            $type = false;
                            if (isset($mainAttributes[$attributeId])) {
                                $type = $mainAttributes[$attributeId]['type'];
                            }
                            $preview = 0;
                            if (isset($postData['preview'][$attributeId])) {
                                $preview = ($postData['preview'][$attributeId] ? 1 : 0);
                            }
                            if (is_array($data)) {
                                $options = implode(',', $data);
                                $optionId = $options;
                                $value = '';
                            } else {
                                if (($type == 'text' || $type == 'checkbox' || $type == 'textarea') ? $optionId = 0 : $optionId = $data);
                                $value = $data;
                                if ($type == 'checkbox') {
                                    if ($data == 1 || $data == 'on')
                                        $value = 1;
                                    else
                                        $value = 0;
                                }
                            }
                            $newDataArray = [
                                'attribute_id' => $attributeId,
                                'preview' => $preview,
                                'product_id' => $id,
                                'attribute_option_id' => $optionId,
                                'value' => $value
                            ];
                            $applicationProductAttributeTable->add($newDataArray);
                        }
                    }
                }
                $this->flashMessenger()->addSuccessMessage(t('Data inserted successfully'));
                $connection->commit();
            } catch (\Exception $e) {
                if ($connection instanceof \Zend\Db\Adapter\Driver\ConnectionInterface) {
                    $connection->rollback();
                }
            }
            return $this->redirect()->toRoute('products-product', [
                'controller' => 'product',
                'action' => 'list',
                'lang' => $this->tools->getLang()
            ]);
        }
        
        $this->layout()->headTitle = t("Manage Product Main Attribute");
        $LinkData = [];
        
        $this->layout()->LinkData = $LinkData;
        return new ViewModel($view);
    }

    public function editAction()
    {
        $id = $this->params('id', - 1);
        $applicationProductGroupTable = new ApplicationProductGroupTable($this->getServiceLocator());
        $groupTableData = $applicationProductGroupTable->fetch([
            'is_parent' => 0
        ]);
        
        $applicationProductTable = new ApplicationProductListTable($this->getServiceLocator());
        $productData = $applicationProductTable->fetch([
            'id' => $id
        ]);
        
        if (! $productData) {
            $this->flashMessenger()->addErrorMessage(t('No Such Field Exist'));
            return $this->redirect()->toRoute('products-product', [
                'controller' => 'product',
                'action' => 'list',
                'lang' => $this->tools->getLang()
            ]);
        }
        $productData = $productData[0];
        $view['id'] = $id;
        $view['name'] = $productData['pname'];
        
        $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
        $attributeTableData = $applicationAttributeTable->fetch([
            'is_parent' => 1,
            'parent_id' => 0
        ]);
        
        $where = [];
        if (! $this->isSupperAdmin) {
            $where = [
                'user_id' => $this->userData->id
            ];
        }
        
        $shopForm = new ApplicationProductForm($groupTableData, $attributeTableData, true);
        $shopForm->populateValues($productData);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $shopForm->setData($postData);
            if ($shopForm->isValid()) {
                $shopOwnersTable = new ApplicationShopOwnersTable($this->getServiceLocator());
                $postData = $shopForm->getData();
                $postData['active'] = 1;
                $postData['attribute_parrent_id'] = $postData['attribute_parrent_id'];
                $addedId = $applicationProductTable->update($id, $postData);
                if ($addedId !== false) {
                    $this->flashMessenger()->addSuccessMessage(t('Data Update successfully'));
                    return $this->redirect()->toRoute('products-product', [
                        'controller' => 'product',
                        'action' => 'list',
                        'lang' => $this->tools->getLang()
                    ]);
                } else {
                    $this->flashMessenger()->addErrorMessage(t('Data Update error'));
                }
            } else {
                $this->flashMessenger()->addErrorMessage(t('Form Data error'));
            }
        }
        $view['form'] = $shopForm;
        return new ViewModel($view);
    }

    public function addAction()
    {
        $applicationProductGroupTable = new ApplicationProductGroupTable($this->getServiceLocator());
        $groupTableData = $applicationProductGroupTable->fetch([
            'is_parent' => 1,
            'parent_id' => 0
        ]);
        
        $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
        $attributeTableData = $applicationAttributeTable->fetch([
            'parent_id' => 0
        ]);
        
        $where = [];
        if (! $this->isSupperAdmin) {
            $where = [
                'user_id' => $this->userData->id
            ];
        }
        $applicationProductTable = new ApplicationProductListTable($this->getServiceLocator());
        $shopForm = new ApplicationProductForm($groupTableData, $attributeTableData, false);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $shopForm->setData($postData);
            if ($postData['submitnew']) {
                return $this->redirect()->toRoute('products-product', [
                    'controller' => 'product',
                    'action' => 'add',
                    'lang' => $this->tools->getLang()
                ]);
            } else if ($postData['submitexit']) {
                return $this->redirect()->toRoute('products-product', [
                    'controller' => 'product',
                    'action' => 'list',
                    'lang' => $this->tools->getLang()
                ]);
            }
            if ($shopForm->isValid()) {
                $shopOwnersTable = new ApplicationShopOwnersTable($this->getServiceLocator());
                $postData = $shopForm->getData();
                $postData['active'] = 1;
                $postData['attribute_parrent_id'] = $postData['attribute_parrent_id'];
                $addedId = $applicationProductTable->add($postData);
                if ($addedId) {
                    $this->flashMessenger()->addSuccessMessage(t('Data inserted successfully'));
                    $view['id'] = $addedId;
                } else {
                    $this->flashMessenger()->addErrorMessage(t('Data insert error'));
                }
            } else {
                $res = $shopForm->getMessages();
                var_dump($res);
                die();
                $this->flashMessenger()->addErrorMessage(t('Form Data error'));
            }
        }
        $view['form'] = $shopForm;
        return new ViewModel($view);
    }

    public function listAction()
    {
        $groupsConfig = $this->getServiceLocator()->get('config')['groups_config']['shopkeeper'];
        $applicationProductTable = new ApplicationProductListTable($this->getServiceLocator());
        $applicationProductTableData = $applicationProductTable->fetch();
        $shopid = $this->params('id', - 1);
        
        $token = $this->tools->token();
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        
        $dbAdapter = new Adapter($config['db']);
        
        $groupId = $this->userData->user_group_id;
        $producrs = $applicationProductTable->fetch();
        $grid->setTitle(t('Product List'));
        $grid->setDefaultItemsPerPage($config['zfcItemsPerPage']);
        $grid->setDataSource($producrs, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(t('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $col = new Column\Select('pname');
        $col->setLabel(t('Name'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(t('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-fire');
        $viewAction->setLink('/product/assign-attribute/id/' . $rowId);
        $viewAction->setTooltipTitle(t('Add Attribute'));
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink('/product/edit/id/' . $rowId);
        $viewAction1->setTooltipTitle(t('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink('/product/delete/id/' . $rowId . '/token/' . $token);
        $viewAction2->setTooltipTitle(t('Delete'));
        
        $viewAction3 = new Column\Action\Icon();
        $viewAction3->setIconClass('glyphicon glyphicon-plus');
        $viewAction3->setLink('/product/setforsell/id/' . $rowId . '/shopid/' . $shopid);
        $viewAction3->setTooltipTitle(t('Add Product'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(t('Operations'));
        if ($groupId == $config['groups_config']['shopkeeper']) {
            $actions2->addAction($viewAction3);
        } else if ($groupId < $config['groups_config']['shopkeeper']) {
            
            $actions2->addAction($viewAction);
            $actions2->addAction($viewAction1);
            $actions2->addAction($viewAction2);
        }
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        $block[] = '<a href="/product/add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . t("Add") . '</a>';
        $grid->setLink($block);
        
        $grid->getRenderer()->setTemplate('products/product/list');
        $grid->render();
        
        return $grid->getResponse();
    }

    public function deleteAction()
    {
        if ($this->tools->token('check', $this->params('token'))) {
            $applicationProductTable = new ApplicationProductListTable($this->getServiceLocator());
            
            $isDeleted = $applicationProductTable->delete($this->params('id', - 1));
            $isDeleted = $applicationProductTable->actination($this->params('id', - 1), 0);
            if ($isDeleted !== false) {
                $this->flashMessenger()->addSuccessMessage(t('Record Deleted Successfully.'));
            } else {
                $this->flashMessenger()->addErrorMessage(t('Opration Failed.'));
            }
        } else {
            $this->flashMessenger()->addErrorMessage(t('Opration Failed.'));
        }
        return $this->redirect()->toRoute('products-product', [
            'controller' => 'product',
            'action' => 'list',
            'lang' => $this->tools->getLang()
        ]);
    }
}