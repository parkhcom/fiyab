<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Products\Controller;

use Zend\View\Model\ViewModel;
use Application\Service\BaseAdminController;
use Zend\Mvc\MvcEvent;
use Products\Model\ApplicationAttributeTable;
use Products\Model\ApplicationAttributeOptionTable;
use Products\Form\ApplicationAttributeForm;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use Products\Model\ApplicationShopTable;
use Products\Model\ApplicationShopOwnersTable;
use ZfcDatagrid\Column\Style;

class AttributeController extends BaseAdminController
{

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e, 'products-attribute-');
        if (! $this->userData)
            return $this->redirect()->toRoute('products-attribute');
        
        parent::onDispatch($e);
    }

    public function listAction()
    {
        $applicationShopTable = new ApplicationAttributeTable($this->getServiceLocator());
        $where = array();
        
        $token = $this->tools->token();
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $grid->setTitle(t('Customer Price report'));
        
        $config = $this->getServiceLocator()->get('Config');
        $rowCount = $config['zfcItemsPerPage'];
        $dbAdapter = new Adapter($config['db']);
        
        $groupId = $this->userData->user_group_id;
        $shops = $applicationShopTable->fetch($where);
        
        $grid->setTitle(t('Shop List'));
        $grid->setDefaultItemsPerPage($rowCount);
        $grid->setDataSource($shops, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(t('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        
        $style = new Style\BackgroundColor(array(
            200,
            100,
            100
        ));
        $style = new Style\BackgroundColor(array(
            200,
            100,
            100
        ));
        $style->addByValue($col, 'male');
        $col->addStyle($style);
        $col->setTranslationEnabled(true);
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(t('Name'));
        $col->setWidth(15);
        $grid->addColumn($col);
        
        $col = new Column\Select('username');
        $col->setLabel(t('Shop Keeper'));
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $col = new Column\Select('url');
        $col->setLabel(t('Url'));
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $status = new Column\Select('status');
        $status->setLabel(t('Status'));
        $options = array(
            '0' => t('No'),
            '1' => t('Yes')
        );
        $status->setFilterSelectOptions($options);
        $replaces = array(
            '0' => t('No'),
            '1' => t('Yes')
        );
        $status->setReplaceValues($replaces, $col);
        $grid->addColumn($status);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(t('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink('/attribute/edit/id/' . $rowId);
        $viewAction->setTooltipTitle(t('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink('/attribute/delete/id/' . $rowId . '/token/' . $token);
        
        $viewAction2->setTooltipTitle(t('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(t('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        $block[] = '<a href="/attribute/add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . t("Add") . '</a>';
        $grid->setLink($block);
        
        $grid->getRenderer()->setTemplate('products/attribute/list');
        $grid->render();
        
        return $grid->getResponse();
    }
    public function addAction()
    {
        $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
        $aplicationAttributeOptionTable = new ApplicationAttributeOptionTable($this->getServiceLocator());
        $parrentAttributes = $applicationAttributeTable->fetch([
            'is_parent' => 1
        ]);
        $applicationAttributeform = new ApplicationAttributeForm($parrentAttributes, false);
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $applicationAttributeform->setData($postData);
            if ($applicationAttributeform->isValid()) {
                $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
                $validData = $applicationAttributeform->getData();
                $validData['name'] = (string) $validData['name'];
                if ($applicationAttributeTable->checkExist(trim($validData['name']), null)) {
                    $view['error'] = t('Record With This Name Already Exists');
                } else {
                    $addedId = $applicationAttributeTable->add($validData);
                    if ($addedId !== false) {
                        if ($validData['type'] != 'text') {
                            if (isset($postData['options']) && $postData['options']) {
                                foreach ($postData['options'] as $key => $options) {
                                    if (isset($postData['values'][$key])) {
                                        $data = [
                                            'attribute_id' => $addedId,
                                            'value' => $postData['values'][$key],
                                            'label' => $options,
                                            'image' => ''
                                        ];
                                        $aplicationAttributeOptionTable->add($data);
                                    }
                                }
                            }
                        }
                        $this->flashMessenger()->addSuccessMessage(t('Data inserted successfully'));
                        if ($postData['submitandnew']) {
                            return $this->redirect()->toRoute('products-attribute', [
                                'controller' => 'attribute',
                                'action' => 'add',
                                'lang' => $this->tools->getLang()
                            ]);
                        } else {
                            return $this->redirect()->toRoute('products-attribute', [
                                'controller' => 'attribute',
                                'action' => 'list',
                                'lang' => $this->tools->getLang()
                            ]);
                        }
                    } else {
                        $view['error'] = t('Operation Failed');
                    }
                }
            }
        }
        
        $view['form'] = $applicationAttributeform;
        return new ViewModel($view);
    }
    public function editAction()
    {
        $id = $this->params('id', - 1);
        $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
        $aplicationAttributeOptionTable = new ApplicationAttributeOptionTable($this->getServiceLocator());
        $parrentAttributes = $applicationAttributeTable->fetch([
            'is_parent' => 1
        ]);
        $attributeData = $applicationAttributeTable->fetch([
            'id' => $id
        ]);
        $attributeOptionData = $aplicationAttributeOptionTable->fetch([
            'attribute_id' => $id
        ]);
        
        if (! $attributeData) {
            $this->flashMessenger()->addErrorMessage(t('No Such Field Exist'));
            $this->flashMessenger()->addSuccessMessage(t());
            return $this->redirect()->toRoute('products', [
                'controller' => 'attribute',
                'action' => 'list'
            ]);
        }
        $attributeData = $attributeData[0];
        $view['name'] = $attributeData['name'];
        $applicationAttributeform = new ApplicationAttributeForm($parrentAttributes, false);
        $applicationAttributeform->populateValues($attributeData);
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $applicationAttributeform->setData($postData);
            if ($applicationAttributeform->isValid()) {
                $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
                $validData = $applicationAttributeform->getData();
                $validData['name'] = (string) $validData['name'];
                if ($applicationAttributeTable->checkExist(trim($validData['name']), null)) {
                    $view['error'] = t('Record With This Name Already Exists');
                } else {
                    $isUpdate = $applicationAttributeTable->update($id,$validData);
                    if ($isUpdate !== false) {
                        $aplicationAttributeOptionTable->deleteAttributeId($id);
                        if ($validData['type'] != 'text') {
                            if (isset($postData['options']) && $postData['options']) { 
                                foreach ($postData['options'] as $key => $options) {
                                    if (isset($postData['values'][$key])) {
                                        $data = [
                                            'attribute_id' => $id,
                                            'value' => $postData['values'][$key],
                                            'label' => $options,
                                            'image' => ''
                                        ];
                                        $aplicationAttributeOptionTable->add($data);
                                    }
                                }
                            }
                        } 
                        $this->flashMessenger()->addSuccessMessage(t('Data Updated successfully'));
                        return $this->redirect()->toRoute('products-attribute', [
                            'controller' => 'attribute',
                            'action' => 'list',
                            'lang' => $this->tools->getLang()
                        ]);
                    } else {
                        $view['error'] = t('Operation Failed');
                    }
                }
            }
        }
        
        $view['options'] = $attributeOptionData;
        $view['form'] = $applicationAttributeform;
        return new ViewModel($view);
    }

    public function deleteAction()
    {
        if ($this->tools->token('check', $this->params('token'))) {
            $applicationTagsTable = new ApplicationShopTable($this->getServiceLocator());
            $applicationShopOwnersTable = new ApplicationShopOwnersTable($this->getServiceLocator());
            $applicationShopOwnersTable->delete($this->params('shop_id', - 1));
            $isDeleted = $applicationTagsTable->delete($this->params('id', - 1));
            if ($isDeleted !== false) {
                $this->flashMessenger()->addSuccessMessage(t('Record Deleted Successfully.'));
            } else {
                $this->flashMessenger()->addErrorMessage(t('Opration Failed.'));
            }
        } else {
            $this->flashMessenger()->addErrorMessage(t('Opration Failed.'));
        }
        return $this->redirect()->toRoute('products-attribute', [
            'controller' => 'attribute',
            'action' => 'list',
            'lang' => $this->tools->getLang()
        ]);
    }
}
