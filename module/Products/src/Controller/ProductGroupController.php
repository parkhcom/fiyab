<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Products\Controller;

use Zend\View\Model\ViewModel;
use Application\Service\BaseAdminController;
use Zend\Mvc\MvcEvent;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use Products\Model\ApplicationProductGroupTable;
use Products\Form\ApplicationProductGroupForm;
use Application\Helper\ImageUploader;
use Products\Model\ApplicationAttributeTable;

class ProductGroupController extends BaseAdminController // implements CrudInterface
{

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e, 'products-product-group-');
        if (! $this->userData)
            return $this->redirect()->toRoute('products-product-group');
        
        parent::onDispatch($e);
    }

    public function editAction()
    {
        $id = $this->params('id', - 1);
        
        $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
        $applicationProductGroupTable = new ApplicationProductGroupTable($this->getServiceLocator());
        $groups = $applicationProductGroupTable->fetch([
            'is_parent' => 1
        ]);
        
        $thisGroupData = $applicationProductGroupTable->fetch([
            'id' => $id
        ]);
        
        if (! $thisGroupData) {
            $this->flashMessenger()->addErrorMessage(t('No Such Field Exist'));
            return $this->redirect()->toRoute('products-product-group', [
                'controller' => 'product-group',
                'action' => 'list',
                'lang' => $this->tools->getLang()
            ]);
        }
        $thisGroupData = $thisGroupData[0];
        $view['name'] = $thisGroupData['name'];
        $view['image'] = $thisGroupData['avatar'];
        
        $groupForm = new ApplicationProductGroupForm($groups, true);
        $groupForm->populateValues($thisGroupData);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $files = $request->getFiles()->toArray();
            $groupForm->setData($postData);
            
            if ($groupForm->isValid()) {
                $postData = $groupForm->getData();
                if (isset($files['avatar']) && empty($files['avatar']['name'])) {
                    $postData['avatar'] = $thisGroupData['avatar'];
                } else {
                    $config = $this->getServiceLocator()->get('config');
                    $uploadDir = $config['base_route'] . "/uploads/productGroup/";
                    $validationExt = "jpg,jpeg,png,gif";
                    $uploadImages = new ImageUploader();
                    $uploadResult = $uploadImages->UploadFile($files, $validationExt, $uploadDir, 'avatar');
                    if ($uploadResult) {
                        $uploadResult = $uploadResult[0];
                        $error = $uploadResult->error;
                        if (! $error) {
                            $postData['avatar'] = $uploadResult->getFileName();
                            @unlink($uploadDir . $thisGroupData['avatar']);
                        }
                    }
                }
                
                $addedId = $applicationProductGroupTable->update($id, $postData);
                if ($addedId !== false) {
                    $this->flashMessenger()->addSuccessMessage(t('Data Update successfully'));
                    
                    return $this->redirect()->toRoute('products-product-group', [
                        'controller' => 'product-group',
                        'action' => 'list',
                        'lang' => $this->tools->getLang()
                    ]);
                } else {
                    $this->flashMessenger()->addErrorMessage(t('Data Update error'));
                }
            } else { 
              /*   $t = $groupForm->getMessages();
                var_dump($t);
                die(); */
                
                $this->flashMessenger()->addErrorMessage(t('Form Submitted Is not Orgin Data error'));
            }
        }
        $view['form'] = $groupForm;
        return new ViewModel($view);
    }

    public function addAction()
    {
        $applicationAttributeTable = new ApplicationAttributeTable($this->getServiceLocator());
        $applicationProductGroupTable = new ApplicationProductGroupTable($this->getServiceLocator());
        $groups = $applicationProductGroupTable->fetch([
            'is_parent' => 1
        ]);
        if ($groups) {
            foreach ($groups as &$group) {
                if ($group['parent_id'] > 0) {
                    $groupData = $applicationProductGroupTable->getGroupName([
                        'id' => $group['parent_id']
                    ]);
                    $group['parent_name'] = $groupData['name'];
                }
            }
        }
        
        $groupForm = new ApplicationProductGroupForm($groups, false);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $files = $request->getFiles()->toArray();
            $config = $this->getServiceLocator()->get('config');
            $uploadImage = new ImageUploader();
            
            $groupForm->setData($postData);
            if ($groupForm->isValid()) {
                $postData = $groupForm->getData();
                if (isset($files['avatar']) && empty($files['avatar']['name'])) {
                    $postData['avatar'] = '';
                } else {
                    $uploadDir = $config['base_route'] . "/uploads/productGroup/";
                    $validationExt = "jpg,jpeg,png,gif";
                    $uploadImages = new ImageUploader();
                    $uploadResult = $uploadImage->UploadFile($files, $validationExt, $uploadDir, 'avatar');
                    if ($uploadResult) {
                        $uploadResult = $uploadResult[0];
                        $error = $uploadResult->error;
                        if (! $error) {
                            $postData['avatar'] = $uploadResult->getFileName();
                        }
                    }
                }
                
                $addedId = $applicationProductGroupTable->add($postData);
                if ($addedId) {
                    $this->flashMessenger()->addSuccessMessage(t('Data inserted successfully'));
                    if ($postData['submitandnew']) {
                        return $this->redirect()->toRoute('products-product-group', [
                            'controller' => 'product-group',
                            'action' => 'add',
                            'lang' => $this->tools->getLang()
                        ]);
                    } else {
                        return $this->redirect()->toRoute('products-product-group', [
                            'controller' => 'product-group',
                            'action' => 'list',
                            'lang' => $this->tools->getLang()
                        ]);
                    }
                } else {
                    $this->flashMessenger()->addErrorMessage(t('Data insert error'));
                }
            } else {
                
                $t = $groupForm->getMessages();
                var_dump($t);
                die();
                
                $this->flashMessenger()->addErrorMessage('Form Data error');
            }
        }
        $view['form'] = $groupForm;
        return new ViewModel($view);
    }

    public function listAction()
    {
        $applicationProductGroupTable = new ApplicationProductGroupTable($this->getServiceLocator());
        $where = array();
        
        $token = $this->tools->token();
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $rowCount = $config['zfcItemsPerPage'];
        
        $productgroups = $applicationProductGroupTable->fetch($where);
        
        $grid->setTitle(t('ProductGroup List'));
        $grid->setDefaultItemsPerPage($rowCount);
        $grid->setDataSource($productgroups, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(t('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(t('Name'));
        $col->setWidth(15);
        $grid->addColumn($col);
        
        $col = new Column\Select('order');
        $col->setLabel(t('Order'));
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $status = new Column\Select('active');
        $status->setLabel(t('Status'));
        $options = array(
            '0' => t('No'),
            '1' => t('Yes')
        );
        $status->setFilterSelectOptions($options);
        $replaces = array(
            '0' => t('No'),
            '1' => t('Yes')
        );
        $status->setReplaceValues($replaces, $col);
        $grid->addColumn($status);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(t('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink('/product-group/edit/id/' . $rowId);
        $viewAction->setTooltipTitle(t('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink('/product-group/delete/id/' . $rowId . '/token/' . $token);
        
        $viewAction2->setTooltipTitle(t('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(t('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        $block[] = '<a href="/product-group/add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . t("Add") . '</a>';
        $grid->setLink($block);
        
        $grid->getRenderer()->setTemplate('products/product-group/list');
        $grid->render();
        
        return $grid->getResponse();
    }

    public function deleteAction()
    {
        if ($this->tools->token('check', $this->params('token'))) {
            $applicationTagsTable = new ApplicationProductGroupTable($this->getServiceLocator());
            
            $isDeleted = $applicationTagsTable->delete($this->params('id', - 1));
            if ($isDeleted !== false) {
                $this->flashMessenger()->addSuccessMessage(t('Record Deleted Successfully.'));
            } else {
                $this->flashMessenger()->addErrorMessage(t('Opration Failed.'));
            }
        } else {
            $this->flashMessenger()->addErrorMessage(t('Opration Failed.'));
        }
        return $this->redirect()->toRoute('products-product-group', [
            'controller' => 'product-group',
            'action' => 'list',
            'lang' => $this->tools->getLang()
        ]);
    }

    private function uploadImageNew($files, $validationExt, $uploadDir, $index = NULL)
    {
        $validationExt = "jpg,jpeg,png,gif";
        $postImage = array();
        foreach ($files as $k => $file) {
            $newFileName = md5(time() . $file['name']);
            $uploadImage = new ImageUploader();
            
            $isUploaded = $uploadImage->UploadFile($file, $validationExt, $uploadDir, $newFileName);
            var_dump($isUploaded);
            die();
            if ($isUploaded[0])
                $postImage[] = @$isUploaded[1];
        }
        
        if ($index !== NULL && count($postImage))
            return $postImage[$index];
        if (count($postImage))
            return $postImage;
        return null;
    }
}
