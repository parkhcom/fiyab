<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Products\Controller;

use Zend\View\Model\ViewModel;
use Application\Service\BaseAdminController;
use Zend\Mvc\MvcEvent;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use Products\Model\ApplicationShopTable;
use Products\Form\ShopForm;
use Products\Model\ApplicationUsersTable;
use Products\Model\ApplicationShopOwnersTable;

class ShopController extends BaseAdminController
{

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e, 'products-shop-');
        if (! $this->userData)
            return $this->redirect()->toRoute('shop');
        
        parent::onDispatch($e);
    }

    public function editAction()
    {
        $id = (int) $this->params('id', - 1);
        $shopTable = new ApplicationShopTable($this->getServiceLocator());
        $applicationUsersTable = new ApplicationUsersTable($this->getServiceLocator());
        $shopOwnersTable = new ApplicationShopOwnersTable($this->getServiceLocator());
        $shopOwnersTableData = $shopOwnersTable->fetch([
            'shop_id' => $id
        ]);
        
        $groupsConfig = $this->getServiceLocator()->get('config')['groups_config']['shopkeeper'];
        
        if($groupId <= $groupsConfig) {
            $where =[
                'application_shop_owners.user_id'=>$this->userData->id
            ];
        }
        
        $shopTableData = $shopTable->fetch([
            'application_shop.id' => $id
        ]);
        $view['name'] = $shopTableData[0]['name'];
        $view['id'] = $shopTableData[0]['id'];
        
        if ($shopOwnersTableData) {
            $shopTableData[0]['user_id'] = $shopOwnersTableData[0]['user_id'];
        }
        
        if (! $shopTableData || ! $shopOwnersTableData) {
            $this->flashMessenger()->addErrorMessage(t('No Such Field Exist'));
            return $this->redirect()->toRoute('products-shop', [
                'controller' => 'shop',
                'action' => 'list',
                'lang' => $this->tools->getLang()
            ]);
        }
        $applicationUsersTable = new ApplicationUsersTable($this->getServiceLocator());
        
        $shopForm = new ShopForm($userTableData, true);
        $shopForm->populateValues($shopTableData[0]);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $shopForm->setData($postData);
            if ($shopForm->isValid()) {
                $postData = $shopForm->getData();
                $isUpdated = $shopTable->update($id, $postData);
                if ($isUpdated !== false) {
                    $shopOwnersTable->deleteByShopId($id);
                    $data = [
                        'user_id' => $postData['user_id'],
                        'shop_id' => $id
                    ];
                    
                    $ownerId = (int) $shopOwnersTableData[0]['id'];
                    $shopOwnersTable->update($ownerId, $data);
                    $this->flashMessenger()->addSuccessMessage(t('Data Updated successfully'));
                    return $this->redirect()->toRoute('products-shop', [
                        'controller' => 'shop',
                        'action' => 'list',
                        'lang' => $this->tools->getLang()
                    ]);
                } else {
                    $this->flashMessenger()->addErrorMessage(t('Data Updated error'));
                }
            } else {
                
                $this->flashMessenger()->addErrorMessage(t('Form Data error'));
            }
        }
        $view['form'] = $shopForm;
        return new ViewModel($view);
    }

    public function addAction()
    {
        $groupsConfig = $this->getServiceLocator()->get('config')['groups_config']['shopkeeper'];
        $applicationUsersTable = new ApplicationUsersTable($this->getServiceLocator()); 
        $where = [
            'user_group_id' => $groupsConfig, 
        ];
        $groupsConfig = $this->getServiceLocator()->get('config')['groups_config']['shopkeeper'];
        $groupId = $this->userData->user_group_id;
        if($groupId >= $groupsConfig) {
            $where = [
                'id'=>$this->userData->id,
                'user_group_id' => $groupsConfig, 
            ];
        }
        $userTableData = $applicationUsersTable->fetch($where); 
        $shopForm = new ShopForm($userTableData, false);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $shopForm->setData($postData);
            if ($shopForm->isValid()) {
                $shopTable = new ApplicationShopTable($this->getServiceLocator());
                $shopOwnersTable = new ApplicationShopOwnersTable($this->getServiceLocator());
                $postData = $shopForm->getData();
                
                $addedId = $shopTable->add($postData);
                
                if ($addedId) {
                    $data = [
                        'user_id' => $postData['user_id'],
                        'shop_id' => $addedId
                    ];
                    $shopOwnersTable->add($data);
                    $this->flashMessenger()->addSuccessMessage(t('Data inserted successfully'));
                    if ($postData['submitandnew']) {
                        return $this->redirect()->toRoute('products-shop', [
                            'controller' => 'shop',
                            'action' => 'add',
                            'lang' => $this->tools->getLang()
                        ]);
                    } else {
                        return $this->redirect()->toRoute('products-shop', [
                            'controller' => 'shop',
                            'action' => 'list',
                            'lang' => $this->tools->getLang()
                        ]);
                    }
                } else {
                    $this->flashMessenger()->addErrorMessage(t('Data insert error'));
                }
            } else {
                // $t = $shopForm->getMessages();
                // var_dump($t);
                // die();
                $this->flashMessenger()->addErrorMessage(t('Form Data error'));
            }
        }
        $view['form'] = $shopForm;
        return new ViewModel($view);
    }

    public function listAction()
    {
        $applicationShopTable = new ApplicationShopTable($this->getServiceLocator());
        $groupsConfig = $this->getServiceLocator()->get('config')['groups_config']['shopkeeper'];
        $where = array();
        
        $token = $this->tools->token();
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $grid->setTitle(t('Customer Price report'));
        
        $config = $this->getServiceLocator()->get('Config');
        $rowCount = $config['zfcItemsPerPage'];
        $dbAdapter = new Adapter($config['db']);
        
        $groupId = $this->userData->user_group_id;
        if($groupId >= $groupsConfig) {
            $where =[
                'application_shop_owners.user_id'=>$this->userData->id
            ];
        }
        $shops = $applicationShopTable->fetch($where);
        
        $grid->setTitle(t('Shop List'));
        $grid->setDefaultItemsPerPage($rowCount);
        $grid->setDataSource($shops, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(t('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(t('Name'));
        $col->setWidth(15);
        $grid->addColumn($col);
        
        $col = new Column\Select('username');
        $col->setLabel(t('Shop Keeper'));
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $col = new Column\Select('url');
        $col->setLabel(t('Url'));
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $status = new Column\Select('status');
        $status->setLabel(t('Status'));
        $options = array(
            '0' => t('No'),
            '1' => t('Yes')
        );
        $status->setFilterSelectOptions($options);
        $replaces = array(
            '0' => t('No'),
            '1' => t('Yes')
        );
        $status->setReplaceValues($replaces, $col);
        $grid->addColumn($status);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(t('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink('/shop/edit/id/' . $rowId);
        $viewAction->setTooltipTitle(t('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink('/shop/delete/id/' . $rowId . '/token/' . $token); 
        $viewAction2->setTooltipTitle(t('Delete'));
        
        $viewAction3 = new Column\Action\Icon();
        $viewAction3->setIconClass('glyphicon glyphicon-plus');
        $viewAction3->setLink('/product/list/id/' . $rowId );
        $viewAction3->setTooltipTitle(t('Add To My Shop')); 
        
        $actions2 = new Column\Action();
        $actions2->setLabel(t('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction2);
        $actions2->addAction($viewAction3);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        $block[] = '<a href="/shop/add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . t("Add") . '</a>';
        $grid->setLink($block);
        
        $grid->getRenderer()->setTemplate('products/shop/list');
        $grid->render();
        
        return $grid->getResponse();
    }

    public function deleteAction()
    {
        if ($this->tools->token('check', $this->params('token'))) {
            $applicationTagsTable = new ApplicationShopTable($this->getServiceLocator());
            $applicationShopOwnersTable = new ApplicationShopOwnersTable($this->getServiceLocator());
            $applicationShopOwnersTable->delete($this->params('shop_id', - 1));
            $isDeleted = $applicationTagsTable->delete($this->params('id', - 1));
            if ($isDeleted !== false) {
                $this->flashMessenger()->addSuccessMessage(t('Record Deleted Successfully.'));
            } else {
                $this->flashMessenger()->addErrorMessage(t('Opration Failed.'));
            }
        } else {
            $this->flashMessenger()->addErrorMessage(t('Opration Failed.'));
        }
        return $this->redirect()->toRoute('products-shop', [
            'controller' => 'shop',
            'action' => 'list',
            'lang' => $this->tools->getLang()
        ]);
    }
}