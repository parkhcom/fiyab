<?php
namespace Products\Controller;

 
use Zend\Ldap\Attribute; 
use Zend\View\Model\ViewModel; 
use Application\Helper\Generals;
use Zend\Db\Sql\Where;
use Application\Service\BaseAjaxController;
use Zend\Mvc\MvcEvent;
use Products\Model\ApplicationProductGroupTable;
use Products\Model\ApplicationProductTable;
use Products\Model\ApplicationProductListTable;

class AjaxController extends BaseAjaxController
{
    public function onDispatch(MvcEvent $e)
    { 
        $this->initController($e, 'products-ajax-');
        if (! $this->userData)
            return $this->redirect()->toRoute('products-ajax');
            
            parent::onDispatch($e);
    }
    public function checkNameExistAction()
    { 
        $request = $this->getRequest();
        $name = $request->getPost('name'); 
        $applicationProductGroupTable = new ApplicationProductGroupTable($this->getServiceLocator());
        $where = [
            'name' => $name
        ];
        $applicationProductGroupData = $applicationProductGroupTable->fetch($where);
        if($applicationProductGroupData) {
            echo 'isExist'; die;
        } else {
            echo false; die;
        } 
  
    }
    
    public function checkPnameExistAction()
    { 
        $request = $this->getRequest();
        $name = $request->getPost('name');  
        $applicationProductTable = new ApplicationProductListTable($this->getServiceLocator());
        $where = [
            'pname' => $name, 
        ];
        $applicationProductTableData = $applicationProductTable->fetch($where);
 
        if($applicationProductTableData) {
            echo 'isExist'; die;
        } else {
            echo false; die;
        } 
  
    }
 
    
    public function searchProductAction()
    {
        $request = $this->getRequest();
        $ajax = $request->getQuery('ajax');
        $search = $request->getQuery('search');
        $limit = $request->getQuery('limit', 100);
        $productId = $request->getQuery('productId', false);
        $requestId = $request->getQuery('requestId');
        $requestsCheck = $request->getQuery('requestCheck', true);
        if ($ajax == 'true') {
            $productMainTable = $this->getModel("Product", "ProductMainTable");
            $productMainTableData = $productMainTable->searchProductMain($search, $limit, $requestsCheck, $productId, $requestId);
            $view['productList'] = $productMainTableData;
            $this->viewModel->setVariables($view);
            return $this->viewModel;
        }
        return $this->viewModel;
    }
    
    public function searchParrentAction()
    {
        $request = $this->getRequest();
        $setId = $request->getPost('sid');
        $attributeTable = $this->getModel("Product", "AttributeTable");
        
        $attributes = $attributeTable->getAttributesBySetIdForGroup($setId);
        $view['attribute'] = $attributes;
        $this->viewModel->setVariables($view);
        return $this->viewModel;
    }
    
    public function searchGroupAction()
    {
        $request = $this->getRequest();
        $shopId = $request->getPost('shopId', 1);
        $groupTable = $this->getModel("Product", "ProductGroupTable");
        $groupTableData = $groupTable->getGroupList(array(
            'shop_id' => $shopId
        ));
        if ($groupTableData) {
            $groupList = array();
            $i = 0;
            foreach ($groupTableData as $result) {
                $groupList[$i]['id'] = $result['id'];
                $groupList[$i]['name'] = $result['name'];
                $i ++;
            }
            $view['groupList'] = $groupList;
            $this->viewModel->setVariables($view);
            return $this->viewModel;
        }
        return $this->viewModel;
    }
    
    public function searchProductsAction()
    {
        $request = $this->getRequest();
        $shopId = $request->getPost('shopId', 1);
        $productsTable = $this->getModel("Product", "ProductViewTable");
        $productsTableData = $productsTable->getProductsList($shopId);
        if ($productsTableData) {
            $groupList = array();
            $i = 0;
            foreach ($productsTableData as $result) {
                $groupList[$i]['id'] = $result['id'];
                $groupList[$i]['name'] = $result['name'];
                $i ++;
            }
            $view['productsList'] = $groupList;
            $this->viewModel->setVariables($view);
            return $this->viewModel;
        }
        return $this->viewModel;
    }
    
    public function searchAttributesAction()
    {
        $productMainAttributes = array();
        $final = array();
        $consquence = array();
        $productGroupAttributes = array();
        $i = 0;
        $j = 0;
        $request = $this->getRequest();
        $mainProductId = $request->getPost('id');
        // GET PRODUCT MAIN ATTR -
        // $result
        // keep
        // main
        // product
        // attributes
        $productMainAttributeTable = $this->getModel('Product', 'ProductMainAttributeTable');
        $productMainAttributeTableData = $productMainAttributeTable->getProductMainAttributesWithProductName($mainProductId);
        
        if (! empty($productMainAttributeTableData)) {
            foreach ($productMainAttributeTableData as $value) {
                $productMainAttributes[$value['attribute_id']]['attribute_id'] = $value['attribute_id'];
                $productMainAttributes[$value['attribute_id']]['name'] = $value['name'];
                $productMainAttributes[$value['attribute_id']]['value'] = $value['value'];
            }
        }
        
        // GET THE ATTRIBUTES OF each Group That Involve Product FIND THESE GROUPS
        $productsGroupProductMainTable = $this->getModel("Product", "ProductGroupProductMainTable");
        $productsGroupProductMainTableData = $productsGroupProductMainTable->getProductGroups($mainProductId);
        
        foreach ($productsGroupProductMainTableData as $val) {
            $groupId = $val['group_id'];
            $productGroupAttributeTable = $this->getModel("Product", "ProductGroupAttributeTable");
            $productGroupAttributeData = $productGroupAttributeTable->getProductGroupAttributesValue($groupId);
            if ($productGroupAttributeData) {
                foreach ($productGroupAttributeData as $attributeData) {
                    $consquence[] = array(
                        'product_group_id' => $attributeData['product_group_id'],
                        'attribute_id' => $attributeData['attribute_id'],
                        'name' => $attributeData['name'],
                        'value' => $attributeData['value']
                    );
                }
            }
        }
        /*
         * GET THE
         * // ATTRIBUTE
         * // OF
         * // GROUP
         * // AND
         * // MAKE
         * // THEM
         * // UNIQUE BY PRIORITY OF product_group_id
         *
         */
        foreach ($consquence as $key => $value) {
            if ($key == 0) {
                $productGroupAttributes[$value["attribute_id"]] = $value;
            } else {
                if (isset($productGroupAttributes[$value["attribute_id"]]) && $value["product_group_id"] > $productGroupAttributes[$value["attribute_id"]]["product_group_id"]) {
                    $productGroupAttributes[$value["attribute_id"]] = $value;
                } else
                    if (! isset($productGroupAttributes[$value["attribute_id"]])) {
                        $productGroupAttributes[$value["attribute_id"]] = $value;
                    }
            }
        }
 
        foreach ($productGroupAttributes as $productGroupAttributeId => $productGroupAttribute) {
            if (array_key_exists($productGroupAttributeId, $productMainAttributes)) {
                continue;
            }
            $productMainAttributes[$productGroupAttributeId] = $productGroupAttribute;
        }
        ksort($productMainAttributes);
  
        $view['staticAttributes'] = $productMainAttributes;
        $this->viewModel->setVariables($view);
        return $this->viewModel;
    }
    
    public function searchOtherAttributesAction()
    {
        $attributeTable = $this->getModel("Product", "AttributeTable");
        $attributeOptionTable = $this->getModel("Product", "AttributeOptionTable");
        $request = $this->getRequest();
        $attributeId = $request->getQuery('attributeId');
        $search = $request->getQuery('search');
        
        $attributeData = $attributeTable->getAttributesList($search, $attributeId, 'product_view', $limit = 10);
        if ($attributeData) {
            foreach ($attributeData as & $attribute) {
                if ($attribute['type'] != 'text') {
                    $attributeOptionsData = $attributeOptionTable->findByAttributeId($attribute['id']);
                    if ($attributeOptionsData) {
                        $attribute['options'] = $attributeOptionsData;
                    } else {
                        $attribute['options'] = array();
                    }
                } else {
                    $attribute['options'] = array();
                }
            }
        }
        $view['otherAttributes'] = $attributeData;
        $this->viewModel->setVariables($view);
        return $this->viewModel;
    }
    
    public function searchAllProductMainAction()
    {
        $mainProduct = array();
        $productMainInfo = array();
        $emptyProducts = array();
        $finalResult = array();
        $emptyProductsequal = array();
        $request = $this->getRequest();
        $productMainId = $request->getPost('productMainId');
        $ajax = $request->getPost('ajax');
        $editmode = $request->getPost('editmode');
        $selectedProducts = $request->getPost('products', null);
        $search = $request->getPost('search');
        $isAccessory = $request->getPost('isAccessory');
       
        $productId = $request->getPost('productId', false);
        $limit = $request->getPost('limit', 50);
        $requestsCheck = $request->getPost('requestCheck', true);
        $productMainTable = $this->getModel("Product", "ProductMainTable");
        
        // TODO  search  all product  views also search all product  mains
        if ($editmode) {
            $productMainTableData = $productMainTable->searchAllProductMain($search, $limit, $requestsCheck, $productId, $productMainId, $isAccessory, true);
        } else {
            $productMainTableData = $productMainTable->searchAllProductMain($search, $limit, $requestsCheck, $productId, $productMainId, $isAccessory);
        }
         
        $productMainForceAttributes = array();
        if ($productMainTableData) {
            $productGroupProductMainTable = $this->getModel("Product", "ProductGroupProductMainTable");
            foreach ($productMainTableData as $productMain) {
                $productMainForceAttributes[$productMain['id']] = $productGroupProductMainTable->getProductMainForceAttributes($productMain['id']);
            }
        } else {
            return $this->viewModel;
        }
        $productMainData = array();
        $eachMainProductsequal = array();
          
        if ($productMainForceAttributes) {
            foreach ($productMainForceAttributes as $productMainId => $productData) {
                if ($productData) {
                    foreach ($productData as $attribute) {
                        $productMainData[$productMainId]['groups'][$attribute['product_group_id']] = $attribute['product_group_id'];
                        if ($attribute['is_force']) {
                            $productMainData[$productMainId]['forceAttributes'][$attribute['attribute_id']] = $attribute['product_attribute_value'];
                            $productMainData[$productMainId]['forceAttributeIds'][] = $attribute['attribute_id'] . '-' . $attribute['product_group_id'];
                        } else {
                            /* if product group has attribute which  product  main  has  not, then  ignore  that  attribute  Because  LEFT JOIN*/
                            
                            if ($attribute['product_attribute_value'] !== null) {
                                $productMainData[$productMainId]['notForceAttributes'][$attribute['attribute_id']] = $attribute['product_attribute_value'];
                            }
                        }
                        $productMainData[$productMainId]['mainProduct']['id'] = $attribute['ProductMainId'];
                        $productMainData[$productMainId]['mainProduct']['shop_id'] = $attribute['shop_id'];
                        $productMainData[$productMainId]['mainProduct']['name'] = $attribute['name'];
                        $productMainData[$productMainId]['mainProduct']['is_accessory'] = $attribute['is_accessory'];
                    }
                } else {
                    // Never
                    // happen
                    /*
                     *
                     * $producttMainTable
                     * =
                     * $this->getModel(
                     * "Product",
                     * "ProductMainTable"
                     * );
                     * $emptyProductDetail
                     * =
                     * $producttMainTable->findById($productMainId);
                     * $emptyMainProductsequal[$productMainId]
                     * =
                     * $emptyProductDetail;
                     * $emptyProductsequal
                     * =
                     * $emptyProductDetail;
                     * unset($productMainData[$productMainId]);
                     */
                }
            }
            // ksort($productMainData);
            
            $viewProducts = array();
            if ($productMainData) {
                foreach ($productMainData as $productMainId => $productMainInfo) {
                    $equalDeletedProductId = array();
                    foreach ($productMainData as $compareProductMainId => $compareProductMainInfo) {
                        if ($compareProductMainId == $productMainId) {
                            $sameProductMains[$productMainId][] = $productMainInfo;
                            unset($productMainData[$productMainId]);
                            continue;
                        }
                        if (isset($productMainInfo['forceAttributeIds']) && isset($compareProductMainInfo['forceAttributeIds'])) {
                            // if
                            // product
                            // attributes
                            // (even
                            // by
                            // group)
                            // is
                            // same
                            // with
                            // another
                            // product
                            // attributes
                            // (even
                            // by
                            // group)
                            // THESE
                            // products
                            // is
                            // same
                            if (array_diff($productMainInfo['forceAttributeIds'], $compareProductMainInfo['forceAttributeIds'])) {
                                continue;
                            } else {
                                $sameProductMains[$productMainId][] = $compareProductMainInfo;
                                unset($productMainData[$compareProductMainId]);
                            }
                        }
                    }
                }
            } else {
                return $this->viewModel;
            }
        } else {
            // There
            // is
            // no
            // product
            // main
            return $this->viewModel;
        }
        
        // this is for getting product main prices from request product main
        if ($sameProductMains) {
            $mainTable = $this->getModel("Product", "ProductMainTable");
            foreach ($sameProductMains as $key1 => &$productDataexe) {
                foreach ($productDataexe as $key2 => $productDataIni) {
                    foreach ($productDataIni as $key => $productMain) {
                        if ($key == 'mainProduct') {
                            $requestProductMainTableData = $mainTable->getProductMainPrice($productMain['id']);
                            // var_dump($requestProductMainTableData);die;
                            $price = array();
                            if ($requestProductMainTableData) {
                                foreach ($requestProductMainTableData as $priceData) {
                                    // var_dump($priceData);
                                    if (isset($priceData['price']) && $priceData) {
                                        $prices = explode('.',  $priceData['price']);
                                        $price[] = $prices[0];
                                    }
                                }
                                // insert price range to array
                                if (! empty($price)) {
                                    $count = COUNT($price);
                                    if (COUNT($price) > 1) {
                                        $ranagePrice = $price[0] . "-" . $price[$count - 1];
                                    } else {
                                        $ranagePrice = $price[0];
                                    }
                                    $sameProductMains[$key1][$key2][$key]['price'] = $ranagePrice;
                                } else {
                                    $sameProductMains[$key1][$key2][$key]['price'] = " ";
                                }
                            } else {
                                $sameProductMains[$key1][$key2][$key]['price'] = " ";
                            }
                        }
                    }
                }
            }
        }
        
        $view['sameProductList'] = $sameProductMains;
        $this->viewModel->setVariables($view);
        return $this->viewModel;
    }
    
    public function searchSameAttributesAction()
    {
        /*
         *
         * @var
         * $productMainTable
         * \Product\Model\ProductMainAttributeTable
         */
        $mainOptionalAttribute = array();
        $request = $this->getRequest();
        $mainProductId = $request->getPost('id');
        $productMainAttributeTable = $this->getModel("Product", "ProductMainAttributeTable");
        $productMainAttributeTableData = $productMainAttributeTable->getProductMainAttributesWithAttributeName($mainProductId);
        if ($productMainAttributeTableData) {
            $productMainTable = $this->getModel("Product", "ProductGroupProductMainTable");
            $productMainTableData = $productMainTable->getSameProductMainAttributesGroups($mainProductId);
            foreach ($productMainTableData as $group => $groupAttribute) {
                foreach ($productMainAttributeTableData as $productMainAttribute) {
                    if ($groupAttribute['attribute_id'] == $productMainAttribute['attribute_id']) {
                        unset($productMainTableData[$group]);
                    }
                }
            }
            if ($productMainTableData) {
                foreach ($productMainTableData as $remainGroupAttrinute)
                    $productMainAttributeTableData[] = $remainGroupAttrinute;
            }
            foreach ($productMainAttributeTableData as $productMainKey => $devideAttributrs) {
                $sameDetail = $devideAttributrs['product_main_id'];
                $mainOptionalAttribute[$productMainKey]['attribute_id'] = $devideAttributrs['attribute_id'];
                $mainOptionalAttribute[$productMainKey]['value'] = $devideAttributrs['value'];
                $mainOptionalAttribute[$productMainKey]['name'] = $devideAttributrs['name'];
            }
        } else {
            $productMainTable = $this->getModel("Product", "ProductGroupProductMainTable");
            $productMainTableData = $productMainTable->getSameProductMainAttributesGroups($mainProductId);
            foreach ($productMainTableData as $productMainKey => $groupDevideAttributrs) {
                $sameDetail = $groupDevideAttributrs['product_main_id'];
                $mainOptionalAttribute[$productMainKey]['attribute_id'] = $groupDevideAttributrs['attribute_id'];
                $mainOptionalAttribute[$productMainKey]['value'] = $groupDevideAttributrs['value'];
                $mainOptionalAttribute[$productMainKey]['name'] = $groupDevideAttributrs['name'];
            }
        }
        $view['sameProducts'] = $mainOptionalAttribute;
        $this->viewModel->setVariables($view);
        return $this->viewModel;
    }
    
    public function searchAccessoriesAction()
    {
        $rquest = $this->getRequest();
        $ajax = $rquest->getQuery('ajax');
        $search = $rquest->getQuery('search');
        $productId = $rquest->getQuery('productId', false);
        $limit = $rquest->getQuery('limit', 10);
        $requestsCheck = $rquest->getQuery('requestCheck', true);
        if ($ajax == 'true') {
            $productMainTable = $this->getModel("Product", "ProductMainTable");
            $productMainTableData = $productMainTable->searchAccessoryProducts($search, $limit, $requestsCheck, $productId);
            $view['productList'] = $productMainTableData;
            $this->viewModel->setVariables($view);
            return $this->viewModel;
        }
        return $this->viewModel;
    }
    
    public function searchSelectedProductsAction()
    {
        $request = $this->getRequest();
        $supplierId = $request->getPost('supplierId');
        $productMainTable = $this->getModel("Invoice", "BuyInvoiceTable");
        $productMainTableData = $productMainTable->getProductListFromBuyInvoiceByInvoiceId($supplierId);
        $view['productList'] = $productMainTableData;
        $this->viewModel->setVariables($view);
        return $this->viewModel;
    }
    
    public function searchProductMainProductViewAction()
    {
        $access = $this->getAccess('orders-orders-add-manage-discount');
        $today = date("Y-m-d H:i:s");
        $specialsResult = array();
        $specialsArray = array();
        $NotForceArray = array();
        $taxes = array();
        $forceArray = array();
        $groupDataArray = array();
        $allProductIsForceMainList = array();
        $allProductMainNotIsForceList = array();
        $rquest = $this->getRequest();
        $ajax = $rquest->getQuery('ajax');
        $search = $rquest->getQuery('search');
        $productId = $rquest->getQuery('productId', false);
        $limit = $rquest->getQuery('limit', 30);
        $requestsCheck = $rquest->getQuery('requestCheck', true);
        if ($ajax == 'true') {
            $productMainProductViewTable = $this->getModel("Product", "ProductMainProductViewTable");
            $productMainProductViewTableData = $productMainProductViewTable->searchProductMainProductView($search, $limit, $requestsCheck);
            if ($productMainProductViewTableData) {
                foreach ($productMainProductViewTableData as $mainProduct) {
                    $tax = $this->calculateTaxOfProductMain($mainProduct['product_main_id']);
                    // GET
                    // THE
                    // GROUP
                    // BY
                    // PRODUCT
                    // MAIN
                    // ID
                    // FOR
                    // GETTING
                    // SPECIALS
                    // OF
                    // GROUP
                    // IF
                    // EXIST
                    $productGroupProductMainTable = $this->getModel('Product', 'ProductGroupProductMainTable');
                    $productGroupProductMainTableData = $productGroupProductMainTable->getCompleteDataOfGroupsByProductViewId($mainProduct['product_main_id']);
                    // $mainProduct['product_main_id']
                    $taxes[$mainProduct['product_main_id']] = $tax;
                    if ($productGroupProductMainTableData) {
                        foreach ($productGroupProductMainTableData as $groupData) {
                            $groupDataArray[$groupData['product_main_id']][] = $groupData['group_id'];
                        }
                    }
                    $specialsRelationsTable = $this->getModel('Specials', 'SpecialsRelationsTable');
                    if ($groupDataArray) {
                        foreach ($groupDataArray as $groupsData) {
                            if ($groupsData) {
                                foreach ($groupsData as $groups) {
                                    $typeGroup = "group";
                                    $specialsRelationsTableData = $specialsRelationsTable->getSpecialsRelationByTypeAndRealation($today, $groups, $typeGroup);
                                    
                                    if ($specialsRelationsTableData) {
                                        $specialsResult[$mainProduct['id']]['groupSpecial']['productViewId'] = $mainProduct['product_view_id'];
                                        $specialsResult[$mainProduct['id']]['groupSpecial'][] = $specialsRelationsTableData;
                                    }
                                }
                            }
                        }
                    }
                    
                    // GET THE PRODUCT SPECIALS BY PRODUCT VIEW ID IF EXIST
                    $typeProduct = "product";
                    // var_dump($mainProduct['product_view_id']);
                    $specialsRelations = $specialsRelationsTable->getSpecialsRelationByTypeAndRealation($today, $mainProduct['product_view_id'], $typeProduct);
                    // $specialsRelations['price']
                    // =
                    // $mainProduct['mainPrice'];
                    if ($specialsRelations) {
                        $specialsResult[$mainProduct['id']]['productSpecial']['productViewId'] = $mainProduct['product_view_id'];
                        $specialsResult[$mainProduct['id']]['productSpecial'][] = $specialsRelations;
                    }
                    
                    /*
                     * in this place we should find best discount between
                     * group
                     * and
                     * product
                     * i
                     * found
                     * that
                     * if
                     * $specialsResult
                     * contan
                     * above
                     * data
                     * some
                     * data
                     * like
                     * as
                     * title
                     * going
                     * to
                     * be
                     * same
                     * so
                     * ->
                     * i
                     * desided
                     * to
                     * choose
                     * one
                     * between
                     * these
                     * (product
                     * &
                     * group)
                     * for
                     * example
                     * if
                     * the
                     * group
                     * of
                     * product
                     * and
                     * also
                     * the
                     * product
                     * have
                     * discount
                     * i
                     * select
                     * one
                     * that
                     * customer
                     * should
                     * pay
                     * less
                     * by
                     * writing
                     * following
                     * code
                     */
                    /*
                     * if ($specialsResult) {
                     * if ((isset($specialsResult[$mainProduct['id']]['productSpecial'])) && $specialsResult[$mainProduct['id']]['groupSpecial']) {
                     * foreach ($specialsResult[$mainProduct['id']]['groupSpecial'] as $keyGroup => $groupOfProduct) {
                     * if ($keyGroup !== 'productViewId') {
                     * $this->findPercentOrAmount($groupOfProduct);
                     * }
                     * }
                     * }
                     * }
                     */
                    
                    $forceArray = $productMainProductViewTable->searchProductMainProductView($search, $limit, $requestsCheck, $accessory = 1, $isForce = 1, $mainProduct['product_view_id']);
                    // var_dump($forceArray);
                    if (! empty($forceArray)) {
                        foreach ($forceArray as $key => $forces) {
                            $forceTax = $this->calculateTaxOfProductMain($forces['product_main_id']);
                            $forceArray[$key]['tax'] = $forceTax;
                        }
                        $allProductIsForceMainList[$mainProduct['product_view_id']] = $forceArray;
                    }
                    
                    $NotForceArray = $productMainProductViewTable->searchProductMainProductView($search, $limit, $requestsCheck, $accessory = 1, $isForce = 0, $mainProduct['product_view_id']);
                    if (! empty($NotForceArray)) {
                        foreach ($NotForceArray as $keyNotForce => $notForces) {
                            $notForceTax = $this->calculateTaxOfProductMain($notForces['product_main_id']);
                            $NotForceArray[$keyNotForce]['tax'] = $notForceTax;
                        }
                        $allProductMainNotIsForceList[$mainProduct['product_view_id']] = $NotForceArray;
                    }
                }
            }
            
            if ($specialsResult) {
                foreach ($specialsResult as $pmpvId => $results) {
                    // var_dump($results);
                    if ($results) {
                        foreach ($results as $productOrGroup => $specials) {
                            // var_dump($specials);
                            if ($specials) {
                                foreach ($specials as $mykey => $product) {
                                    // var_dump($product)
                                    // ;
                                    if ($product) {
                                        // var_dump($mykey);
                                        if ($mykey === "productViewId") {
                                            $pvIdNew = $product;
                                        } else
                                            if ($mykey !== "productViewId") {
                                                foreach ($product as $resq) {
                                                    if ($resq['percent'] > 0) {
                                                        $percent = array(
                                                            'percent' => $resq['percent'],
                                                            'title' => $resq['title'],
                                                            'remaining' => $resq['remaining'],
                                                            'product_view_id' => $pvIdNew,
                                                            'specials_property_id' => $resq['specials_property_id']
                                                        );
                                                        $specialsArray[$resq['id']] = $percent;
                                                    } else
                                                        if ($resq['amount'] > 0) {
                                                            $amount = array(
                                                                'amount' => $resq['amount'],
                                                                'title' => $resq['title'],
                                                                'remaining' => $resq['remaining'],
                                                                'product_view_id' => $pvIdNew,
                                                                'specials_property_id' => $resq['specials_property_id']
                                                            );
                                                            $specialsArray[$resq['id']] = $amount;
                                                        }
                                                }
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // inject $specialsArray data inside $productMainProductViewTableData
            if ($productMainProductViewTableData) {
                if ($specialsArray) {
                    foreach ($productMainProductViewTableData as $key => $mainData) {
                        foreach ($specialsArray as $specialRelationId => $specialData) {
                            if ($mainData['product_view_id'] == $specialData['product_view_id']) {
                                $productMainProductViewTableData[$key]['discount'][$specialRelationId]['id'] = $specialRelationId;
                                $productMainProductViewTableData[$key]['discount'][$specialRelationId]['specials_property_id'] = $specialData['specials_property_id'];
                                if (isset($specialData['amount'])) {
                                    $productMainProductViewTableData[$key]['discount'][$specialRelationId]['amount'] = $specialData['amount'];
                                } else {
                                    $productMainProductViewTableData[$key]['discount'][$specialRelationId]['percent'] = $specialData['percent'];
                                }
                                $productMainProductViewTableData[$key]['discount'][$specialRelationId]['title'] = $specialData['title'];
                                $productMainProductViewTableData[$key]['discount'][$specialRelationId]['remaining'] = $specialData['remaining'];
                                if ($taxes) {
                                    foreach ($taxes as $pmId => $taxe) {
                                        if ($mainData['product_main_id'] == $pmId) {
                                            $productMainProductViewTableData[$key]['tax'] = $taxe;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            $view['access'] = $access;
            $view['notIsForceList'] = $allProductMainNotIsForceList;
            $view['isForceList'] = $allProductIsForceMainList;
            $view['mainProductList'] = $productMainProductViewTableData;
            $this->viewModel->setVariables($view);
            return $this->viewModel;
        }
        return $this->viewModel;
    }
    
    function calculateTaxOfProductMain($productMainId)
    {
        $totalTaxValue = 0;
        $taxProductMainTable = $this->getModel('Taxes', 'TaxProductMainTable');
        $taxProductMainTableData = $taxProductMainTable->getProductTaxesValue($productMainId);
        foreach ($taxProductMainTableData as $taxes) {
            $totalTaxValue += $taxes['value'];
        }
        return $totalTaxValue;
    }
    
    public function registerNewProductAction()
    {
        $taxTable = $this->getModel("Taxes", "taxTable");
        $taxTableData = $taxTable->getTaxList();
        $productMainTable = $this->getModel("Product", "ProductGroupTable");
        $productGroupProductMainTable = $this->getModel("Product", "ProductGroupProductMainTable");
        $taxProductMainTable = $this->getModel("Taxes", "TaxProductMainTable");
        $groupsList = $productMainTable->getProductGroupList();
        
        $request = $this->getRequest();
        $post = $request->getPost();
        parse_str($post['dataPass'], $data);
        if (($data['name'] !== "") && ($data['group_id'] !== "") && ($data['taxes'] !== "")) {
            $mainProduct["name"] = $data["name"];
            $mainProduct["is_accessory"] = $data["is_accessory"];
            // TODO
            // fix
            // shop
            // id
            $mainProduct["shop_id"] = 1;
            $productMainTable = $this->getModel("Product", "ProductMainTable");
            $mainProductId = $productMainTable->newMainProduct($mainProduct);
            Generals::setProductMainSession($mainProductId);
            if ($mainProductId) {
                if ($data["group_id"]) {
                    foreach ($data["group_id"] as $group_id) {
                        $productGroupProductMainTable->addProductGroup($mainProductId, $group_id);
                    }
                }
                
                if ($data["taxes"]) {
                    foreach ($data["taxes"] as $taxId) {
                        $taxTableData = $taxTable->getTaxList(array(
                            "id = ?" => $taxId
                        ))->toArray();
                        $insertData["product_main_id"] = $mainProductId;
                        $insertData["tax_id"] = $taxId;
                        $insertData["tax_group"] = $taxTableData[0]["tax_group"];
                        $taxProductMainTable->addTaxProductMain($insertData);
                    }
                }
                // i put product main id in session for dropzone in this way form submit with image
                Generals::setProductMainSession($mainProductId);
            } else {
                $mainProductId = false;
            }
        } else {
            $mainProductId = false;
        }
        $view['mainProductList'] = $mainProductId;
        $this->viewModel->setVariables($view);
        return $this->viewModel;
    }
    
    public function getAttributeChildAction()
    {
        $productMainTable = $this->getModel("Product", "ProductMainTable");
        $request = $this->getRequest();
        $postData = $request->getPost();
        $productId = $request->getPost('productId');
        $attributeId = $request->getPost('attributeId');
        $newAttribute = array();
        $productMainAttributeTable = $this->getModel("Product", "ProductMainAttributeTable");
        $productGroupProductMainTable = $this->getModel("Product", "ProductGroupProductMainTable");
        $attributeTable = $this->getModel("Product", "AttributeTable");
        $productGroupAttributeTable = $this->getModel("Product", "ProductGroupAttributeTable");
        
        $productGroupAttributes = array();
        $attributes = $attributeTable->getAttributesChild($attributeId);
        
        // GET PRODUCT ATTRIBUTE DATA
        $productMainGroups = $productGroupProductMainTable->getProductGroups($productId);
        foreach ($productMainGroups as $productGroup) {
            $productGroupAttributeData = $productGroupAttributeTable->getProductGroupAttributes($productGroup["group_id"]);
            foreach ($productGroupAttributeData as $productMainGroupAttribute) {
                $productGroupAttributes[] = array(
                    "product_main_id" => $productId,
                    "attribute_id" => $productMainGroupAttribute["attribute_id"],
                    "value" => $productMainGroupAttribute["value"],
                    "is_force" => $productMainGroupAttribute["is_force"],
                    "is_parent_attribute" => true, // don't show/preview attribute on top of the product page
                    "preview" => 0
                );
            }
        }
        
        $productMainAttributes = $productMainAttributeTable->getProductMainAttributes($productId);
        $productAttributes = array_merge($productGroupAttributes, $productMainAttributes);
        // ----- AAAAAAAAAAAAAAAAA -----
        foreach ($attributes as $attributeData) {
            foreach ($productAttributes as $productAttr) {
                if ($productAttr['attribute_id'] == $attributeData['id']) {
                    
                    if (isset($productAttr['attribute_option_id'])) {
                        $attributeData['attribute_option_id'] = $productAttr['attribute_option_id'];
                    } else {
                        $attributeData['attribute_option_id'] = '';
                    }
                    $newAttribute[$attributeData['type']][] = $attributeData;
                }
            }
        }
        
        // var_dump($newAttribute);die;
        $view['attributesChild'] = $newAttribute;
        $this->viewModel->setVariables($view);
        return $this->viewModel;
    }
    
    public function searchAllProductMainBannerAction()
    {
        $mainProduct = array();
        $productMainInfo = array();
        $emptyProducts = array();
        $finalResult = array();
        $emptyProductsequal = array();
        $request = $this->getRequest();
        $productMainId = $request->getPost('productMainId');
        $ajax = $request->getPost('ajax');
        $selectedProducts = $request->getPost('products', null);
        $search = $request->getPost('search');
        $productId = $request->getPost('productId', false);
        $limit = $request->getPost('limit', 50);
        $requestsCheck = $request->getPost('requestCheck', true);
        $productMainTable = $this->getModel("Product", "ProductMainTable");
        
        $productMainTableData = $productMainTable->getProductMainFullDataForBanner($search, $limit, $productMainId);
        if ($productMainTableData) {
            foreach ($productMainTableData as $pmData) {
                if ($pmData['type']) {
                    
                    $dataArray = $this->discountCalculation()->findPercentOrAmount($pmData, $pmData['type']);
                    
                    die();
                }
            }
        }
        
        $view['sameProductList'] = $finalResult;
        $this->viewModel->setVariables($view);
        return $this->viewModel;
    }
    public function searchAttributeByParrentSetAction()
    {
        
        $request = $this->getRequest();
        $post = $request->getPost();
        $setId = $post['setId'];
        $attributeTable = $this->getModel("Product", "AttributeTable");
        $where = new Where() ;
        $where->equalTo('is_parent', 1);
        $where->equalTo('attribute_set_id', $setId);
        $attributeTableData = $attributeTable->getAttributesBySet($where);
        
        echo json_encode($attributeTableData);die;
    }
    
    public function registerOrderAction()
    {
        $request = $this->getRequest();
        $groupId = $request->getPost('id');
        $order = $request->getPost('order');
        
        $productGroupTable = $this->getModel("Product", "ProductGroupTable");
        
        if (is_numeric($order)) {
            $attributeTableData = $productGroupTable->setOrder($groupId, $order);
            if ($attributeTableData != false) {
                echo 'yes';
                die;
            } else {
                echo 'no';die;
            }
        } else {
            echo 'no'; die;
        }
        
    }
    
}







