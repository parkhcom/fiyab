<?php
namespace Products;
 
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Helper\Tools;
use Zend\Router\Http\Literal;
 
$tools = new Tools();

return [
    'router' => [
        'routes' => [  
            'product-home' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '[/:lang]/productss',
                    'constraints' => [
                        'lang' => '[a-z]{2}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index',
                        //TODO
                        // temporarily disabled to make dynamic breadcrumb working
                        // this may cause other urls no to work properly
                        // 'lang' => 'en'
                    ]
                ]
            ],
            'products-shop' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop[/:action][/id/:id][/token/:token]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'token' => '[a-f0-9]{32}',
                         'id' => '[1-9][0-9]*'  
                    ], 
                    'defaults' => [
                        'controller' => Controller\ShopController::class,
                        'action' => 'list' 
                    ]
                ]
            ],
            'products-product' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product[/:action][/id/:id][/shopid/:shopid][/token/:token]',
                    'constraints' => [
                        'lang' => '[a-z]{2}',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}',
                        'page' => '[1-9][0-9]*'
                    ],
                    'defaults' => [
                        'controller' => Controller\ProductController::class,
                        'action' => 'list' 
                    ]
                ]
            ],
            'products-product-group' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product-group[/:action][/id/:id][/token/:token][/page/:page]',
                    'constraints' => [
                        'lang' => '[a-z]{2}',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}',
                        'page' => '[1-9][0-9]*'
                    ],
                    'defaults' => [
                        'controller' => Controller\ProductGroupController::class,
                        'action' => 'list' 
                    ]
                ]
            ],
  
            'products-product-main' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product-main[/:action][/id/:id][/token/:token][/page/:page]',
                    'constraints' => [
                        'lang' => '[a-z]{2}',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}',
                        'page' => '[1-9][0-9]*'
                    ],
                    'defaults' => [
                        'controller' => Controller\ProductMainController::class,
                        'action' => 'list' 
                    ]
                ]
            ],
  
          
            'products-attribute' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/attribute[/:action][/id/:id][/token/:token][/page/:page]',
                    'constraints' => [
                        'lang' => '[a-z]{2}',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}',
                        'page' => '[1-9][0-9]*'
                    ],
                    'defaults' => [
                        'controller' => Controller\AttributeController::class,
                        'action' => 'list' 
                    ]
                ]
            ],
  
            'products-ajax' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/ajax[/:action]',
                    'constraints' => [
                        'lang' => '[a-z]{2}',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*' 
                    ] ,
                    'defaults' => [
                        'controller' => Controller\AjaxController::class,
                        'action' => 'check-name-exist'
                    ]
                ]
            ],
  
   
 
        ]
    ],
    'translator' => [
        'locale' => (new Tools())->getLocale(),
        'translation_file_patterns' => [
            /*
             * [
             * 'type' => 'phparray',
             * 'base_dir' => __DIR__ . '/../language',
             * 'pattern' => '%s.php'
             * ]
             */
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo'
            ]
        ]
    ],
    
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\AjaxController::class => InvokableFactory::class,
            Controller\ShopController::class => InvokableFactory::class,
            Controller\ProductController::class => InvokableFactory::class,
            Controller\ProductGroupController::class => InvokableFactory::class,
            Controller\ProductMainController::class => InvokableFactory::class,
            Controller\AttributeController::class => InvokableFactory::class,  
        ], 
        'aliases' => [
            'index' => Controller\IndexController::class,
            'ajax' => Controller\AjaxController::class,
            'shop' => Controller\ShopController::class,
            'product' => Controller\ProductController::class,
            'product-group' => Controller\ProductGroupController::class, 
            'product-main' => Controller\ProductMainController::class, 
            'attribute' => Controller\AttributeController::class, 
        ]
    ],
 
 
    
    'view_manager' => [ 
        
        // It automatically finds the view file but a little bit slower
        'template_path_stack' => [
            __DIR__ . '/../view'
        ]
    ]
];
