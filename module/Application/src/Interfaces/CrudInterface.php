<?php
declare(strict_types = 1);  

namespace Application\Interfaces;

interface CrudInterface

{
    public function fetch (array $where = null);

    public function add(array $data);

    public function update(int $id, array $data);

    public function delete(int $id);
}