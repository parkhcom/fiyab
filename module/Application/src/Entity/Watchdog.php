<?php
namespace Application\Entity;

use Application\Helper\Jdates;
use Application\Model\ApplicationWatchdogTable;

class Watchdog
{

    private $serviceLocator;

    private $watchdogTable;

    private $user;

    public function __construct($serviceManager, $user = null)
    {
        $this->serviceLocator = $serviceManager;
        $this->watchdogTable = new ApplicationWatchdogTable($serviceManager);
        $this->setUser($user);
    }

    public function setUser($user)
    {
        $this->user = $user;
        
        
        return $this;
    }

    public function log($type = 'notice', $message = '', $data = null, $dataBofore = null, $severity = 6)
    {
        $userId = isset($this->user) ? $this->user['id'] : 0;
//         $postData = array(
//             'user_id' => $userId,
//             'type' => clean_input($type),
//             'message' => clean_input($message),
//             'severity' => (int) $severity,
//             'timestamp' => time(),
//             'location' => clean_input(@$_SERVER['REQUEST_METHOD'] . ' :: ' . @$_SERVER['REQUEST_URI']),
//             'hostname' => clean_input(@$_SERVER['HTTP_HOST']),
//             'referer' => clean_input($_SERVER['HTTP_REFERER']),
//             'data' => serialize($data),
//             'data_before' => serialize($dataBofore)
//         );
        $postData = array(
            'user_id' => $userId,
            'type' => $type,
            'message' => $message,
            'severity' => (int) $severity,
            'timestamp' => time(),
            'location' => @$_SERVER['REQUEST_METHOD'] . ' :: ' . @$_SERVER['REQUEST_URI'],
            'hostname' => @$_SERVER['HTTP_HOST'],
            'referer' => @$_SERVER['HTTP_REFERER'],
            'data' => serialize($data),
            'data_before' => serialize($dataBofore)
        );
        
        if (! $this->watchdogTable)
            $this->watchdogTable = new ApplicationWatchdogTable($this->serviceLocator);
        
        return $this->watchdogTable->add($postData);
    }

    public function notice($message = '', $data = null, $dataBefore = null)
    {
        return $this->log('notice', $message, $data, $dataBefore, 6);
    }

    public function error($message = '', $data = null, $dataBefore = null)
    {
        return $this->log('error', $message, $data, $dataBefore, 1);
    }
}