<?php
namespace Application\Entity;

use Application\Service\PermissionManager;

class User
{

    private $serviceLocator;

    public $userTable;

    private $user;

    public $permission;

    private $prefix = 'user_';

    public $id, $email, $password, $salt, $user_group_id;
    
    // this method copies the data from the provided array to our entity's properties
    public function exchangeArray(array $data)
    {
        $this->id = $data['id'];
        $this->user_group_id = $data['user_group_id'];
        $this->email = $data['email'];
        $this->password = $data['password'];
        $this->salt = $data['salt'];
        $this->user = (array) $this;
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator($sm)
    {
        $this->serviceLocator = $sm;
    }

    public function setUserTable($userTable)
    {
        $this->userTable = $userTable;
    }

    public function setUser($user = null, $setPermission = true)
    {
        if (!$user)
            $user = $this->user;
        if ($setPermission && $user)
            $this->permission = new PermissionManager($this->serviceLocator, $user);
        return $this;
    }

    public function getUser($key = false)
    {
        if ($key && isset($this->user[$key]))
            return $this->user[$key];
        
        return $this->user;
    }

    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function hasPermission($permission = false)
    {
  
        if (is_array($this->permission->getPermissionList())) { 
            $has = in_array($permission, $this->permission->getPermissionList());
            return $has;
        }
        
        return $this->permission->getPermissions($this->user, $permission);
    }
    
    public function fetchUserpermissions()
    {
        return $this->permission->getPermissions($this->user);
    }
    
    

//     public function Theme($user = false, $minimal = false)
//     {
//         if (! $user)
//             $user = $this->user;
        
//         return $user['email'];
//     }

    public function getUserDetails($userId = false, $key = false)
    {
        if (! $userId)
            $userId = $this->getUser("id");
        
        if (! is_numeric($userId))
            return false;
        
        $user = $this->userTable->getUserById($userId);
    }

    public function findUser($userId)
    {
        return $this->userTable->getUserById($userId);
    }

    public function isOwnerOf($item, $key = 'user_id')
    {
        if ($this->user == null)
            return false;
        
        if (isset($item[$key]) && $item[$key] == $this->user['id']) {
            return true;
        }
        return false;
    }
}