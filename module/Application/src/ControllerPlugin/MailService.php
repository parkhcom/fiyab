<?php
namespace Application\ControllerPlugin;

use Zend\Mail;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class MailService extends AbstractPlugin
{

    //TODO
    public function sendMail($sender, $recipient, $subject, $text)
    {
        $result = false;
        try {
            $mail = new Message();
            $mail->setBody($text);
            $mail->setFrom($sender);
            $mail->addTo($recipient);
            $mail->setSubject($subject);
            
            $transport = new Sendmail($sender);
            $transport->send($mail);
            $result = true;
        } catch (\Exception $e) {
            $result = false;
        }
        return $result;
    }
}