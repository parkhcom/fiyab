<?php
namespace Application\Controller;
 
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Application\Service\BaseAdminController;

class AdminController extends BaseAdminController
{

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e, 'application-admin-');
        if (! $this->userData)
            return $this->redirect()->toRoute('base');
        
        parent::onDispatch($e);
    }

    public function dashboardAction()
    {
        return new ViewModel();
    }
    public function logoutAction()
    {
        if ($this->authenticatinService->isLoggedIn())
            $this->authenticatinService->clearIdentity();
            return $this->redirect()->toRoute('base');
    }
}
