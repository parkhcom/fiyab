<?php
namespace Application\Controller;
 
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Application\Model\ApplicationUsersPermissionsTable;
use Application\Form\ApplicationUsersPermissionsForm;
use Application\Model\ApplicationUsersGroupsPermissionsTable;
use Application\Model\ApplicationUsersGroupsTable;
use Zend\Db\Sql\Where;
use Application\Service\BaseAdminController; 
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Application\Model\ApplicationUsersTable;

class PermissionController extends BaseAdminController
{

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e, 'application-permission-');
        if (! $this->userData)
            return $this->redirect()->toRoute('base');
        
        parent::onDispatch($e);
    }
    public function indexAction() {
        
    }
    public function listsAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf; 
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid'); 
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $usersTable = new ApplicationUsersTable($this->getServiceLocator()) ;
        $allusers = $usersTable->getList();
        $grid->setTitle(t('User List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($allusers,$dbAdapter);
        
        $col = new Column\Select('id',"application_users");
        $col->setLabel(t('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('username');
        $col->setLabel(t('Username'));
        $col->setSortDefault(1, 'DESC');
      /*   $col->setRendererParameter('formatter', '
            function(cellvalue, options, rowObject){
                var valuePrint = \'<span style="font-size: 110%">\' + cellvalue + \'</span>\';
            
                $.each(rowObject.selectedTags, function(index, value){
                    valuePrint += \' <span class="label label-info">\' + value + \'</span>\';
                });
            
                valuePrint += "<br />";
                valuePrint += \'<i>' . $grid->getTranslator()->translate('Created by') . ' \' + rowObject.entryCreator_displayName + \'</i>\';
            
                return valuePrint;
            }
        ', 'jqgrid'); */
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
       
        $col = new Column\Select('email');
        $col->setLabel(t('Email'));
        $grid->addColumn($col);
        
        $col = new Column\Select('mobile');
        $col->setLabel(t('Mobile'));
        $grid->addColumn($col);
        
        $col = new Column\Select('status');
        $col->setLabel(t('Status'));
        $col->setWidth(10);
        $options = array('0' => 'inactive', '1' => 'active');
        $col->setFilterSelectOptions($options);
        $replaces = array('0' => 'inactive', '1' => 'active');
        $col->setReplaceValues($replaces, $col);
        
        $grid->addColumn($col);
        
        $col = new Column\Select('user_group_id');
        $col->setLabel(t('User Group'));
        $options = array('2' => 'Manager', '3' => 'User', '4' => 'User',  '5' => 'Spesial user');
        $col->setFilterSelectOptions($options);
        $replaces = array( '2' => 'Manager', '3' => 'User', '4' => 'User', '5' => 'Spesial user');
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
    
        
        
        $btn = new Column\Action\Button();
        $btn->setLabel(t('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
 
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-remove');
 
        
        /* $viewAction2 = new Column\Action\Icon();
         $viewAction2->setIconClass('glyphicon glyphicon-eye-open');
         $viewAction2->setLink("/". $this->lang .'/user/show-profile/'.$rowId);
         $viewAction2->setTooltipTitle(t('Show user profile info')); */
        
        $viewAction3 = new Column\Action\Icon();
        $viewAction3->setIconClass('glyphicon glyphicon-list'); 
        
        
        $actions2 = new Column\Action();
        $actions2->setLabel(t('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        //$actions2->addAction($viewAction2);
        $actions2->addAction($viewAction3); 
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
      //  $link[] = '<a href='."/". $this->lang .'/admin-user/add-user class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.t("Add").'</a>';
        $link[] = '<a tablename="users"  href="/application-ajax-delete-all"  class="btn btn-danger delete_all"><i class="fa fa-times" aria-hidden="true"></i>' . t("Delete Group Permission") . '</a>';
        $link[] = '<a href="/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hid den="true"></i>'.t("Help").'</a>';
       
        $grid->getRenderer()->setTemplate('application/permission/list');
        $grid->render(); 
        return $grid->getResponse();
       /*  $viewModel->setTemplate('application/permission/list');
        $viewModel->setVariable('zfc', $test); 
        return $viewModel; */
     
       
    }
    public function listAction()
    { 
       $this->flashMessenger()->addSuccessMessage('Record Updated Successfully.');
        return new ViewModel([
            'permissions' => (new ApplicationUsersPermissionsTable($this->getServiceLocator()))->fetch(),
            'token' => $this->tools->token()
        ]);   
    }
    public function addAction()
    { 
        $applicationUsersPermissionsForm = new ApplicationUsersPermissionsForm();
        $view['applicationUsersPermissionsForm'] = $applicationUsersPermissionsForm;
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $applicationUsersPermissionsForm->setData($data);
            if ($applicationUsersPermissionsForm->isValid()) {
                $applicationUsersPermissionsTable = new ApplicationUsersPermissionsTable($this->getServiceLocator());
                $validData = $applicationUsersPermissionsForm->getData();
                if ($applicationUsersPermissionsTable->checkExist(trim($validData['permission_name']))) {
                    $view['error'] = t('Record Already Exists');
                } else {
                    if ($applicationUsersPermissionsTable->add($validData) !== false) {
                        $this->flashMessenger()->addSuccessMessage('Record Added Successfully.');
                        return $this->redirect()->toRoute('application-admin', [
                            'controller' => 'permission',
                            'action' => 'list',
                            'lang' => $this->tools->getLang()
                        ]);
                    } else {
                        $view['error'] = t('Operation Failed');
                    }
                }
            }
        }
        return new ViewModel($view);
    }
    public function updateAclAction()
    {

        $view = array();
        $applicationPermissionsTable = new ApplicationUsersPermissionsTable($this->getServiceLocator());
        $allPermissions = $this->myHelper()->getDirContents(__DIR__ . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "..");
        $permArray = [];
      
        if ($allPermissions) {
            foreach ($allPermissions as $perm) {
                $permArray[] = $perm;
                $where = new Where();
                $where->equalTo('permission_name', $perm);
                $isExist = $applicationPermissionsTable->fetch($where);
                $label = explode('-', $perm);
                $perm = preg_replace('/\B([A-Z])/', '-$1', $perm);
                $inserDataArray = array(
                    'permission_name' => $perm,
                    'module_name' => $label[0],
                    'permission_label' => $label[2],
                    'exception' => 0
                );
    
                if (! $isExist) {
                    $applicationPermissionsTable->add($inserDataArray);
                } else {
                    $res = $applicationPermissionsTable->update((int) $isExist[0]['id'], $inserDataArray);
                    
                }
            }
        }
       if ($permArray) {
            $where = new Where();
            $where->notIn('permission_name', $permArray);
            $applicationPermissionsTable->deleteWhere($where);
        }   
        $this->flashMessenger()->addSuccessMessage(t('Record Updated Successfully.'));
        return $this->redirect()->toRoute('application-permission', [
            'controller' => 'permission',
            'action' => 'list',
            'lang' => $this->tools->getLang()
        ]);  
        return new ViewModel($view);
    }

    public function updateAction()
    {
        $applicationUsersPermissionsTable = new ApplicationUsersPermissionsTable($this->getServiceLocator());
        $permissionById = $applicationUsersPermissionsTable->fetch([
            'id' => $this->params('id', - 1)
        ]);
        $applicationUsersPermissionsForm = new ApplicationUsersPermissionsForm();
        $applicationUsersPermissionsForm->populateValues($permissionById[0]);
        $view['applicationUsersPermissionsForm'] = $applicationUsersPermissionsForm;
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $applicationUsersPermissionsForm->setData($data);
            if ($applicationUsersPermissionsForm->isValid()) {
                $validData = $applicationUsersPermissionsForm->getData();
                if ($applicationUsersPermissionsTable->checkExist(trim($validData['permission_name']), $this->params('id', - 1))) {
                    $view['error'] = t('Record Already Exists');
                } else {
                    if ($applicationUsersPermissionsTable->update($this->params('id', - 1), $validData) !== false) {
                        $this->flashMessenger()->addSuccessMessage('Record Updated Successfully.');
                        return $this->redirect()->toRoute('application-admin', [
                            'controller' => 'permission',
                            'action' => 'list',
                            'lang' => $this->tools->getLang()
                        ]);
                    } else {
                        $view['error'] = t('Operation Failed');
                    }
                }
            }
        }
        return new ViewModel($view);
    }

    public function deleteAction()
    {
        if ($this->tools->token('check', $this->params('token'))) {
            $applicationUsersPermissionsTable = new ApplicationUsersPermissionsTable($this->getServiceLocator());
            $isDeleted = $applicationUsersPermissionsTable->delete($this->params('id', - 1));
            if ($isDeleted !== false) {
                $this->flashMessenger()->addSuccessMessage('Record Deleted Successfully.');
            } else {
                $this->flashMessenger()->addErrorMessage('Opration Failed.');
            }
        } else {
            $this->flashMessenger()->addErrorMessage('Opration Failed.');
        }
        return $this->redirect()->toRoute('application-admin', [
            'controller' => 'permission',
            'action' => 'list',
            'lang' => $this->tools->getLang()
        ]);
    }

    public function assignPermissionToUserGroupAction()
    {
        $applicationUsersGroupsPermissionsTable = new ApplicationUsersGroupsPermissionsTable($this->getServiceLocator());
        $applicationUsersPermissionsTable = new ApplicationUsersPermissionsTable($this->getServiceLocator());
        $permissionListaData = $applicationUsersPermissionsTable->fetch();
        $permisiionList = array();
        if($permissionListaData) {
            foreach ($permissionListaData as $permData) {
                $permisiionList[$permData['module_name']][] = $permData;
            }
        }
 
        $view['list'] = $permisiionList;
        $applicationUsersGroupsTable = new ApplicationUsersGroupsTable($this->getServiceLocator());
        $view['groups'] = $applicationUsersGroupsTable->fetch();
        if ($this->getRequest()->isPost()) {
            $postData = (array) $this->getRequest()->getPost();
            $perms = $postData['perms'];
            $lastPerms = $postData['lastperms'];
            foreach ($lastPerms as $permId => $perm) {
                $applicationUsersGroupsPermissionsTable->delete($permId);
            }
            foreach ($perms as $permId => $groups) {
                foreach ($groups as $groupId) {
                    $applicationUsersGroupsPermissionsTable->add($permId, $groupId);
                }
            }
            $this->flashMessenger()->addSuccessMessage('Record Deleted Successfully.');
            return $this->redirect()->toRoute('application-permission', [
                'controller' => 'permission',
                'action' => 'list',
                'lang' => $this->tools->getLang()
            ]);
        }
        $groupPermissions = $applicationUsersGroupsPermissionsTable->getPermissions();
        $permissionsPerGroup = array();
        foreach ($groupPermissions as $key => $gp) {
            $permissionsPerGroup[$gp['user_group_id']][] = $gp['permission_id'];
        }
        $view['current'] = $permissionsPerGroup;
        return new ViewModel($view);
    }
}