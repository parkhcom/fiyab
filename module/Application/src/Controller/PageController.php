<?php
namespace Application\Controller;

use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Application\Model\ApplicationPagesTable;
use Application\Form\ApplicationPagesForm;
use Application\Service\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Action;
use ZfcDatagrid\Action\Mass;

class PageController extends BaseAdminController
{

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e, 'application-page-');
        if (! $this->userData)
            return $this->redirect()->toRoute('baseZ');
        
        parent::onDispatch($e);
    }

    public function listAction()
    {
        $applicationPageTable = new ApplicationPagesTable($this->getServiceLocator());
        $groupsConfig = $this->getServiceLocator()->get('config')['groups_config']['shopkeeper'];
        $where = array();
        
        $token = $this->tools->token();
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $grid->setTitle(t('Customer Price report'));
        
        $config = $this->getServiceLocator()->get('Config');
        $rowCount = $config['zfcItemsPerPage'];
        $dbAdapter = new Adapter($config['db']);
        $pages = $applicationPageTable->fetch();
        
        $grid->setTitle(t('Shop List'));
        $grid->setDefaultItemsPerPage($rowCount);
        $grid->setDataSource($pages, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(t('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $col = new Column\Select('title');
        $col->setLabel(t('Title'));
        $col->setWidth(15);
        $grid->addColumn($col);
        
        $col = new Column\Select('slug');
        $col->setLabel(t('Slug'));
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $status = new Column\Select('status');
        $status->setLabel(t('Status'));
        $options = array(
            '0' => t('No'),
            '1' => t('Yes')
        );
        $status->setFilterSelectOptions($options);
        $replaces = array(
            '0' => t('No'),
            '1' => t('Yes')
        );
        $status->setReplaceValues($replaces, $col);
        $grid->addColumn($status);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(t('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink('/page/edit/id/' . $rowId);
        $viewAction->setTooltipTitle(t('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink('/page/delete/id/' . $rowId . '/token/' . $token);
        $viewAction2->setTooltipTitle(t('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(t('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        /* $mass = new Mass(t('Add new'), '/page/add', false);
        $grid->addMassAction($mass); */
        
        $grid->getRenderer()->setTemplate('application/page/list');
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addAction()
    {
        $applicationPagesForm = new ApplicationPagesForm();
        $view['applicationPagesForm'] = $applicationPagesForm;
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $applicationPagesForm->setData($data);
            if ($applicationPagesForm->isValid()) {
                
                $validData = $applicationPagesForm->getData();
                $validData['user_id'] = $this->userData->id;
                $validData['user_ip'] = $this->userIp;
                
                if ((new ApplicationPagesTable($this->getServiceLocator()))->add($validData) !== false) {
                    $this->flashMessenger()->addSuccessMessage('Record Added Successfully.');
                    return $this->redirect()->toRoute('application-admin', [
                        'controller' => 'page',
                        'action' => 'list',
                        'lang' => $this->tools->getLang()
                    ]);
                } else {
                    $view['error'] = t('Operation Failed');
                }
            }
        }
        return new ViewModel($view);
    }

    public function editAction()
    {
        $applicationPagesTable = new ApplicationPagesTable($this->getServiceLocator());
        $pageById = $applicationPagesTable->fetch([
            'id' => $this->params('id', - 1)
        ]);
        if (! $pageById) {
            $this->flashMessenger()->addErrorMessage(t('No Such Field Exist'));
            return $this->redirect()->toRoute('application-page', [
                'controller' => 'page',
                'action' => 'list',
                'lang' => $this->tools->getLang()
            ]);
        }
        $applicationPagesForm = new ApplicationPagesForm();
        $applicationPagesForm->populateValues($pageById[0]);
        
        $view['applicationPagesForm'] = $applicationPagesForm;
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            
            $applicationPagesForm->setData($data);
            if ($applicationPagesForm->isValid()) {
                
                $validData = $applicationPagesForm->getData();
                $validData['user_id'] = $this->userData->id;
                $validData['user_ip'] = $this->userIp;
                
                if ($applicationPagesTable->update($this->params('id', - 1), $validData) !== false) {
                    $this->flashMessenger()->addSuccessMessage('Record Added Successfully.');
                    return $this->redirect()->toRoute('application-page', [
                        'controller' => 'page',
                        'action' => 'list',
                        'lang' => $this->tools->getLang()
                    ]);
                } else {
                    $res = $applicationPagesForm->getMessages();
                    
                    $view['error'] = t('Operation Failed');
                }
            }
        }
        return new ViewModel($view);
    }

    public function deleteAction()
    {
        if ($this->tools->token('check', $this->params('token'))) {
            $applicationPagesTable = new ApplicationPagesTable($this->getServiceLocator());
            
            $isDeleted = $applicationPagesTable->delete($this->params('id', - 1));
            if ($isDeleted !== false) {
                $this->flashMessenger()->addSuccessMessage('Record Deleted Successfully.');
            } else {
                $this->flashMessenger()->addErrorMessage('Opration Failed.');
            }
        } else {
            $this->flashMessenger()->addErrorMessage('Opration Failed.');
        }
        return $this->redirect()->toRoute('application-admin', [
            'controller' => 'page',
            'action' => 'list',
            'lang' => $this->tools->getLang()
        ]);
    }
}