<?php
namespace Application\Controller;

use Application\Service\BaseController;
use Zend\Mvc\MvcEvent;
use Application\Service\Authentication;
use Zend\View\Model\ViewModel;
use Application\Form\ApplicationContentsForm;
use Application\Model\ApplicationContentsTable;
use Application\Model\ApplicationTagsTable;
use Application\Form\ApplicationTagsForm;

class ContentController extends BaseController
{

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e, 'application-content-');
        if (! $this->userData)
            return $this->redirect()->toRoute('base');
        
        parent::onDispatch($e);
    }
    
    // manage content
    public function indexAction()
    {
    
    }
    public function articlesAction()
    {
        return new ViewModel([
            'articles' => (new ApplicationContentsTable($this->getServiceLocator()))->fetch([
                'type' => 1,
                'lang' => $this->tools->getLang()
            ]),
            'token' => $this->tools->token()
        ]);
    }

    public function addContentAction()
    {
        $applicationContentsForm = new ApplicationContentsForm();
        $view['applicationContentsForm'] = $applicationContentsForm;
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $applicationContentsForm->setData($data);
            if ($applicationContentsForm->isValid()) {
                $applicationContentsTable = new ApplicationContentsTable($this->getServiceLocator());
                $validData = $applicationContentsForm->getData();
                $validData['user_id'] = $this->userData->id;
                $validData['type'] = $this->getServiceLocator()->get('config')['content_types']['article'];
                $validData['slug'] = ($validData['slug'] ? $validData['slug'] : str_replace(' ', '-', strtolower($validData['title'])));
                $validData['meta_description'] = ($validData['meta_description'] ? $validData['meta_description'] : substr($validData['content'], 0, 160));
                $validData['lang'] = $this->tools->getLang();
                $validData['user_ip'] = $this->userIp;
                
                if ($applicationContentsTable->add($validData) !== false) {
                    $this->flashMessenger()->addSuccessMessage('Record Added Successfully.');
                    return $this->redirect()->toRoute('application-admin', [
                        'controller' => 'content',
                        'action' => 'articles',
                        'lang' => $this->tools->getLang()
                    ]);
                } else {
                    $view['error'] = t('Operation Failed');
                }
            }
        }
        return new ViewModel($view);
    }

    public function updateContentAction()
    {
        $applicationContentsTable = new ApplicationContentsTable($this->getServiceLocator());
        $contentById = $applicationContentsTable->fetch([
            'id' => $this->params('id', - 1)
        ]);
        $applicationContentsForm = new ApplicationContentsForm();
        $applicationContentsForm->populateValues($contentById[0]);
        $view['applicationContentsForm'] = $applicationContentsForm;
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $applicationContentsForm->setData($data);
            if ($applicationContentsForm->isValid()) {
                $validData = $applicationContentsForm->getData();
                $validData['meta_description'] = substr($validData['content'], 0, 160);
                $validData['lang'] = $this->tools->getLang();
                $validData['slug'] = str_replace(' ', '-', strtolower($validData['title']));
                $validData['user_ip'] = $this->userIp;
                
                if ($applicationContentsTable->update($this->params('id', - 1), $validData) !== false) {
                    $this->flashMessenger()->addSuccessMessage('Record Updated Successfully.');
                    return $this->redirect()->toRoute('application-admin', [
                        'controller' => 'content',
                        'action' => 'articles',
                        'lang' => $this->tools->getLang()
                    ]);
                } else {
                    $view['error'] = t('Operation Failed');
                }
            }
        }
        return new ViewModel($view);
    }

    public function deleteContentAction()
    {
        if ($this->tools->token('check', $this->params('token'))) {
            $applicationContentsTable = new ApplicationContentsTable($this->getServiceLocator());
            
            $isDeleted = $applicationContentsTable->delete($this->params('id', - 1));
            if ($isDeleted !== false) {
                $this->flashMessenger()->addSuccessMessage('Record Deleted Successfully.');
            } else {
                $this->flashMessenger()->addErrorMessage('Opration Failed.');
            }
        } else {
            $this->flashMessenger()->addErrorMessage('Opration Failed.');
        }
        return $this->redirect()->toRoute('application-admin', [
            'controller' => 'content',
            'action' => 'articles',
            'lang' => $this->tools->getLang()
        ]);
    }
    
    // manage tags
    public function tagsAction()
    {
        return new ViewModel([
            'tags' => (new ApplicationTagsTable($this->getServiceLocator()))->fetch([
                'pre_defined' => 1
            ]),
            'token' => $this->tools->token()
        ]);
    }

    public function addTagAction()
    {
        $applicationTagsTable = new ApplicationTagsTable($this->getServiceLocator());
        $allPrentTags = $applicationTagsTable->fetch([
            'parent_id' => 0
        ]);
        $applicationTagsForm = new ApplicationTagsForm($allPrentTags);
        $view['applicationTagsForm'] = $applicationTagsForm;
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $applicationTagsForm->setData($data);
            if ($applicationTagsForm->isValid()) {
                $validData = $applicationTagsForm->getData();
                $validData['user_id'] = $this->userData->id;
                $validData['user_ip'] = $this->userIp;
                if ($applicationTagsTable->checkExist($validData['title'])) {
                    $view['error'] = t('Duplicate Title');
                } else {
                    if ($applicationTagsTable->add($validData) !== false) {
                        $this->flashMessenger()->addSuccessMessage('Record Added Successfully.');
                        return $this->redirect()->toRoute('application-admin', [
                            'controller' => 'content',
                            'action' => 'tags',
                            'lang' => $this->tools->getLang()
                        ]);
                    } else {
                        $view['error'] = t('Operation Failed');
                    }
                }
            }
        }
        return new ViewModel($view);
    }

    public function updateTagAction()
    {
        $applicationTagsTable = new ApplicationTagsTable($this->getServiceLocator());
        $allPrentTags = $applicationTagsTable->fetch([
            'parent_id' => 0,
            'lang' => $this->tools->getLang(),
            'id != ?' => $this->params('id', - 1)
        ]);
        $tagById = $applicationTagsTable->fetch([
            'id' => $this->params('id', - 1)
        ]);
        $applicationTagsForm = new ApplicationTagsForm($allPrentTags);
        $applicationTagsForm->populateValues($tagById[0]);
        $view['applicationTagsForm'] = $applicationTagsForm;
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $applicationTagsForm->setData($data);
            if ($applicationTagsForm->isValid()) {
                $validData = $applicationTagsForm->getData();
                $validData['user_id'] = $this->userData->id;
                $validData['user_ip'] = $this->userIp;
                if ($applicationTagsTable->checkExist($validData['title'], $this->params('id', - 1))) {
                    $view['error'] = t('Duplicate Title');
                } else {
                    if ($applicationTagsTable->update($this->params('id', - 1), $validData) !== false) {
                        $this->flashMessenger()->addSuccessMessage('Record Added Successfully.');
                        return $this->redirect()->toRoute('application-admin', [
                            'controller' => 'content',
                            'action' => 'tags',
                            'lang' => $this->tools->getLang()
                        ]);
                    } else {
                        $view['error'] = t('Operation Failed');
                    }
                }
            }
        }
        return new ViewModel($view);
    }

    public function deleteTagAction()
    {
        if ($this->tools->token('check', $this->params('token'))) {
            $applicationTagsTable = new ApplicationTagsTable($this->getServiceLocator());
            
            $isDeleted = $applicationTagsTable->delete($this->params('id', - 1));
            if ($isDeleted !== false) {
                $this->flashMessenger()->addSuccessMessage('Record Deleted Successfully.');
            } else {
                $this->flashMessenger()->addErrorMessage('Opration Failed.');
            }
        } else {
            $this->flashMessenger()->addErrorMessage('Opration Failed.');
        }
        return $this->redirect()->toRoute('application-admin', [
            'controller' => 'content',
            'action' => 'tags',
            'lang' => $this->tools->getLang()
        ]);
    }
}