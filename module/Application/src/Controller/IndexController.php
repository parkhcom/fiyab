<?php
namespace Application\Controller;

use Zend\View\Model\ViewModel;
 
use Application\Form\ApplicationSigninForm;
use Application\Service\BaseController;
use Zend\Mvc\MvcEvent;
use Application\Helper\Tools;
 
use Zend\Session\SessionManager;
use Application\Form\ApplicationSignupForm;
use Application\Model\ApplicationUsersBlockIpTable;
use Users\Model\ApplicationUsersTable;
use Application\Model\ApplicationPagesTable;
use Application\Entity\User;

class IndexController extends BaseController
{

    public $applicationUsersTable, $user;

    private $sessionManager;

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e);
        parent::onDispatch($e);
    }

    public function indexAction()
    {
        return new ViewModel();
    }

    public function signinAction()
    {
        $this->layout('layout/login-layout.phtml');
        $viewModel = new ViewModel(); 
        $signinForm = new ApplicationSigninForm();
        $viewModel->setVariable('signinForm', $signinForm);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()
                ->getPost()
                ->toArray();
            if ($signinForm->setData($data)->isValid()) {
                $validData = $signinForm->getData();
                $sessionManager = new SessionManager();
                $sessionManager->rememberMe(60 * 60 * 24 * 30);
                if ($this->authenticatinService->auth($validData['username'], $validData['password'])) {
                    $this->userData = $this->authenticatinService->getIdentity();
                    if ($this->userData->status == 1) {
                        
                        $applicationUsersTable = new ApplicationUsersTable($this->getServiceLocator());
                        $applicationUsersTable->updateLastLoginTime($this->userData->id);
                        
                        return $this->redirect()->toRoute('application-admin');
                    } else {
                        $this->authenticatinService->logOut();
                        
                        $viewModel->setVariable('error', t('please check your email admin not confirm you yet'));
                    }
                } else {
                    
                    $viewModel->setVariable('error', t('Incorrect email or password'));
                }
            }
        }
        
        return $viewModel;
    }

    public function signupAction()
    {
        $this->layout('layout/login-layout.phtml');
        // LOGOUT FIRST
        if ($this->authenticatinService->isLoggedIn())
            $this->authenticatinService->clearIdentity();
        // check if the user if logged in
        
        if ($this->authenticatinService->isLoggedIn()) {
            return $this->redirect()->toRoute('application-admin');
        }
        $viewModel = new ViewModel();
        $signupForm = new ApplicationSignupForm();
        
        $viewModel->setVariable('signupForm', $signupForm);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            $signupForm->setData($data);
            if ($signupForm->isValid()) {
                $validData = $signupForm->getData();
                // Check If EMAIL IS EXIST
                $applicationUsersTable = new ApplicationUsersTable($this->getServiceLocator());
                $where = [
                    'email' => $validData['email']
                ];
                $applicationUsersTableData = $applicationUsersTable->fetch($where);
                if ($applicationUsersTableData) {
                    $this->flashMessenger()->addSuccessMessage(t("this email is exist try another one please"));
                } else {
                    $groupsConfig = $this->getServiceLocator()->get('config')['groups_config']['shopkeeper'];
                    $password = $validData['password'];
                    $salt = time();
                    $validData['password'] = md5(sha1($validData['password']) . $salt);
                    $validData['user_group_id'] = $groupsConfig;
                    $validData['salt'] = $salt;
                    $validData['username'] = $validData['email'];
                    $validData['fullname'] = $validData['fullname'];
                    $validData['is_customer'] = 0;
                    $validData['status'] = 0;
                    // TODO
                    $applicationUsersTable = new ApplicationUsersTable($this->getServiceLocator());
                    $applicationUsersBlockIpTable = new ApplicationUsersBlockIpTable($this->getServiceLocator());
                    
                    $applicationUsersTable->saveUser($validData);
                    $viewModel->setVariable('error', t('thank you for registration please check your email for admin confirmation'));
                    
                    // return $this->redirect()->toRoute('base');
                }
            } else {
                $res = $signupForm->getMessages();
                if ($res) {
                    foreach ($res as $key => $result) {
                        if ($key == 'captcha')
                            $viewModel->setVariable('error', t('Captcha value is wrong'));
                    }
                }
                // var_dump($res);die;
            }
        }
        return $viewModel;
    }

    public function contentAction()
    {
        return new ViewModel([
            'id' => $this->params('id', - 1)
        ]);
    }

    public function searchAction()
    {
        return new ViewModel();
    }

    public function passwordForgottenAction()
    {
        return new ViewModel();
    }

    public function resetPasswordAction()
    {
        return new ViewModel();
    }

    // Static pages
    public function aboutAction()
    {
        return new ViewModel([
            'pageInfo' => (new ApplicationPagesTable($this->getServiceLocator()))->fetch([
                'slug' => $this->params('action'),
                'lang' => $this->tools->getLang()
            ])
        ]);
    }

    public function contactUsAction()
    {
        return new ViewModel([
            'pageInfo' => (new ApplicationPagesTable($this->getServiceLocator()))->fetch([
                'slug' => $this->params('action'),
                'lang' => $this->tools->getLang()
            ])
        ]);
    }

    public function guideAction()
    {
        return new ViewModel([
            'pageInfo' => (new ApplicationPagesTable($this->getServiceLocator()))->fetch([
                'slug' => $this->params('action'),
                'lang' => $this->tools->getLang()
            ])
        ]);
    }

    public function termsAction()
    {
        return new ViewModel([
            'pageInfo' => (new ApplicationPagesTable($this->getServiceLocator()))->fetch([
                'slug' => $this->params('action'),
                'lang' => $this->tools->getLang()
            ])
        ]);
    }

    public function pageAction()
    {
        return new ViewModel([
            'pageInfo' => (new ApplicationPagesTable($this->getServiceLocator()))->fetch([
                'slug' => $this->params('action'),
                'lang' => $this->tools->getLang()
            ])
        ]);
    }

    public function logoutAction()
    {
        if ($this->authenticatinService->isLoggedIn())
            $this->authenticatinService->clearIdentity();
        return $this->redirect()->toRoute('base');
    }
}