<?php
namespace Application\Service;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Mvc\MvcEvent;
use Application\Model\ApplicationUsersTable;
use Application\Helper\Tools;
use Application\Entity\Watchdog;

class BaseController extends AbstractActionController
{

    protected $authenticatinService, $serviceManger, $dbAdapter;

    public $userIp;

    protected $userData;

    /**
     *
     * @var \Application\Entity\Watchdog
     */
    public $whatchdog;

    /**
     *
     * @var \Application\Helper\Tools
     */
    public $tools;

    /**
     *
     * @var \Application\Entity\User
     */
    protected $user;

    public function onDispatch(MvcEvent $e)
    {
        parent::onDispatch($e);
    }

    public function isAllowed($name)
    {
        return true;
        if ($this->user) {
            if ($this->user->hasPermission($name))
                return true;
        }
        return false;
    }

    public function initController(MvcEvent $e, $prefix = null)
    {
        $this->tools = new Tools();
        $this->layout()->tools = $this->tools;
        
        $GLOBALS["lang"] = $this->layout()->tools->getLang();
        
        $this->userIp = getenv('HTTP_CLIENT_IP') ?: getenv('HTTP_X_FORWARDED_FOR') ?: getenv('HTTP_X_FORWARDED') ?: getenv('HTTP_FORWARDED_FOR') ?: getenv('HTTP_FORWARDED') ?: getenv('REMOTE_ADDR');
        
        $app = $e->getApplication();
        $this->serviceManger = $app->getServiceManager();
        $this->authenticatinService = new Authentication($this->serviceManger);
        $this->dbAdapter = $this->serviceManger->get(AdapterInterface::class);
        
        $this->userData = $this->authenticatinService->getIdentity();
        
        $this->whatchdog = new Watchdog($this->getServiceLocator());
        
        if ($this->userData) {
            $this->layout()->userData = $this->userData;
            $applicationUserTable = new ApplicationUsersTable($this->getServiceLocator());
           /*  if ($this->authenticatinService->isLoggedIn())
                $this->authenticatinService->clearIdentity();
                return $this->redirect()->toRoute('base');
            var_dump($this->userData);die; */
            $this->user = $applicationUserTable->fetchUser($this->userData->id);
            $this->user->setUserTable($applicationUserTable);
            $this->user->setUser();
            $this->whatchdog->setUser($this->user->getUser());
            if ($prefix) {
                $permissionName = $prefix . $this->params('action');
                $isAllowed = $this->isAllowed($permissionName);
                if (! $isAllowed) {
                    return $this->redirect()->toRoute('base');
                }
            }
            $this->layout()->userPermissions = $this->user->fetchUserpermissions();
        }
        
        $this->initTranslator();
    }

    public function initTranslator()
    {
        global $translatorHelper;
        $translator = $this->serviceManger->get('translator');
        $translatorHelper = $translator;
    }

    protected function getDbAdapter()
    {
        return $this->dbAdapter;
    }

    public function getServiceLocator()
    {
        return $this->serviceManger;
    }
}