<?php 

namespace Application\Service;
use Zend\Db\Adapter\Adapter; 
use Zend\Db\TableGateway\AbstractTableGateway;

class BaseModel  extends AbstractTableGateway 
{
	/**
	 * 
	 * @var \Zend\Db\TableGateway\TableGateway
	 */
    protected $dbAdapter;
   public function getDbAdapter()
    {  
        $this->dbAdapter =  $this->createAdapter(); 
        
        return $this->dbAdapter;
    }
    public function createAdapter()
    {
        $config = include './config/autoload/global.php';
        //get child class name
        $databaseConfig = $config['db'] ;
        $modelClass = $this->modelClass;
        if (property_exists($modelClass, 'database')) {
            if (isset($config['db_config'][$modelClass::$database])) {
                $databaseConfig = $config['db_config'][$modelClass::$database];
            }
        }
        return new Adapter($databaseConfig);
        
    }
}