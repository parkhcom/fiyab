<?php
namespace Application\Service;
 
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

class BaseAjaxController extends BaseAdminController
{  	 
	public $viewModel;
	
	public function onDispatch(MvcEvent $e)
	{  
		$this->viewModel = new ViewModel();
		$this->viewModel->setTerminal(true);
		parent::onDispatch($e);
	}
	  
}