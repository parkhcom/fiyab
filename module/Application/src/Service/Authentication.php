<?php
namespace Application\Service;

use Zend\Authentication\AuthenticationService;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as AuthAdapter;
use Zend\Authentication\Storage\Session;
use Zend\ServiceManager\ServiceManager;

class Authentication extends AuthenticationService
{

    protected $authAdapter;

    public function __construct(ServiceManager $serviceManager)
    {
        $adapter = $serviceManager->get('Zend\Db\Adapter\Adapter');
        $this->authAdapter = new AuthAdapter($adapter);
        $this->authAdapter->setTableName('application_users');
        $this->authAdapter->setIdentityColumn('username');
        $this->authAdapter->setCredentialColumn('password');
      //  $this->authAdapter->setCredentialTreatment('md5(concat(Sha1(?), salt)) AND status = 1');
        $this->authAdapter->setCredentialTreatment('md5(concat(Sha1(?), salt))');
        $this->setAdapter($this->authAdapter);
        $this->setStorage(new Session('authentication'));
    }
    
    
    public function auth($username, $password)
    {
        $this->authAdapter->setIdentity($username);
        $this->authAdapter->setCredential($password);
        $isLogin = $this->authenticate($this->authAdapter);
     
        if ($isLogin->isValid()) {
            $storage = $this->getStorage('authentication'); 
            $storage->write($this->authAdapter->getResultRowObject(array(
                'id',
                'user_group_id',
                'status', 
                'fullname', 
                'is_customer', 
                'email',
            )));
            $t = $this->getIdentity();
          
            return true;
        } else {
            return false;
        }
    }
    
    public function isLoggedIn()
    {
        return $this->getIdentity();
    }
    
    public function logOut()
    {
        $this->getStorage()->clear();
        return $this->clearIdentity();
    }
}