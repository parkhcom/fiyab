<?php
namespace Application\Service;

use Application\Model\ApplicationUsersGroupsPermissionsTable;
use Application\Model\ApplicationUsersPermissionsTable;
// use Application\Model\ApplicationUsers;
class PermissionManager
{

    public $user;

    public $permissionList;

    public $serviceLocator;

    public function __construct($serviceManager = false, $user = null)
    {
        $this->user = $user;
        $this->serviceLocator = $serviceManager;
        $this->permissionList = $this->getPermissions();
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function getPermissionList()
    {
        return $this->permissionList;
    }

    public function getPermissions($user = false, $permissionName = null)
    {
        if (! $user)
            $user = $this->user;
        
        $applicationUsersPermissionsTable = new ApplicationUsersPermissionsTable($this->getServiceLocator());
        $defaultPermissions = $applicationUsersPermissionsTable->getDefaultPermission();
        
        $anonymousPermissions = array();
        
        foreach ($defaultPermissions as $aPermission) {
            $anonymousPermissions[] = strtolower($aPermission['permission_name']);
        }
        
        if ($permissionName && in_array($permissionName, $anonymousPermissions))
            return true;
        
        if ($user) {
            $applicationUsersGroupsPermissionsTable = new ApplicationUsersGroupsPermissionsTable($this->getServiceLocator());
            $permissions = $applicationUsersGroupsPermissionsTable->getPermissions($user['user_group_id']);
            if ($permissions) {
                $userPermissions = array();
                foreach ($permissions as $index => $permission) {
                    $userPermissions[$index] = $permission['permission_name'];
                }
                if ($permissionName) {
                    return in_array($permissionName, $userPermissions);
                }
                return $userPermissions;
            }
        }
        return $anonymousPermissions;
    }
}
