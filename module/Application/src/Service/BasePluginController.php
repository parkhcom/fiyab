<?php
namespace Application\Service;

use Application\Registry\Registry;

class BasePluginController
{
    public $userData ;
    public function __construct($serviceManager, $lang = "fa")
    {
        $this->serviceLocator = $serviceManager;
      /*   $this->lang = $lang;
        $this->authenticationService = $this->getServiceLocator()->get('AuthenticationService');
        $this->userData = $this->authenticationService->isLoggedIn();
        var_dump($this->userData);die; */
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    
    public function getRegistry($methodName, $params = null)
    {
        $regisrey = new \Zend\View\Helper\Placeholder\Registry($this->getServiceLocator());
        return $regisrey->$methodName($params);
    }
}
