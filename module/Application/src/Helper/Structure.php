<?php
namespace Application\Helper;

class Structure
{

    private static $structure;

    private static $lookup;

    // Nodes can be any objects that have IDs (id) and a parent IDs (parent)
    public static function buildTree($nodes)
    { 
        // Initialize structure and lookup arrays
        self::$structure = array();
        self::$lookup = array();
        // Walk through nodes
        array_walk($nodes, 'self::addNode');
        // Return the hierarchical (tree) structure
        return self::$structure;
    }

    private static function addNode($obj)
    {
        // Convert string ids returned by PDO to integers
        $nid = (int) $obj['id'];
        $pid = (int) $obj['parent_id'];
        // Initialize child array
        $obj['children'] = array();
        // If top level node, set parent child array to a reference to whole structure
        if ($pid === 0) {
            // If top level node, set parent child array to whole structure
            $parent = &self::$structure;
        } else {
            // Else, set it to reference to the parent child array
            $parent = &self::$lookup[$pid]['children'];
        }
        // Get ID of new node it child array
        if($parent) {
        $id = count($parent); 
        } else {
            $id = 0;
        }
        // Add node to child array
        $parent[] = $obj;
        // Add reference to node in parent child array to the lookup table 
        if(!$parent[$id]['children']) {
            unset($parent[$id]['children']);
        }
        self::$lookup[$nid] = &$parent[$id];  
    }
}