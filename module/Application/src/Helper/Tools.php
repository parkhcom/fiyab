<?php
namespace Application\Helper;

use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Filter\ToInt;
use Zend\Filter\DateTimeFormatter;
use Zend\Filter\UriNormalize;
use Zend\Validator\Ip;
use Zend\Session\Container;
use Application\Entity\User;

class Tools

{
    public function getLang()
    {
        $langParts = explode("/", $_SERVER['REQUEST_URI']);
        $currentLang = (isset($langParts[1]) && strlen($langParts[1]) == 2) ? $langParts[1] : 'en';
        return $currentLang;
    }

    public function getLocale()
    {
        $langParts = explode("/", $_SERVER['REQUEST_URI']);
        $currentLang = (isset($langParts[1]) && strlen($langParts[1]) == 2) ? $langParts[1] : 'en_US';
        switch ($currentLang) {
            case 'en':
                $currentLang = 'en_US';
                break;
            case 'fa':
                $currentLang = 'fa_IR';
            default:
                break;
        }
        return $currentLang;
    }

    /**
     *
     * @param string $ip            
     * @return boolean|error message
     */
    public function validateIp($ip)
    {
        if ($ip) {
            $validator = new Ip();
            $stripTags = new StripTags();
            $validatedIp = $stripTags->filter($ip);
            $validatedIp = $validator->isValid($validatedIp);
            if ($validatedIp == true) {
                return true;
            } else {
                $messages = $validator->getMessages();
                return $messages['notIpAddress'];
            }
        }
        return false;
    }

    /**
     *
     * @param string $url            
     * @return stripped string
     */
    public function normalizeUrl($url)
    {
        if ($url) {
            $stripTags = new StripTags();
            $normalizedUrl = $stripTags->filter($url);
            
            $uriNormalize = new UriNormalize();
            $uriNormalize->setDefaultScheme('http');
            $uriNormalize->setEnforcedScheme('http');
            $normalizedUrl = $uriNormalize->filter($normalizedUrl);
            return $normalizedUrl;
        }
        return false;
    }

    /**
     *
     * @param DateTime $date            
     * @param string $format
     *            F j, Y g:i A => March 22, 2014 3:36 PM
     *            Y-m-d H:i:s => 2014-03-22 15:36:00
     *            d/m/Y H:i:s => 22/03/2014 15:36:00
     *            d/m/y => 24/03/12
     *            g:i A => 5:45 PM
     *            G:ia => 05:45pm
     *            g:ia \o\n l jS F Y => 5:45pm on Saturday 24th March 2012
     *            
     *            for more formats, refer to http://www.w3schools.com/php/func_date_date_format.asp
     *            
     * @return formatted date
     */
    public function formatDate($date, $format = null)
    {
        if ($date) {
            $dateTimeFormatter = new DateTimeFormatter();
            if ($format)
                $dateTimeFormatter->setFormat($format);
            $formattedDate = $dateTimeFormatter->filter($date);
            return $formattedDate;
        }
        return false;
    }

    /**
     *
     * @param string $digit            
     * @return int
     */
    public function filterToInt($digit)
    {
        if ($digit) {
            $toInt = new ToInt();
            $int = $toInt->filter($digit);
            return $int;
        }
        return false;
    }

    /**
     *
     * @param string $input            
     * @param array $allowedTags            
     * @return string
     */
    public function filterInput($input, array $allowedTags = null)
    {
        if ($input) {
            if ($allowedTags) {
                $stripTags = new StripTags([
                    'allowTags' => $allowedTags
                ]);
            } else {
                $stripTags = new StripTags();
            }
            
            $stringTrim = new StringTrim();
            $stringTrim->setCharList("\r\n\t ");
            
            $filteredInput = $stripTags->filter($input);
            $filteredInput = $stringTrim->filter($filteredInput);
            return $filteredInput;
        }
        return false;
    }

    public function convertFatoEn($string)
    {
        $string = str_replace('۰', '0', $string);
        $string = str_replace('۱', '1', $string);
        $string = str_replace('۲', '2', $string);
        $string = str_replace('۳', '3', $string);
        $string = str_replace('۴', '4', $string);
        $string = str_replace('۵', '5', $string);
        $string = str_replace('۶', '6', $string);
        $string = str_replace('۷', '7', $string);
        $string = str_replace('۸', '8', $string);
        $string = str_replace('۹', '9', $string);
        return $string;
    }

    /**
     *
     * @param array $assets            
     * @return Link header response
     *         how to use: $this->tools->pushAsset(['/css/styles-main.css' => 'style',]);
     */
    public function pushAsset($assets)
    {
        $result = '';
        foreach ($assets as $asset => $as) { // $as can be image, style and script
            $result .= '<' . $asset . '>; rel=preload; as=' . $as . ', ';
        }
        header('Link: ' . $result, false);
    }

    public function token($switch = 'gen', $token = null, $name = 'token')
    {
        $container = new Container($name);
        if ($switch == 'gen') {
            $hash = md5(time() . rand());
            $container->$name = $hash;
            return $hash;
        }
        if ($switch == 'check' && $token) {
            if ($container->offsetExists($name)) {
                if ($container->offsetGet($name) == $token) {
                    return true;
                }
            }
            return false;
        }
    }
}