<?php
namespace Application\Helper;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorInterface; 
use Zend\View\Helper\ViewModel;

class Plugins extends AbstractPlugin
{

    protected $serviceLocator;
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    { 
        if($this->serviceLocator !== null) return $this;
        $this->serviceLocator = $serviceLocator; return $this;
        
    }

    public function __invoke($module, $controller, $action, $variables = false, $checkExists = true)
    {
      //  $dbConfig = $this->serviceLocator->get('config'); 
        $class = $module . "\\Controller\\" . $controller . "Controller";
       
        $class = new $class($this->serviceLocator);
        $method = $action . "Action";
 
        $viewModel = $class->$method($variables);
        
        if(!$viewModel->getTemplate()) {
            $viewModel->setTemplate(strtolower("/" . $module . "/" . $controller. "/" . preg_replace("/([A-z]+)/", "-$1", $action) . ".phtml"));
        }
        if($variables) {
            $viewModel->setVariables($variables);
        }
        if($viewModel instanceof ViewModel) {
            $html = $this->getView()->render($viewModel);
   
            return $html;
        } else { 
            return "dddddddddd";
        }
    }
    
}