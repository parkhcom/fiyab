<?php
namespace Application\Helper;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use FileUpload\Validator\Simple;
use FileUpload\PathResolver\Simple as PathSimple;
use FileUpload\FileSystem\Simple as FileSimple; 
use FileUpload\FileUpload;
use FileUpload\FileNameGenerator\MD5; 

class ImageUploader extends AbstractPlugin
{

    public function UploadFile($files, $validationExt, $url, $key = 'avatar')
    { 
        if ($files) {
            if (! is_dir($url)) {
                if (false === @mkdir($url, 0777, true)) {
                    throw new \RuntimeException(sprintf('Unable to create the %s directory', $url));
                }
            }
            $validator = new Simple('5M', [
                'image/png',
                'image/jpg',
                'image/png',
                'image/gif',
                'image/jpeg'
            ]);
            $pathresolver = new PathSimple($url);
            $filesystem = new FileSimple();
         
            if (isset($files[$key]['tmp_name']) && ! empty($files[$key]['tmp_name'])) {
                $fileupload = new FileUpload($files[$key], $_SERVER);
                
                $fileupload->setPathResolver($pathresolver);
                $fileupload->setFileSystem($filesystem);
                $fileupload->addValidator($validator);
                
                $md5Generator = new MD5(10);
                $fileupload->setFileNameGenerator($md5Generator);
                list ($files, $headers) = $fileupload->processAll();
                
                foreach ($headers as $header => $value) {
                    header($header . ': ' . $value);
                }
                return $files;
            } else {
                return false;
            }
        } else {
            
            return false;
        }
    }
}
?>