<?php
namespace Application\ViewHelper;

use Zend\View\Helper\AbstractHelper;

class Menu extends AbstractHelper
{

    protected $activeItem = '';

    // Sets ID of the active items.
    public function setActiveItem($permission)
    {
        $this->activeItem = $permission;
    }

    public function getActiveMenu()
    {
        return $this->activeItem;
    }

    public function renderAdminMenuBar()
    {
        return array(
            array(
                'label' => t("Dashboard"),
                'link' => 'javascript:void(0)',
                'permission' => 'application-admin-index',
                'class' => 'icon-dashboard'
            ),
            
            array(
                'label' => t('Permissions'),
                'class' => 'icon-key',
                'link' => 'javascript:void(0)',
                'pages' => array(
                    array(
                        'label' => t('Permission List'),
                        'permission' => 'application-permission-list',
                        'controller' => 'permission',
                        'action' => 'list',
                        'link' => '/permission/list'
                    ),
                    array(
                        'label' => t('Assign Permission'),
                        'permission' => 'application-permission-assign-permission-to-user-group',
                        'controller' => 'permission',
                        'link' => '/permission/assign-permission-to-user-group',
                        'action' => 'assign-permission-to-user-group'
                    ),
                    array(
                        'label' => t('Add Permission'),
                        'permission' => 'application-permission-add',
                        'link' => '/permission/add',
                        'controller' => 'permission',
                        'action' => 'add'
                    ),
                    array(
                        'label' => t('Acl Update'),
                        'permission' => 'application-permission-update-acl',
                        'link' => '/permission/update-acl',
                        'controller' => 'permission',
                        'action' => 'update-acl'
                    )
                )
            ),
            array(
                'label' => t('Users'),
                'link' => 'javascript:void(0)',
                'class' => 'icon-user',
                'permission' => 'users-admin-list',
                'pages' => array(
                    array(
                        'label' => t('Users List'),
                        'permission' => 'users-admin-list',
                        'link' => '/users-admin/list',
                        'controller' => 'admin',
                        'action' => 'list'
                    ) 
                )
            ),
            array(
                'label' => t('Pages'),
                'link' => 'javascript:void(0)',
                'class' => 'icon-windows',
                'permission' => 'application-page-index',
                'pages' => array(
                    array(
                        'label' => t('Pages List'),
                        'permission' => 'application-page-list',
                        'link' => '/page/list',
                        'controller' => 'page',
                        'action' => 'list'
                    ),
                    array(
                        'label' => t('Add Page'),
                        'permission' => 'application-page-add',
                        'link' => '/page/add',
                        'controller' => 'page',
                        'action' => 'add'
                    )
                )
            ),
            array(
                'label' => t("Block management"),
                'link' => 'javascript:void(0)',
                'class' => 'icon-list',
                'permission' => 'block-admin-list',
                'pages' => array(
                    array(
                        'label' => t("Block list"),
                        'link' => '/block-admin/list',
                        'permission' => 'block-admin-list'
                    ),
                    array(
                        'label' => t("Block Add"),
                        'link' => '/block-admin/add',
                        'permission' => 'block-admin-add'
                    )
                )
            ),
            array(
                'label' => t("Link management"),
                'link' => 'javascript:void(0)',
                'class' => 'icon-link',
                'permission' => 'link-admin-list',
                'pages' => array(
                    array(
                        'label' => t("Link Category list"),
                        'link' => '/link-admin/list-link-categories',
                        'permission' => 'link-admin-list-link-categories'
                    ),
                    array(
                        'label' => t("Link add Category"),
                        'link' => '/link-admin/add-link-category',
                        'permission' => 'link-admin-add-link-category'
                    ),
                /*     array(
                        'label' => t("Link list"),
                        'link' => '/link-admin/list',
                        'permission' => 'link-admin-list'
                    ),
                    array(
                        'label' => t("Link add"),
                        'link' => '/link-admin/add',
                        'permission' => 'link-admin-add'
                    ) */
                )
            ),
            array(
                'label' => t('Contents'),
                'class' => 'glyphicon glyphicon-list-alt',
                'link' => 'javascript:void(0)',
                'permission' => 'application-content-index',
                'pages' => array(
                    array(
                        'label' => t('Articles List'),
                        'link' => '/content/articles',
                        'permission' => 'application-content-articles',
                        'controller' => 'content',
                        'action' => 'articles'
                    ),
                    array(
                        'label' => t('Add Article'),
                        'link' => '/content/add-content',
                        'permission' => 'application-content-add-content',
                        'controller' => 'content',
                        'action' => 'add-content'
                    )
                )
            ),
            array(
                'label' => t('Tags'),
                'class' => 'icon-tag',
                'permission' => 'application-content-tags',
                'link' => 'javascript:void(0)',
                'pages' => array(
                    array(
                        'label' => t('Default Tags'),
                        'link' => '/content/tags',
                        'permission' => 'application-content-tags',
                        'controller' => 'content',
                        'action' => 'tags'
                    ),
                    array(
                        'label' => t('Add Tag'),
                        'link' => '/content/add-tag',
                        'permission' => 'application-content-add-tag',
                        'controller' => 'content',
                        'action' => 'add-tag'
                    ),
                    array(
                        'label' => t('User Tags'),
                        'link' => '/content/tags',
                        'permission' => 'application-user-tags',
                        'controller' => 'content',
                        'action' => 'tags'
                    )
                )
            ),
            
            array(
                'label' => t("Menus management"),
                'link' => 'javascript:void(0)',
                'class' => 'glyphicon glyphicon-tasks',
                'permission' => 'menu-admin-add-main-menu',
                'pages' => array(
                    array(
                        'label' => t("Menus list"),
                        'link' => '/menu-admin',
                        'controller' => 'menu',
                        'action' => 'list-main-menus',
                        'permission' => 'menu-admin-add-main-menu'
                    ),
                    array(
                        'label' => t("Add new main menu"),
                        'link' => '/menu-admin/add-main-menu',
                        'controller' => 'menu',
                        'action' => 'list-main-menus',
                        'permission' => 'menu-admin-add-main-menu'
                    )
                )
            ),
            array(
                'label' => t("Products management"),
                'link' => 'javascript:void(0)',
                'class' => 'glyphicon glyphicon-phone',
                'pages' => array(
                    
                    array(
                        'label' => t("Shop"),
                        'link' => 'javascript:void(0)',
                        'class' => 'glyphicon glyphicon-shop',
                        'permission' => 'products-shop-list',
                        'pages' => array(
                            array(
                                'label' => t("List Shop"),
                                'link' => '/shop/list',
                                'controller' => 'shop',
                                'action' => 'list',
                                'permission' => 'products-shop-list'
                            ),
                            array(
                                'label' => t("Add Shop"),
                                'link' => '/shop/add',
                                'controller' => 'shop',
                                'action' => 'add',
                                'permission' => 'products-shop-add'
                            )
                        )
                    ),
                    
                    array(
                        'label' => t("Products Group"),
                        'link' => 'javascript:void(0)',
                        'permission' => 'products-product-group-list',
                        'pages' => array(
                            array(
                                'label' => t("List Product Group"),
                                'link' => '/product-group/list',
                                'controller' => 'product-group',
                                'action' => 'list',
                                'permission' => 'products-product-group-list'
                            ),
                            array(
                                'label' => t("Add Product Group"),
                                'link' => '/product-group/add',
                                'controller' => 'product-group',
                                'action' => 'add',
                                'permission' => 'products-product-group-add'
                            )
                        )
                    ),
                    
                    array(
                        'label' => t("Products list"),
                        'permission' => 'products-product-list',
                        'link' => 'javascript:void(0)',
                        'pages' => array(
                            array(
                                'label' => t("List Product"),
                                'link' => '/product/list',
                                'controller' => 'product',
                                'action' => 'list',
                                'permission' => 'products-product-list'
                            ),
                            array(
                                'label' => t("Add Product"),
                                'link' => '/product/add',
                                'controller' => 'product',
                                'action' => 'add',
                                'permission' => 'products-product-add'
                            ),
                            array(
                                'label' => t("Add By Excel"),
                                'link' => '/product/import-excel',
                                'controller' => 'product',
                                'action' => 'import-excel',
                                'permission' => 'products-product-import-excel'
                            
                            )
                        )
                    ),
                    array(
                        'label' => t("Products Attribute"),
                        'link' => 'javascript:void(0)',
                        'permission' => 'products-attribute-list',
                        'pages' => array(
                            array(
                                'label' => t("List Product Attribute"),
                                'link' => '/attribute/list',
                                'controller' => 'attribute',
                                'action' => 'list',
                                'permission' => 'products-attribute-list'
                            ),
                            array(
                                'label' => t("Add Product Attribute"),
                                'link' => '/attribute/add',
                                'controller' => 'attribute',
                                'action' => 'add',
                                'permission' => 'products-attribute-add'
                            )
                        )
                    )
                
                )
            ),
            array(
                'label' => t("Logout"),
                'link' => 'javascript:void(0)',
                'permission' => 'application-admin-logout',
                'link' => '/logout',
                'class' => 'icon-off'
            )
        
        );
    }
}