<?php
namespace Application\ViewHelper;

use Zend\View\Helper\AbstractHelper;

class General extends AbstractHelper
{

    public function share($socialMedia, array $options)
    {
        switch ($socialMedia) {
            case 'facebook':
                $baseUrl = 'https://www.facebook.com/sharer/sharer.php';
                break;
            case 'twitter':
                $baseUrl = 'https://twitter.com/intent/tweet';
                break;
            case 'linkedin':
                $baseUrl = 'https://www.linkedin.com/shareArticle';
                break;
            case 'googleplus':
                $baseUrl = 'https://plus.google.com/share';
                break;
            // case 'instaram':
            // $baseUrl = '';
            // break;
        }
        foreach ($options as $key => $option) {
            @$content .= $key . '=' . $option . '&';
        }
        return $baseUrl . '?' . rtrim($content, '&');
    }
}