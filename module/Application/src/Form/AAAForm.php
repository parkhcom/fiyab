<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter; 

class AAAForm extends Form
{

    public function __construct()
    {
        parent::__construct('aaa-form');
        
        $this->setAttributes([
            'method' => 'post',
            'novalidate' => true
        ]);
        
        $title = new Element\Text('title');
        $title->setAttributes([
            'id' => 'application-page-title',
            'class' => 'form-control required'
        ]);
        $title->setLabel(t('Title'));
        $title->setLabelAttributes([
            'class' => 'required'
        ]);
        
        $csrf = new Element\Csrf('aaa_form_csrf');
        $csrf->setCsrfValidatorOptions([
            'timeout' => 600
        ]);
        
        $submit = new Element\Submit('submit');
        $submit->setAttributes([
            'id' => 'application-page-submit-button',
            'class' => 'btn btn-primary'
        ]);
        $submit->setValue(t('Submit'));
        
        $this->add($title)
            ->add($csrf)
            ->add($submit);
        $this->addInputFilter();
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => t('Required Field')
                        ]
                    ]
                ]
            ]
        ]);
    }
}