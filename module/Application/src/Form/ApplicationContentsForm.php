<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\Captcha\Image;

class ApplicationContentsForm extends Form
{

    public function __construct()
    {
        parent::__construct('application-contents-form');
        
        $this->setAttributes([
            'method' => 'post',
            'novalidate' => true
        ]);
        
        $title = new Element\Text('title');
        $title->setAttributes([
            'id' => 'application-content-title',
            'class' => 'form-control required'
        ]);
        $title->setLabel(t('Title'));
        $title->setLabelAttributes([
            'class' => 'required'
        ]);
        
        $slug = new Element\Text('slug');
        $slug->setAttributes([
            'id' => 'application-content-slug',
            'class' => 'form-control'
        ]);
        $slug->setLabel(t('Slug'));
        
        $metaDescription = new Element\Textarea('meta_description');
        $metaDescription->setAttributes([
            'id' => 'application-content-meta-description',
            'class' => 'form-control'
        ]);
        $metaDescription->setLabel(t('Meta Description'));
        
        $content = new Element\Textarea('content');
        $content->setAttributes([
            'id' => 'application-content-content',
            'class' => 'form-control'
        ]);
        $content->setLabel(t('Content'));
        $content->setLabelAttributes([
            'class' => 'required'
        ]);
        
        $status = new Element\Checkbox('status');
        $status->setAttributes([
            'id' => 'application-content-status',
            'class' => 'form-control'
        ]);
        $status->setLabel(t('Status'));
        
        $mainImage = new Element\File('main_image');
        $mainImage->setAttributes([
            'id' => 'application-content-main-image',
            'class' => 'form-control'
        ]);
        $mainImage->setLabel(t('Main Image'));
        
        $csrf = new Element\Csrf('application_contents_form_csrf');
        $csrf->setCsrfValidatorOptions([
            'timeout' => 600
        ]);
        
        $submit = new Element\Submit('submit');
        $submit->setAttributes([
            'id' => 'application-content-submit-button',
            'class' => 'btn btn-primary'
        ]);
        $submit->setValue(t('Submit'));
        
        $this->add($title)
            ->add($slug)
            ->add($metaDescription)
            ->add($content)
            ->add($status)
            ->add($mainImage)
            ->add($csrf)
            ->add($submit);
        $this->addInputFilter();
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => t('Required Field')
                        ]
                    ]
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'content',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => t('Required Field')
                        ]
                    ]
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'slug',
            'required' => false,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 'meta_description',
            'required' => false,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'status',
            'required' => false,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
        
        $inputFilter->add([
            'type' => 'Zend\InputFilter\FileInput',
            'name' => 'main_image',
            'required' => false,
            'validators' => [
                [
                    'name' => 'FileUploadFile'
                ],
                [
                    'name' => 'FileIsImage'
                ],
                [
                    'name' => 'FileImageSize',
                    'options' => [
                        'minWidth' => 128,
                        'minHeight' => 128,
                        'maxWidth' => 4096,
                        'maxHeight' => 4096
                    ]
                ]
            ],
            'filters' => [
                [
                    'name' => 'FileRenameUpload',
                    'options' => [
                        'target' => __DIR__ . '/../../../../public/img/upload/',
                        'useUploadName' => true,
                        'useUploadExtension' => true,
                        'overwrite' => false,
                        'randomize' => true
                    ]
                ]
            ]
        ]);
    }
}