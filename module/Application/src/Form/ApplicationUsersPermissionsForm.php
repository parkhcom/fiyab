<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\Captcha\Image;

class ApplicationUsersPermissionsForm extends Form
{

    public function __construct()
    {
        parent::__construct('application-users-permissions-form');
        
        $this->setAttributes([
            'method' => 'post',
            'novalidate' => true
        ]);
        $permissionName = new Element\Text('permission_name');
        $permissionName->setAttributes([
            'id' => 'permission-name',
            'class' => 'form-control'
        ]);
        $permissionName->setLabel(t('Permission Name'));
        
        $permissionLabel = new Element\Text('permission_label');
        $permissionLabel->setAttributes([
            'id' => 'permission-lebel',
            'class' => 'form-control'
        ]);
        $permissionLabel->setLabel(t('Permission Label'));
        
        $permissionException = new Element\Checkbox('exception');
        $permissionException->setAttributes([
            'id' => 'permission-exception',
            'class' => 'form-control'
        ]);
        $permissionException->setLabel(t('Permission Exception'));
        
        $csrf = new Element\Csrf('application_users_permissions_form_csrf');
        $csrf->setCsrfValidatorOptions([
            'timeout' => 600
        ]);
        
        $submit = new Element\Submit('submit');
        $submit->setAttributes([
            'id' => 'signup-form-submit',
            'class' => 'btn btn-primary'
        ]);
        $submit->setValue(t('Add'));
        
        $this->add($permissionName)
            ->add($permissionLabel)
            ->add($permissionException)
            ->add($csrf)
            ->add($submit);
        $this->addInputFilter();
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
            'name' => 'permission_name',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => array(
                            'isEmpty' =>t('Required Field')
                        )
                    ]
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'permission_label',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => array(
                            'isEmpty' =>t('Required Field')
                        )
                    ]
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'exception',
            'required' => false,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
    }
}