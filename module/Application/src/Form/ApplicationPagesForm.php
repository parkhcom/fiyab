<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\Captcha\Image;

class ApplicationPagesForm extends Form
{

    public function __construct()
    {
        parent::__construct('application-pages-form');
        
        $this->setAttributes([
            'method' => 'post',
            'novalidate' => true
        ]);
        
        $title = new Element\Text('title');
        $title->setAttributes([
            'id' => 'application-page-title',
            'class' => 'form-control'
        ]);
        $title->setLabel(t('Title'));
        $title->setLabelAttributes([
            'class' => 'required'
        ]);
        
        $slug = new Element\Text('slug');
        $slug->setAttributes([
            'id' => 'application-pages-slug',
            'class' => 'form-control'
        ]);
        $slug->setLabel(t('Slug'));
        $slug->setLabelAttributes([
            'class' => 'required'
        ]);
        
        $lang = new Element\Select('lang');
        $lang->setAttributes([
            'id' => 'application-pages-lang',
            'class' => 'form-control'
        ]);
        $lang->setValueOptions([
            'en' => t('English'),
            'fa' => t('Persian'),
        ]);
        $lang->setLabel(t('Language'));
        $lang->setLabelAttributes([
            'class' => 'required'
        ]);
        
        
        $metaDescription = new Element\Textarea('meta_description');
        $metaDescription->setAttributes([
            'id' => 'application-page-meta-description',
            'class' => 'form-control'
        ]);
        $metaDescription->setLabel(t('Meta Description'));
        
        $content = new Element\Textarea('content');
        $content->setAttributes([
            'id' => 'application-page-content',
            'class' => 'form-control'
        ]);
        $content->setLabel(t('Content'));
        $content->setLabelAttributes([
            'class' => 'required'
        ]);
        
        $status = new Element\Checkbox('status');
        $status->setAttributes([
            'id' => 'application-page-status',
            'class' => 'form-control'
        ]);
        $status->setLabel(t('Status'));
        
        $csrf = new Element\Csrf('application_page_form_csrf');
        $csrf->setCsrfValidatorOptions([
            'timeout' => 600
        ]);
        
        $submit = new Element\Submit('submit');
        $submit->setAttributes([
            'id' => 'application-page-submit-button',
            'class' => 'btn btn-primary'
        ]);
        $submit->setValue(t('Submit'));
        
        $this->add($title)
            ->add($metaDescription)
            ->add($slug)
            ->add($lang)
            ->add($content)
            ->add($status)
            ->add($csrf)
            ->add($submit);
        $this->addInputFilter();
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => t('Required Field')
                        ]
                    ]
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'content',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => t('Required Field')
                        ]
                    ]
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'slug',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 'meta_description',
            'required' => false,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'status',
            'required' => false,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'lang',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
    }
}