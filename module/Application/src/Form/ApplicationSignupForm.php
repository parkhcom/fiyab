<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\Captcha\Image;

class ApplicationSignupForm extends Form

{

    public function __construct()
    {
        parent::__construct('signup-form');
        
        $this->setAttributes([
            'method' => 'post',
            'id' => 'registrationForm',
            'novalidate' => true
        ]);
        $email = new Element\Email('email');
        $email->setAttributes([
            'id' => 'signup-form-email',
            'class' => 'form-control required validate[required, custom[email]] ',
            'placeholder' => t('Email')
        ]);
        
        $password = new Element\Password('password');
        $password->setAttributes([
            'id' => 'password',
            'class' => 'form-control validate[required ]',
            'placeholder' => t('Password')
        ]);
        
        $rpassword = new Element\Password('repeat_password');
        $rpassword->setAttributes([
            'id' => 'signup-form-password',
            'class' => 'form-control validate[required,equals[password]] ',
            'placeholder' => t('Repeat Password')
        ]);
 
        $image = array(
            'font' => $_SERVER['DOCUMENT_ROOT'] . "/fonts/arial.ttf", 
            'wordLen' => 4,
            'timeout' => 300,
            'width' => 100,
            'height' => 50,
            '_fsize' => 14,
            'lineNoiseLevel' => 3,
            'dotNoiseLevel' => 5
        );
        $captchaImage = new Image($image);
        $captchaImage->setImgDir($_SERVER['DOCUMENT_ROOT'] . "/captcha");
        $captchaImage->setImgUrl("/captcha");
        
        $captcha = new Element\Captcha('captcha');
        
        $captcha->setLabel(t("Im not robot"));
        
        $captcha->setOptions(array(
            'captcha' => $captchaImage
        ));
        $captcha->setAttributes(array(
            'id' => "captcha-id",
            'class' => 'form-control validate[required]',
            'placeholder' => t("Enter image words here")
        ));
        $fullname = new Element\Text('fullname');
        $fullname->setAttributes([
            'id' => 'signin-form-fullname',
            'class' => 'form-control validate[required]',
            'placeholder' => t('Full Name')
        ]);
        $csrf = new Element\Csrf('signup_csrf');
        $csrf->setCsrfValidatorOptions([
            'timeout' => 600
        ]);
        
        $submit = new Element\Submit('submit');
        $submit->setAttributes([
            'id' => 'signup-form-submit',
            'class' => 'btn btn-primary'
        ]);
        $submit->setValue(t('Signup'));
        
        $this->add($email)
            ->add($rpassword)
            ->add($captcha)
            ->add($fullname)
            ->add($password)
            ->add($csrf)
            ->add($submit);
        $this->addInputFilter();
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
            'name' => 'email',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                        'useMxCheck' => false,
                        'messages' => [
                            'emailAddressInvalidFormat' => t('Invalid email address'),
                            'emailAddressDotAtom' => '',
                            'emailAddressQuotedString' => '',
                            'emailAddressInvalidLocalPart' => t('Invalid email address')
                        ]
                    ]
                
                ],
                [
                    'name' => 'Regex',
                    'options' => [
                        'pattern' => '/^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/',
                        'messages' => [
                            \Zend\Validator\Regex::NOT_MATCH => ''
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $inputFilter->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
    }
}
