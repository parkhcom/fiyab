<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
 

class ApplicationSigninForm extends Form

{

    public function __construct()
    {
        parent::__construct('signin-form');

        $this->setAttributes([
            'method' => 'post',
            'id'     => 'loginForm',
            'novalidate' => true
        ]);
        /* $email = new Element\Email('email');
        $email->setAttributes([
            'id' => 'signin-form-email',
            'class' => 'form-control',
            'placeholder' => t('Email')
        ]); */
        $email = new Element\Text('username');
        $email->setAttributes([
            'id' => 'signin-form-email', 
            'class' 	  => 'form-control required validate[required]',
            'placeholder' => t('Email')
        ]);
        
        $password = new Element\Password('password');
        
        $password->setAttributes([
            'id' => 'signin-form-password',
            'class'		  => 'form-control required validate[required]',
            'placeholder'		  => t("Password"),  
        ]);

        $csrf = new Element\Csrf('signin_csrf');
        $csrf->setCsrfValidatorOptions([
            'timeout' => 600
        ]);

        $submit = new Element\Submit('submit');
        $submit->setAttributes([
            'id' => 'signin-form-submit',
            'class' => 'btn btn-primary'
        ]);
        $submit->setValue(t('Signin'));

        $this->add($email)
            ->add($password)
            ->add($csrf)
            ->add($submit);
        $this->addInputFilter();
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

/*         $inputFilter->add([
            'name' => 'email',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ],
            'validators' => [
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                        'useMxCheck' => false,
                        'messages' => [
                            'emailAddressInvalidFormat' => t('Invalid email address'),
                            'emailAddressDotAtom' => '',
                            'emailAddressQuotedString' => '',
                            'emailAddressInvalidLocalPart' => t('Invalid email address'),
                        ]
                    ]

                ],
                [
                    'name' => 'Regex',
                    'options' => [
                        'pattern' => '/^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/',
                        'messages' => [
                            \Zend\Validator\Regex::NOT_MATCH => ''
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]); */
        $inputFilter->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'StripTags'
                ]
            ]
        ]);
    }
}
