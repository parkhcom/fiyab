<?php
namespace Application;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;
use Application\Helper\MyHelper;
 

class Module implements ConfigProviderInterface
{

    const VERSION = '3.0.2dev';

    public function onBootstrap(MvcEvent $event)
    {
        $application = $event->getApplication();
        $serviceManager = $application->getServiceManager();
        
        // The following line instantiates the SessionManager and automatically
        // makes the SessionManager the 'default' one.
        $sessionManager = $serviceManager->get(SessionManager::class);
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'myHelper' => function () {
                    return new MyHelper();
                }, 
               /*  'Plugins' => function ($sm) {
                   $myplg = new Plugins();
                   $myplg->setServiceLocator($sm);
                   return $myplg;
                }, */
                
            )
        
        );
    }
    public function getControllerPluginConfig()
    {
        return array(
            'factories' => array(
                'myHelper' => function () {
                    return new MyHelper();
                }, 
           
                
            )
        
        );
    }
}
