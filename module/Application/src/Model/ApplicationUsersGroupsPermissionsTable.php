<?php
namespace Application\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ApplicationUsersGroupsPermissionsTable
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_users_groups_permissions', $dbAdapter);
    }

    public function getPermissions($userGroupId = null)
    {
        $select = new Select('application_users_groups_permissions');
        $select->join('application_users_permissions', 'application_users_permissions.id  = application_users_groups_permissions.permission_id', [
            'permission_name',
            'exception'
        ], Select::JOIN_INNER);
        
        if ($userGroupId) {
            $select->where([
                'user_group_id' => $userGroupId
            ]);
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function delete($permId)
    {
        if ($permId > 0 && is_numeric($permId)) {
            return $this->tableGateway->delete([
                'permission_id' => $permId
            ]);
        }
    }

    public function add($permId, $groupId)
    {
        if (is_numeric($permId) && is_numeric($groupId)) {
            $this->tableGateway->insert([
                'permission_id' => $permId,
                'user_group_id' => $groupId
            ]);
            return $this->tableGateway->lastInsertValue;
        }
    }
}