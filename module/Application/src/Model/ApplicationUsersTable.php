<?php
namespace Application\Model;
 
use Zend\Db\ResultSet\ResultSet;
use Application\Entity\User;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Select;

class ApplicationUsersTable
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $user = new User();
        
        $user->setServiceLocator($serviceManager);
        $resultSetPrototype->setArrayObjectPrototype($user);
        $this->tableGateway = new TableGateway('application_users', $dbAdapter, null, $resultSetPrototype);
    }

    public function fetch()
    {
        $select = new Select('application_users');
        return $select; 
    }
    public function getList()
    {
        $select = new Select('application_users'); 
        return $select; 
    }
    public function fetchUser($userId)
    {
        if ($userId > 0 && is_numeric($userId)) {
            $result = $this->tableGateway->select([
                'id' => $userId
            ]);
            return $result->current();
        }
        return false;
    }

    public function saveUser(User $user)
    {
        $data = [
            'email' => $user->email,
            'password' => $user->password,
            'user_group_id' => $user->user_group_id,
            'salt' => $user->salt
        ];
        
        $id = (int) $user->id;
        
        if ($id === 0) {
            $this->tableGateway->insert($data);
            return;
        }
        
        $this->tableGateway->update($data, [
            'id' => $id
        ]);
    }

    public function updateLastLoginTime($userId)
    {
        if ($userId > 0 && is_numeric($userId)) {
            return $this->tableGateway->update(array(
                'last_login' => time()
            ), array(
                'id' => $userId
            ));
        }
    }
}