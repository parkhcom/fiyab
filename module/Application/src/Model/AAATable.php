<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Application\Interfaces\CrudInterface;

class AAATable implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('aaa', $dbAdapter);
    }

    public function fetch(array $where = null)
    {
        $select = new Select('aaa');
        if ($where) {
            $select->where($where);
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }


    public function add(array $data)
    {
        $this->tableGateway->insert([
            'x' => $data['x'],
        ]);
        return $this->tableGateway->getLastInsertValue();
    }

    public function update(int $id, array $data)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->update([
                'x' => $data['x'],
            ], [
                'id' => $id
            ]);
        }
    }

    public function delete(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'id' => $id
            ]);
        }
    }
}