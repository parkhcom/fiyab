<?php
namespace Application\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 

class ApplicationUsersGroupsTable //implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_users_groups', $dbAdapter);
    }
    
    public function fetch(array $where = null) {
        return $this->tableGateway->select()->toArray();
    }

    public function add(array $data) {
        
    }

    public function update(int $id, array $data) {
        
    }

    public function delete(int $id) {
        
    }
    
}