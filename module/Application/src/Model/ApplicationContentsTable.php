<?php
namespace Application\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 

class ApplicationContentsTable //implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_contents', $dbAdapter);
    }

    public function fetch(array $where = null)
    {
        $select = new Select('application_contents');
        if ($where) {
            $select->where($where);
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchByAjaxCall(int $offset, int $limit, string $lang)
    {
        $select = new Select('application_contents');
        $select->offset($offset);
        $select->limit($limit);
        $select->where->equalTo('lang', $lang);
        $select->where->equalTo('status', 1);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchSearchQueries($query, $limit)
    {
        $select = new Select('application_contents');
        $select->where->expression("MATCH(title, content) AGAINST (?)", $query);
        $select->limit($limit);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function add(array $data)
    {
        $this->tableGateway->insert([
            'user_id' => $data['user_id'],
            'type' => (int) $data['type'],
            'title' => $data['title'],
            'slug' => $data['slug'],
            'content' => $data['content'],
            'main_image' => $data['main_image'],
            'meta_description' => $data['meta_description'],
            'created_at' => time(),
            'lang' => $data['lang'],
            'user_ip' => $data['user_ip']
        ]);
        return $this->tableGateway->getLastInsertValue();
    }

    public function update(int $id, array $data)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->update([
                'title' => $data['title'],
                'slug' => $data['slug'],
                'content' => $data['content'],
                'main_image' => $data['main_image'],
                'meta_description' => $data['meta_description'],
                'updated_at' => time(),
                'status' => $data['status'],
                'lang' => $data['lang'],
                'user_ip' => $data['user_ip']
            ], [
                'id' => $id
            ]);
        }
    }

    public function delete(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'id' => $id
            ]);
        }
    }
}