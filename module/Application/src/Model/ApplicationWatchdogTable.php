<?php
namespace Application\Model;


use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;

class ApplicationWatchdogTable
{

    protected $tableGateway;

    protected $serviceManager;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $dbAdapter = $this->serviceManager->get(AdapterInterface::class); 
        $this->tableGateway = new TableGateway('application_watchdog', $dbAdapter);
    }

    public function add($postData)
    {
 
        $toSaveData = array(
            'user_id' => ($postData['user_id']),
            'type' => ($postData['type']),
            'message' => ($postData['message']),
            'severity' => ($postData['severity']),
            'location' => ($postData['location']),
            'referer' => ($postData['referer']),
            'hostname' => ($postData['hostname']),
            'timestamp' => time(),
            'data' => ($postData['data']),
            'data_before' => ($postData['data_before'])
        );
        return $this->tableGateway->insert($toSaveData);
    }
}
