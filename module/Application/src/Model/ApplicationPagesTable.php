<?php
namespace Application\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 

class ApplicationPagesTable //implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_pages', $dbAdapter);
    }

    public function fetch(array $where = null)
    {
        $select = new Select('application_pages');
        if ($where) {
            $select->where($where);
        }
        $select->order('slug ASC');
        return $this->tableGateway->selectWith($select)->toArray();
    }


    public function add(array $data)
    {
        $this->tableGateway->insert([
            'user_id' => $data['user_id'],
            'title' => $data['title'],
            'slug' => $data['slug'],
            'content' => $data['content'],
            'meta_description' => $data['meta_description'],
            'created_at' => time(),
            'lang' => $data['lang'],
            'status' => $data['status'],
            'user_ip' => $data['user_ip']
        ]);
        return $this->tableGateway->getLastInsertValue();
    }

    public function update(int $id, array $data)
    {
        if ($id > 0 && is_numeric($id)) { 
            $res =  $this->tableGateway->update([
                'title' => $data['title'],
                'slug' => $data['slug'],
                'content' => $data['content'],
                'meta_description' => $data['meta_description'], 
                'modified_date' => time(),
                'status' => $data['status'],
                'lang' => $data['lang'],
                'user_ip' => $data['user_ip']
            ], [
                'id' => $id
            ]);
          
            return $res;
        }
    }

    public function delete(int $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete([
                'id' => $id
            ]);
        }
    }
}