<?php
namespace Application\Model;
 
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Select;

class ApplicationUsersBlockIpTable
{ 
    public $tableGateway; 
    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_users_block_ip', $dbAdapter);
    }

    public function fetch($ip = null)
    {
        $select = new Select('application_users_block_ip');
        if ($ip) {
            $select->where->equalTo('ip', $ip);
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function add($ip)
    {
        return $this->tableGateway->insert(array(
            'ip' => $ip,
            'expiry_date' => 60 * 60 * 2,
            'login_time' => time()
        ));
    }

    public function updateIp($validData, $ip)
    {
        if ($ip) {
            return $this->tableGateway->update(array(
                'ip' => $validData['ip'],
                'expiry_date' => $validData['expiry_date']
            ), array(
                'ip' => $ip
            ));
        }
    }

    public function delete($ip)
    {
        if ($ip) {
            return $this->tableGateway->delete(array(
                'ip' => $ip
            ));
        }
    }
}
