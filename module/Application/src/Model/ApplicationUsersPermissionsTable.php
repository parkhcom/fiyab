<?php
namespace Application\Model;
 
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 
use Zend\Db\Sql\Where;

class ApplicationUsersPermissionsTable // implements CrudInterface
{

    public $tableGateway;

    public function __construct($serviceManager)
    {
        $dbAdapter = $serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_users_permissions', $dbAdapter);
    }

    public function getDefaultPermission()
    {
        $select = new Select('application_users_permissions');
        $select->where([
            'exception' => 1
        ]);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function checkExist(string $permissionName, int $permissionId = null)
    {
        if ($permissionName) {
            if ($permissionId) {
                return $this->tableGateway->select([
                    'permission_name' => $permissionName,
                    'id != ?' => $permissionId
                ])->toArray();
            } else {
                return $this->tableGateway->select([
                    'permission_name' => $permissionName
                ])->toArray();
            }
        }
        return false;
    }

    public function fetch($where = null)
    {
        $select = new Select('application_users_permissions');
        if ($where) {
            $select->where($where);
        }
        $select->order('permission_name ASC');
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function add(array $data)
    {
        if ($data) {
            return $this->tableGateway->insert([
                'module_name' => $data['module_name'],
                'permission_name' => $data['permission_name'],
                'permission_label' => $data['permission_label'],
                'exception' => $data['exception']
            ]);
        }
        return false;
    }

    public function update($id, array $data)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->update([
                'module_name' => $data['module_name'],
                'permission_name' => $data['permission_name'],
                'permission_label' => $data['permission_label'],
                'exception' => $data['exception']
            ], [
                'id' => $id
            ]);
        }
    }

    public function delete(int $permissionId)
    {
        if ($permissionId > 0 && is_numeric($permissionId)) {
            return $this->tableGateway->delete([
                'id' => $permissionId
            ]);
        }
    }

    public function deleteWhere($where)
    {
        if ($where) {
            return $this->tableGateway->delete($where);
        }
    }

    public function deleteAll()
    {
        $where = new Where();
        $where->greaterThan('id', 0);
        return $this->tableGateway->delete($where);
    }
}