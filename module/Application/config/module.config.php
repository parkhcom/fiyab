<?php
namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Helper\Tools;
$tools = new Tools();

return [
    'router' => [
        'routes' => [
            'base' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'application' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '[/:lang][/:action]',
                    'constraints' => [
                        'lang' => '[a-z]{2}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index',
                        //TODO
                        // temporarily disabled to make dynamic breadcrumb working
                        // this may cause other urls no to work properly
                        // 'lang' => 'en'
                    ]
                ]
            ],
            'application-content' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '[/:lang][/:action]/:id/:slug',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'lang' => '[a-z]{2}',
                         'id' => '[1-9][0-9]*',
                        'slug' => '.+',
                    ], 
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'content',
                        'lang' => 'en'
                    ]
                ]
            ],
            'application-admin' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '[/:lang]/dashboard[/:action][/page/:page][/id/:id][/token/:token]',
                    'constraints' => [
                        'lang' => '[a-z]{2}',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}',
                        'page' => '[1-9][0-9]*'
                    ],
                    'defaults' => [
                        'controller' => Controller\AdminController::class,
                        'action' => 'dashboard' 
                    ]
                ]
            ],
 
            'application-permission' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '[/:lang]/permission[/:action][/id/:id]',
                    'constraints' => [
                        'lang' => '[a-z]{2}',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}',
                        'page' => '[1-9][0-9]*'
                    ],
                    'defaults' => [
                        'controller' => Controller\PermissionController::class,
                        'action' => 'assign-permission-to-user-group' 
                    ]
                ]
            ],
            'application-page' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '[/:lang]/page[/:action][/id/:id]',
                    'constraints' => [
                        'lang' => '[a-z]{2}',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}',
                        'page' => '[1-9][0-9]*'
                    ],
                    'defaults' => [
                        'controller' => Controller\PageController::class,
                        'action' => 'add' 
                    ]
                ]
            ],
            'application-ajax' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/refreshcaptcha',
                    'constraints' => [ 
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*', 
                    ],
                    'defaults' => [
                        'controller' => Controller\AjaxController::class,
                        'action' => 'refresh-captcha' 
                    ]
                ]
            ]
 
        ]
    ],
    'translator' => [
        'locale' => (new Tools())->getLocale(),
        'translation_file_patterns' => [
            /*
             * [
             * 'type' => 'phparray',
             * 'base_dir' => __DIR__ . '/../language',
             * 'pattern' => '%s.php'
             * ]
             */
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo'
            ]
        ]
    ],
    
    'controllers' => [
        'factories' => [
            Controller\PermissionController::class => InvokableFactory::class,
            Controller\IndexController::class => InvokableFactory::class,
            Controller\AdminController::class => InvokableFactory::class, 
            Controller\ContentController::class => InvokableFactory::class,
            Controller\PageController::class => InvokableFactory::class,
            Controller\AjaxController::class => InvokableFactory::class
        ], 
        'aliases' => [
            'permission' => Controller\PermissionController::class,
            'admin' => Controller\AdminController::class, 
            'content' => Controller\ContentController::class,
            'page' => Controller\PageController::class,
            'ajax' => Controller\AjaxController::class
        ]
    ],
    
    
    'controller_plugins' => [
        'factories' => [
            ControllerPlugin\MailService::class => InvokableFactory::class
        ],
        'aliases' => [
            'mailService' => ControllerPlugin\MailService::class
        ]
    ],
    
    'service_manager' => [
        'factories' => [
            'Application\Cache\Redis' => 'Application\Service\Factory\RedisFactory',
        ],
        
        'aliases' => [
            'translator' => 'MvcTranslator'
        ]
    ],
    'view_helpers' => [
        'factories' => [
            ViewHelper\Menu::class => InvokableFactory::class,
            ViewHelper\Breadcrumb::class => InvokableFactory::class,
            ViewHelper\General::class => InvokableFactory::class,
        ],
        'aliases' => [
            'menu' => ViewHelper\Menu::class,
            'breadcrumb' => ViewHelper\Breadcrumb::class,
            'general' => ViewHelper\General::class,
        ]
    ],
    
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        
        // Its faster than template_path_stack
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'application/index/signin' => __DIR__ . '/../view/application/index/signin.phtml',
            'application/index/logout' => __DIR__ . '/../view/application/index/logout.phtml',
            'application/index/signup' => __DIR__ . '/../view/application/index/signup.phtml',
            'application/index/password-forgotten' => __DIR__ . '/../view/application/index/password-forgotten.phtml',
            'application/index/reset-password' => __DIR__ . '/../view/application/index/reset-password.phtml',
            'application/index/about' => __DIR__ . '/../view/application/index/about.phtml',
            'application/index/contact-us' => __DIR__ . '/../view/application/index/contact-us.phtml',
            'application/index/terms' => __DIR__ . '/../view/application/index/terms.phtml',
            'application/index/cooperation' => __DIR__ . '/../view/application/index/cooperation.phtml',
            'application/index/advertisement' => __DIR__ . '/../view/application/index/advertisement.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
            'blockedUserLayout' => __DIR__ . '/../view/application/index/blocked-user-layout.phtml',
            'adminMenubar' => __DIR__ . '/../view/layout/admin-menu.phtml',
            
        ],
        
        // It automatically finds the view file but a little bit slower
        'template_path_stack' => [
            __DIR__ . '/../view'
        ]
    ]
];
