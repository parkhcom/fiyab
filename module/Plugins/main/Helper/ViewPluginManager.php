<?php
namespace Plugins\Helper;

use Zend\View\Helper\AbstractHelper;
use Application\Traits\HelperTrait; 
use Zend\View\HelperPluginManager;
use Zend\View\Model\ViewModel;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Http\Request;
  

class ViewPluginManager extends AbstractHelper  
{
 
	/**  
	 * @var \Plugins\PluginService\PluginLoader
	 */
    protected $serviceLocator;
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        if($this->serviceLocator !== null) return $this;
        $this->serviceLocator = $serviceLocator; return $this;
        
    }
    public function getServiceLocator()
    {
        return  $this->serviceLocator;
        
    }
	
	/**
	 * Get exists plugin (in controller) or get view html of the given pluginName
	 * @param string $pluginName
	 * @param mixed $variables
	 * @param string $checkExists
	 * @return Ambigous <string, unknown>|string|\Zend\View\Model\ViewModel|Ambigous <\Closure, \Zend\View\Model\ViewModel>
	 */
	public function __invoke($pluginName, $variables = false, $checkExists = true)
	{    
	    $classArray = explode("/", $pluginName);
	    $plugin = $classArray[0];
	    $controller = $classArray[1];
	    $method = $classArray[2];
	    $this->request = new Request();
	    $classArray[0] = str_replace("-"," ",$classArray[0]);
	    $classArray[0] = ucwords($classArray[0]);
	    $classArray[0] = str_replace(" ","",$classArray[0]);
	    
	    $className = $classArray[0].'\Controller\\' . ucfirst($classArray[1]) . "Controller";
 
	    $classInstance = new $className($this->getServiceLocator(), $GLOBALS["lang"]);
	    $viewModel = $classInstance->{$method . "Action"}($variables);
	    $viewRender = $this->getServiceLocator()->get('ViewRenderer');
	    if ($viewModel){  
	        $html = $viewRender->render($viewModel); 
	        return $html;
	    }
	       
	}
	  
}