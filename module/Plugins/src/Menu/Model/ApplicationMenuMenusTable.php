<?php 
namespace Menu\Model;

use Zend\Db\TableGateway\TableGateway; 
use Zend\Db\Sql\Select;  
use Zend\Db\Adapter\AdapterInterface;

class ApplicationMenuMenusTable 
{ 
    public $tableGateway;
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $dbAdapter = $this->serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_sub_menus', $dbAdapter);
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getMenu($menuId)
	{
		$select = new Select('application_sub_menus');
		$select->where(array(
				'id' => $menuId
		));
		return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getMenuByMainMenuId($menuId)
	{
		$select = new Select('application_sub_menus');
		$select->join('application_menus', 'application_menus.id = application_sub_menus.main_menu_id', array('lang','name'), SELECT::JOIN_LEFT);
		$select->where(array(
				'main_menu_id' => $menuId
		));
		$select->where->equalTo('active', 1);
		
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getMenuByParentId($menuId)
	{
	    $select = new Select('application_sub_menus');
	    $select->where(array(
	        'parent_id' => $menuId
	    ));
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getMainMenuById($parentId)
	{
	    $select = new Select('application_sub_menus');
	    $select->columns(array(
	        'id' , 
	        'parent_id','caption','order','active'
	    ));
	    
	     
	    $select->order("order ASC");
	    $select->where->equalTo('parent_id', (int)$parentId);
	    $select->where->equalTo('active', 1);
	    return $this->tableGateway->selectWith($select)->toArray();
	    //return $select;
	}
	
	public function deleteMenu($menuId)
	{
		if ($menuId) {
			return $this->tableGateway->delete(array(
					'id' => $menuId
			));
		}
	}
	
	public function editUlLi($menuId, $menuData)
	{
	    $data = array(
	        "class_name_ul" => $menuData["class_name_ul"],
	        "class_name_li" => $menuData["class_name_li"],
	    );
	    return $this->tableGateway->update($data,  array('id' => $menuId));
	}
	
	public function editMenu($menuId, $menuData)
	{
	    $data = array(
	        "caption" => $menuData["caption"],
	        "external_url" => $menuData["external_url"],
	        "internal_url" => ($menuData["internal_url"]==""?null:$menuData["internal_url"]),
	        "order" => $menuData["order"], 
	        "icon" => $menuData["icon"],
	        "image" => $menuData["image"],
	        "active" => $menuData["active"],
	    //    "class_name_ul" => $menuData["class_name_ul"],
	    //     "class_name_li" => $menuData["class_name_li"],
	        "parent_id" => $menuData["parent_id"],
	    );
	    return $this->tableGateway->update($data,  array('id' => $menuId));
	}
	public function activeDeactiveMenu($menuId, $active = 0)
	{
	    $data = array( 
	        "active" => $active,  
	    );
	    return $this->tableGateway->update($data,  array('main_menu_id' => $menuId));
	}
	public function addMenu($mainMenuId, $menuData)
	{
		$data = array(
				"main_menu_id" => $mainMenuId,
				"caption" => $menuData["caption"],
    		    "external_url" => $menuData["external_url"],
    		    "internal_url" => ($menuData["internal_url"]==""?null:$menuData["internal_url"]),
				"order" => $menuData["order"], 
		        "icon" => $menuData["icon"],
		        "image" => $menuData["image"],
		        "active" => $menuData["active"],
    		    "parent_id" => $menuData["parent_id"],
		);
		$result = $this->tableGateway->insert($data);
		if ($result) {
		    return $this->tableGateway->lastInsertValue;
		} else {
		    return false;
		}
	}
	
	public function getMenuChilds($menuId)
	{
	    $ALLDATA = array();
	    $Cond = '';
	    $Tree = $this->tableGateway->select(array('parent_id = ?' => $menuId));
	    $statement = $this->adapter->query("
	        SELECT id FROM application_sub_menus
	        WHERE parent_id = $menuId AND id <> $menuId   ");
	    $Tree = $statement->execute();
	    $myData = array();
	    if($Tree)
	    {
	        $IDS = false;
	        foreach ($Tree as $key => $value) {
	            if(isset($value['id']))
	            {
	                $myData[] =   $value['id'];
	            }
	        }
	        $ALLDATA[] = $IDS = implode(",", $myData);
	    }
	    $myData = null;
	    if($IDS)
	    {
	        do
	        {
	     	 	    $statement = $this->adapter->query("
             			SELECT id  FROM application_sub_menus
             	 	            WHERE parent_id IN (".$IDS.")");
	     	 	    $results = $statement->execute();
	     	 	    if($results)
	     	 	    {
	     	 	        $IDS = false;
	     	 	        $myData = null;
	     	 	        foreach ($results as $key => $value) {
	     	 	            if(isset($value['id']))
	     	 	            {
	     	 	                $myData[] =   $value['id'];
	     	 	                //   var_dump($IDS);
	     	 	            }
	     	 	        }
	     	 	        if ($myData) {
	     	 	            $ALLDATA[] = $IDS = implode(",", $myData);
	     	 	        }
	     	 	    } else
	     	 	        $IDS = false;
	        }while($IDS);
	         
	    }
	    return $ALLDATA;
	}
}
