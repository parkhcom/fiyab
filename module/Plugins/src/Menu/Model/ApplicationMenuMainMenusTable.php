<?php 
namespace Menu\Model;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;

class ApplicationMenuMainMenusTable
{
    
    public $tableGateway;
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $dbAdapter = $this->serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_menus', $dbAdapter);
    }
    
	public function getRecords($id = false)
	{
	    $select = new Select('application_menus');
	    if ($id) {
	        $select->where->equalTo('id', $id);
	        return $this->tableGateway->selectWith($select)->current();
	    }
		return $this->tableGateway->select()->toArray();
	}
	
	public function fetchAllMainMenus()
	{
		$select = new Select('application_menus');
		return $select;
		//return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getMainMenuById($mainMenuId, $paretntId = false)
	{
		$select = new Select('application_menus');
		if ($mainMenuId) {
			$select->where->equalTo('application_menus.id', $mainMenuId);
		}
		$select->join('application_sub_menus', 'application_sub_menus.main_menu_id = application_menus.id',array(
		 	    'main_menu_id' ,
		    'parent_id',
		    'caption',
		    'external_url',
		    'internal_url',
		    'class_name',
		    'order',
		    'class_name_ul',
		    'class_name_li',
		    'active',
		    'width',
		    'image',
		    'icon' 
		),$select::JOIN_LEFT);
		$select->order("order");
// 		$select->where->equalTo('application_sub_menus.active', 1);
		if($paretntId){
		  //  $select->where->equalTo('parent_id', (int)$paretntId);
		}
		return $this->tableGateway->selectWith($select)->toArray();
		//return $select;
	}
 
	public function getMainMenuView($menuLanguage = null, $mainMenuPosition = null)
	{
	    $select = new Select('application_menus');
	    if ($mainMenuPosition) {
	        $select->where->equalTo('application_menus.position', $mainMenuPosition);
	    }
	    if ($menuLanguage) {
	        $select->where->equalTo('application_menus.lang', $menuLanguage);
	    }
	    $select->join('application_sub_menus', 'application_sub_menus.main_menu_id = application_menus.id',array(
	        '*'
	    ),$select::JOIN_LEFT);
	    $select->order("order");
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addMainMenu($mainMenuData)
	{
		$result = $this->tableGateway->insert(array(
				'name' => $mainMenuData['name'],
		        'position' => $mainMenuData['position'],
		        'lang' => $mainMenuData['lang']
		));
		if ($result) {
			return $this->tableGateway->lastInsertValue;
		} else {
			return false;
		}
	}
	
	public function editMainMenu($editMainMenuData, $mainMenuId)
	{
		if ($mainMenuId) {
			return $this->tableGateway->update(array(
					'name' => $editMainMenuData['name'],
			        'position' => $editMainMenuData['position'],
			        'lang' => $editMainMenuData['lang']
			), array('id' => $mainMenuId));
		}
		return false;
	}
	
	public function deleteMainMenu($mainMenuId)
	{
		return $this->tableGateway->delete(array(
				'id' => $mainMenuId
		));
	}
}
