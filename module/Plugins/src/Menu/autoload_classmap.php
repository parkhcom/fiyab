<?php
// Generated by ZF3's ./bin/classmap_generator.php
return array(
    'Menu\Controller\AdminController' => __DIR__ . '/Controller/AdminController.php',
    'Menu\Controller\AjaxController'  => __DIR__ . '/Controller/AjaxController.php',
    'Menu\Controller\IndexController' => __DIR__ . '/Controller/IndexController.php',
    'Menu\Form\MainMenuForm'          => __DIR__ . '/Form/MainMenuForm.php',
    'Menu\Form\MenuForm'              => __DIR__ . '/Form/MenuForm.php',
    'Menu\Model\MenuMainMenusTable'   => __DIR__ . '/Model/MenuMainMenusTable.php',
    'Menu\Model\MenuMenusTable'       => __DIR__ . '/Model/MenuMenusTable.php',
);
