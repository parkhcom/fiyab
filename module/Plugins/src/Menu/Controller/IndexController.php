<?php

namespace Menu\Controller;
   
use Zend\View\Model\ViewModel;  
use Menu\Model\MenuMainMenusTable;
use Page\Model\PagePageTable; 
use Application\Helper\GeneralHelper; 
use Application\Service\BaseController;
use Application\Model\ApplicationPagesTable;
use Menu\Model\ApplicationMenuMainMenusTable;
 
class IndexController extends BaseController 
{ 	
    public function __construct($sm)
    {
        $this->getServiceLocator = $sm;
    }
    
	public function indexAction($params = false)
	{
   
	    $view = array();
        $mainMenuPosition = $params[0]; 
        $menuMainMenusTable = new ApplicationMenuMainMenusTable($this->getServiceLocator);
        
        $mainMenuData = $menuMainMenusTable->getMainMenuView('fa', $mainMenuPosition);
        $count = 0;
        foreach ($mainMenuData as $menuId) {
            if ($menuId["parent_id"] == 0 && $menuId["active"] == 1) {
                $count++;
            }
        }
        
        if ($mainMenuData) {
            $pagePageTable = new ApplicationPagesTable($this->getServiceLocator);
            $pagesList = $pagePageTable->fetch(array("lang" => 'fa'));
            $pagesListArray = array();
            if ($pagesList) {
                foreach ($pagesList as $page) {
                    $pagesListArray[$page["id"]] =  ($page["lang"] != $GLOBALS["lang"]? "/" . $page["lang"] :"") . "/" . $page["slug"];
                }
            }
            $generalHelper = new GeneralHelper(); 
            $tree = $generalHelper->createTree($mainMenuData, true, $pagesListArray, $count);
            $view['menuId'] = $mainMenuData[0]["id"];
            $view['menu'] = $tree;
        }
        
        $viewModel = new ViewModel($view);
        $viewModel->setTemplate("menu/index/index.phtml");
        return $viewModel;
	        
	}
	
	
}
