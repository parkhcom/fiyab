<?php
namespace Menu\Controller;

use Zend\View\Model\ViewModel;
use Menu\Model\ApplicationMenuMenusTable;
use Menu\Model\ApplicationMenuMainMenusTable;
use Menu\Form\MainMenuForm;
use Zend\Session\Container;
use Language\Model\LanguageLanguagesTable;
use Menu\Form\MenuForm;
use Page\Model\PagePageTable;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use Application\Helper\Tree;
use Application\Helper\Structure;
use Application\Service\BaseAdminController;
use Zend\Mvc\MvcEvent;
use Application\Model\ApplicationPagesTable;

class AdminController extends BaseAdminController
{

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e, 'menu-admin-');
        if (! $this->userData)
            return $this->redirect()->toRoute('menu-admin'); 
        parent::onDispatch($e);
    }

    public $dataArrayMenu = array();

    public function showMenuAction()
    { 
        $config = $this->getServiceLocator()->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/menus/";
        $validationExt = "jpg,jpeg,png,gif";
        $container = new Container('token');
        $csrf = md5(time() . rand());
        $dataArrayMenu = array();
        $container->csrf = $csrf;
        $view['csrf'] = $csrf;
        $mainMenuId = $this->params('id');
        $menuMainMenusTable = new ApplicationMenuMainMenusTable($this->getServiceLocator());
        $menuMainMenusData = $menuMainMenusTable->getRecords($mainMenuId);
        if (! $menuMainMenusData) {
            return $this->redirect()->toRoute('menu-admin', array(
                'controller' => 'admin',
                'action' => 'list'
            ));
        }
        
        $applicationPagesTable = new ApplicationPagesTable($this->getServiceLocator());
        $pagesList = $applicationPagesTable->fetch(array(
            "lang" => 'fa'
        ));  
        
        $menusTable = new ApplicationMenuMenusTable($this->getServiceLocator());
        $mainMenuData = $menusTable->getMenuByMainMenuId($mainMenuId);
  
        // EXISTED MENU
        $treeClass = new Structure();
        $dataArrayMenu = $treeClass->buildTree($mainMenuData);
       
        $view['pagesList'] = $pagesList;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $allMenuArray = (array) json_decode($data['result']);
            $menusTable->activeDeactiveMenu($mainMenuId, 0);
            $this->saveMenu($allMenuArray, $mainMenuId, 0, 0);
        }
        
        $view['menuList'] = json_encode($dataArrayMenu);
        return new ViewModel($view);
    }

    public function getChildren($parentId)
    {
        $mainMenuData = array();
        $menuMenusTable = new ApplicationMenuMenusTable($this->getServiceLocator());
        if ($parentId) {
            $mainMenuDataNew = $menuMenusTable->getMainMenuById($parentId);
            
            if ($mainMenuDataNew) {
                foreach ($mainMenuDataNew as $keychild => $dataTree) {
                    $dataArrayMenu[$dataTree['id']] = $dataTree;
                    $isExistChildern = self::getChildren($dataArrayMenu[$dataTree['id']], $dataTree['id']);
                    if ($isExistChildern) {
                        $dataArrayMenu[$dataTree['id']]['children'] = $isExistChildern;
                    }
                }
            }
            return $mainMenuData;
        }
    }

    public function saveMenu($allMenuArray, $mainMenuId, $parentId, $i = 0)
    {
        $i ++;
        if ($allMenuArray) {
            $menusTable = new ApplicationMenuMenusTable($this->getServiceLocator());
            foreach ($allMenuArray as $dataMenu) {
                $dataMenu->active = 0;
                if ($dataMenu->active == 'on' || $dataMenu->active == '1') {
                    $dataMenu->active = 1;
                }
                if ($dataMenu->icon == 'empty') {
                    $dataMenu->icon = '';
                }
                $menuData = array(
                    'main_menu_id' => $mainMenuId,
                    'parent_id' => $parentId,
                    'caption' => $dataMenu->caption,
                    'internal_url' => $dataMenu->customselect,
                    'external_url' => $dataMenu->customUrl,
                    'class_name' => 'dropdown-menu',
                    'image' => 'dropdown',
                    'order' => $i,
                    'active' => $dataMenu->active,
                    'icon' => $dataMenu->icon
                );
                
                $isAdded = $menusTable->addMenu($mainMenuId, $menuData, $i);
                
                if (isset($dataMenu->children)) {
                    self::saveMenu($dataMenu->children, $mainMenuId, $isAdded, $i);
                }
                // $parentId = 0;
            }
        }
    }

    public function showMenuOldAction()
    { 
        $container = new Container('token');
        $csrf = md5(time() . rand());
        $container->csrf = $csrf;
        $view['csrf'] = $csrf;
        $mainMenuId = $this->params('id');
        $menuMainMenusTable = new ApplicationMenuMainMenusTable($this->getServiceLocator());
        $mainMenuData = $menuMainMenusTable->getMainMenuById($mainMenuId);
        
        if (! $mainMenuData) {
            return $this->redirect()->toRoute('menu-admin', array(
                'controller' => 'admin',
                'action' => 'list'
            ));
        }
        
        $applicationPagePageTable = new ApplicationPagesTable($this->getServiceLocator());
        $pagesList = $applicationPagePageTable->fetch(array(
            "lang" => $mainMenuData[0]["lang"]
        ));
        $view['pagesList'] = $pagesList;
        $menuForm = new MenuForm($mainMenuData, $pagesList);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $menusTable = new ApplicationMenuMenusTable($this->getServiceLocator());
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            if (! empty($files['image']['name'])) {
                $data = array_merge($data, $files);
            }
            
            $menuForm->setData($data);
            if ($menuForm->isValid()) {
                $menuData = $menuForm->getData();
                if ($menuData["menu_id"]) { // EditMode
                    $mernuDataOld = $menusTable->getMenu($menuData["menu_id"]);
                    
                    $isUpdated = $menusTable->editMenu($menuData["menu_id"], $menuData);
                    if ($isUpdated !== false) {
                        if (! empty($files)) {
                            $newFileName = md5(time() . $menuData['image']['name']);
                            $isUploaded = $this->imageUploader()->UploadFile($menuData['image'], $validationExt, $uploadDir, $newFileName);
                            $menuData["image"] = $isUploaded[1];
                        }
                        if (isset($mernuDataOld["parent_id"]) && $mernuDataOld["parent_id"] == 0) {
                            if ($menuData["parent_id"] !== 0) {
                                $menuData["class_name_ul"] = "dropdown-menu";
                                $menuData["class_name_li"] = "dropdown";
                                $menusTable->editUlLi($menuData["parent_id"], $menuData);
                            }
                        } else {
                            if ($menuData["parent_id"] !== 0) {
                                $menuData["class_name_ul"] = "dropdown-menu";
                                $menuData["class_name_li"] = "dropdown";
                                $menusTable->editUlLi($menuData["parent_id"], $menuData);
                            } else {
                                $menuData["class_name_ul"] = "";
                                $menuData["class_name_li"] = "";
                                $menusTable->editUlLi($menuData["parent_id"], $menuData);
                            }
                        }
                        $mainMenuData = $menuMainMenusTable->getMainMenuById($mainMenuId);
                        
                        $this->flashMessenger()->addSuccessMessage(t("Operation done successfully."));
                    } else {
                        $this->layout()->errorMessage = t("Operation failed!");
                    }
                } else {
                    // InsertMode
                    if (! empty($files)) {
                        $newFileName = md5(time() . $menuData['image']['name']);
                        $isUploaded = $this->imageUploader()->UploadFile($menuData['image'], $validationExt, $uploadDir, $newFileName);
                        $menuData["image"] = $isUploaded[1];
                    }
                    
                    $isAdded = $menusTable->addMenu($mainMenuId, $menuData);
                    if ($isAdded) {
                        if ($menuData["parent_id"] !== 0) {
                            $menuData["class_name_ul"] = "dropdown-menu";
                            $menuData["class_name_li"] = "dropdown";
                            $menusTable->editUlLi($menuData["parent_id"], $menuData);
                        }
                        $mainMenuData = $menuMainMenusTable->getMainMenuById($mainMenuId);
                        $this->flashMessenger()->addSuccessMessage(t("Operation done successfully."));
                        return $this->redirect()->toRoute('menu-admin', array(
                            'controller' => 'admin',
                            'action' => 'show-menu',
                            'id' => $mainMenuId
                        ));
                    } else {
                        $this->layout()->errorMessage = t("Operation failed!");
                    }
                }
            }
        }
        if ($mainMenuData) {
            $pagesList = $applicationPagePageTable->feach(array(
                "lang" => $mainMenuData[0]["lang"]
            ));
            
            $pagesListArray = array();
            foreach ($pagesList as $page) {
                $pagesListArray[$page["id"]] = "/" . $page["lang"] . "/" . str_replace(" ", "-", $page["title"]);
            }
            
            $tree = $this->generalHelper()->createTree($mainMenuData, false, $pagesListArray, $this->lang);
            $view['menu'] = $mainMenuData[0];
          //  $this->setHeadTitle($mainMenuData[0]["name"]);
            $view['menuTree'] = $tree;
        }
        
        $view['menuForm'] = $menuForm;
        return new ViewModel($view);
    }

    public function listMainMenusAction()
    {
      //  $this->setHeadTitle(t('Main menus list'));
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        $this->lang = 'fa';
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $menuMainMenusTable = new ApplicationMenuMainMenusTable($this->getServiceLocator());
        $mainMenus = $menuMainMenusTable->fetchAllMainMenus();
        $grid->setTitle(t('Main menu list'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($mainMenus, $dbAdapter);
        
        $col = new Column\Select('id', "application_menus");
        $col->setLabel(t('Row'));
        $col->setIdentity();
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(t('Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('position');
        $col->setLabel(t('Position'));
        $grid->addColumn($col);
        
        $col = new Column\Select('lang');
        $col->setLabel(t('Lang'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(t('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-plus');
        $viewAction->setLink("/" . $this->lang . '/menu-admin/show-menu/' . $rowId);
        // $viewAction->setTooltipTitle(t('Add items'));
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/menu-admin/edit-main-menu/' . $rowId);
        $viewAction1->setTooltipTitle(t('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/menu-admin/delete-main-menu/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(t('Delete'));
        
        $link[] = '<a href=' . "/" . $this->lang . '/menu-admin/add-main-menu class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . t("Add") . '</a>';
        $link[] = '<a tablename="menu_menus"  href="/application-ajax-check-parent"  class="btn btn-danger delete_all"><i class="fa fa-times" aria-hidden="true"></i>' . t("Delete All") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . t("Help") . '</a>';
        $grid->setLink($link);
        
        $actions2 = new Column\Action();
        $actions2->setLabel(t('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addMainMenuAction()
    {
       // $this->setHeadTitle(t('Add new main menu'));
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        $mainMenuForm = new MainMenuForm($allActiveLanguages);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $mainMenuForm->setData($data);
            if ($mainMenuForm->isValid()) {
                $mainMenusData = $mainMenuForm->getData();
                $menuMainMenusTable = new ApplicationMenuMainMenusTable($this->getServiceLocator());
                $isAdded = $menuMainMenusTable->addMainMenu($mainMenusData);
                if ($isAdded) {
                    
                    if (isset($_POST['submit'])) {
                        $this->flashMessenger()->addSuccessMessage(t("Operation done successfully."));
                        return $this->redirect()->toRoute('menu-admin', array(
                            'controller' => 'admin',
                            'action' => 'list-main-menus'
                        ));
                    } else {
                        $this->flashMessenger()->addSuccessMessage(t("Operation done successfully."));
                        return $this->redirect()->toRoute('menu-admin', array(
                            'controller' => 'admin',
                            'action' => 'add-main-menu'
                        ));
                    }
                } else {
                    $this->layout()->errorMessage = t("Operation failed!");
                }
            }
        }
        $view['mainMenuForm'] = $mainMenuForm;
        return new ViewModel($view);
    }

    public function editMainMenuAction()
    {
      //  $this->setHeadTitle(t('Edit main menu'));
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        $mainMenuForm = new MainMenuForm($allActiveLanguages);
        $mainMenuId = $this->params('id', - 1);
        $menuMainMenusTable = new ApplicationMenuMainMenusTable($this->getServiceLocator());
        $mainMenusData = $menuMainMenusTable->getMainMenuById($mainMenuId);
        if (! $mainMenusData) {
            return $this->redirect()->toRoute('menu-admin', array(
                'controller' => 'admin',
                'action' => 'list-main-menus'
            ));
        }
        $mainMenuForm->populateValues($mainMenusData[0]);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $mainMenuForm->setData($data);
            if ($mainMenuForm->isValid()) {
                $mainMenuData = $mainMenuForm->getData();
                $isUpdated = $menuMainMenusTable->editMainMenu($mainMenuData, $mainMenuId);
                if ($isUpdated !== false) {
                    $this->flashMessenger()->addSuccessMessage(t("Operation done successfully."));
                    return $this->redirect()->toRoute('menu-admin', array(
                        'controller' => 'admin',
                        'action' => 'list-main-menus'
                    ));
                } else {
                    $this->layout()->errorMessage = t("Operation failed!");
                }
            }
        }
        $view['mainMenuForm'] = $mainMenuForm;
        return new ViewModel($view);
    }

    public function deleteMainMenuAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $mainMenuId = $this->params('id');
        $menuMainMenusTable = new ApplicationMenuMainMenusTable($this->getServiceLocator());
        $mainMenuData = $menuMainMenusTable->getMainMenuById($mainMenuId);
        if ($container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $isDeleted = $menuMainMenusTable->deleteMainMenu($mainMenuId);
            if ($isDeleted) {
                $this->flashMessenger()->addSuccessMessage(t("Operation done successfully."));
            } else {
                $this->flashMessenger()->addErrorMessage(t("Operation failed!"));
            }
        }
        return $this->redirect()->toRoute('menu-admin');
    }
}
