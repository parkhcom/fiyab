<?php
namespace Block\Controller;

use Zend\View\Model\ViewModel;
use Block\Form\BlockForm;
use Zend\Session\Container;
use Block\Model\ApplicationBlockTable;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use Application\Service\BaseAdminController;
use Zend\Mvc\MvcEvent;
use Application\Helper\GeneralHelper;
use Language\Model\ApplicationLanguageLanguagesTable;
use Application\Helper\ImageUploader;

class AdminController extends BaseAdminController
{

    public function onDispatch(MvcEvent $e)
    {
        $this->initController($e, 'block-admin-');
        if (! $this->userData)
            return $this->redirect()->toRoute('base');
        
        parent::onDispatch($e);
    }

    public function listAction()
    {
        $this->lang = 'fa';
        
        $csrf = $this->tools->token();
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $blockTable = new ApplicationBlockTable($this->getServiceLocator());
        $groupId = $this->userData->user_group_id;
        $blocks = $blockTable->getAllBlock($groupId);
        $grid->setTitle(t('Blok List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($blocks, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(t('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $col = new Column\Select('block_position');
        $col->setLabel(t('Position'));
        $grid->addColumn($col);
        
        $col = new Column\Select('block_lang');
        $col->setLabel(t('Language'));
        $options = array(
            'fa' => 'fa',
            'en' => 'en'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            'fa' => 'fa',
            'en' => 'en'
        );
        $grid->addColumn($col);
        
        $col = new Column\Select('block_content');
        $col->setLabel(t('Content'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(t('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink('/block-admin/edit/id/' . $rowId);
        $viewAction->setTooltipTitle(t('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink('/block-admin/delete/id/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(t('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(t('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $grid->getRenderer()->setTemplate('block/admin/list');
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addAction()
    {
        $generalHelper = new GeneralHelper();
        $layouts = $generalHelper->getFilesFromDir(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "View" . DIRECTORY_SEPARATOR . "block" . DIRECTORY_SEPARATOR . "index");
        $languageLanguageLanguagesTable = new ApplicationLanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        $groupId = $this->userData->user_group_id;
        $blockForm = new BlockForm();
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            $blockForm->setData($data);
            if ($blockForm->isValid()) {
                $validData = $blockForm->getData();
                $isPositionExist = $blockTable->getBlockType($validData['block_position']);
                if ($isPositionExist) {
                    $this->flashMessenger()->addSuccessMessage(t("Duplicated Data try Another One"));
                }
                if (! $isPositionExist) {
                    $image = '';
                    if (! empty($files)) {
                        $config = $this->getServiceLocator()->get('config');
                        $uploadDir = $config['base_route'] . "/uploads/block/";
                        $validationExt = "jpg,jpeg,png,gif";
                        $uploadImages = new ImageUploader();
                        $isUploaded = $uploadImages->UploadFile($files, $validationExt, $uploadDir, 'block_image');
                  
                        if (isset($isUploaded[0])) {
                            $isUploaded = $isUploaded[0];
                            $image = $isUploaded->getFileName();
                        }
                    }
                    $item = array(
                        'block_lang' => 'fa',
                        'block_image' => $image,
                        'block_content' => $validData['block_content'],
                        'block_position' => $validData['block_position'],
                        'block_kind' => 'simple'
                    );
                    $blockTable = new ApplicationBlockTable($this->getServiceLocator());
                    
                    $isAdded = $blockTable->addBlock($item);
                    if ($isAdded) {
                        if (isset($_POST['submit'])) {
                            $this->flashMessenger()->addSuccessMessage(t("Operation done successfully."));
                            return $this->redirect()->toRoute('block-admin');
                        } else {
                            $this->flashMessenger()->addSuccessMessage(t("Operation done successfully."));
                            return $this->redirect()->toRoute('block-admin', array(
                                'action' => 'add'
                            ));
                        }
                    } else {
                        
                        $this->flashMessenger()->addSuccessMessage(t("Operation failed!"));
                    }
                } else { 
                    $result = $blockForm->getMessages();
                    foreach ($result as $dataError) {
                        foreach ($dataError as $key => $data) {
                            $this->flashMessenger()->addErrorMessage(t($data));
                        }
                    }
                }
            } 
        }
        $view['blockForm'] = $blockForm;
        return new ViewModel($view);
    }

    public function editAction()
    {
        $generalHelper = new GeneralHelper();
        $layouts = $generalHelper->getFilesFromDir(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "View" . DIRECTORY_SEPARATOR . "block" . DIRECTORY_SEPARATOR . "index");
        $languageLanguageLanguagesTable = new ApplicationLanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        $groupId = $this->userData->user_group_id;
        $blockForm = new BlockForm($allActiveLanguages, $layouts, $groupId);
        $id = $this->params('id', - 1);
        $blockTable = new ApplicationBlockTable($this->getServiceLocator());
        $blockData = $blockTable->getBlockById($id);
        
        if (! $blockData) {
            $this->flashMessenger()->addErrorMessage(t('No Such Field Exist'));
            return $this->redirect()->toRoute('block-admin', [
                'controller' => 'block',
                'action' => 'list'
            ]);
        }
        $view['image'] = $blockData['block_image'];
        $blockForm->populateValues($blockData);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            $blockForm->setData($data);
            if ($blockForm->isValid()) {
                $validData = $blockForm->getData();
                
                $isPositionExist = $blockTable->getBlockType($validData['block_position'], $id);
                if ($isPositionExist) {
                    $this->flashMessenger()->addSuccessMessage(t("Duplicated Data try Another One"));
                }
                if (! $isPositionExist && $id > 0 && is_numeric($id)) {
                    $image = $blockData['block_image'];
                    if (! empty($files)) {
                        $config = $this->getServiceLocator()->get('config');
                        $uploadDir = $config['base_route'] . "/uploads/block/";
                        $validationExt = "jpg,jpeg,png,gif";
                        $uploadImages = new ImageUploader();
                        $isUploaded = $uploadImages->UploadFile($files, $validationExt, $uploadDir, 'block_image');
                       
                        if (isset($isUploaded[0])) {
                            $isUploaded = $isUploaded[0];
                            $image = $isUploaded->getFileName();
                            @unlink($uploadDir.$blockData['block_image']);
                        }
                    }
                 
                    $item = array(
                        'block_lang' => 'fa',
                        'block_image' => $image,
                        'block_content' => $validData['block_content'],
                        'block_position' => $validData['block_position'],
                        'block_kind' => 'simple'
                    );
                    $isEditted = $blockTable->editBlock($item, $id);
                    if ($isEditted) {
                        $this->flashMessenger()->addSuccessMessage(t("Operation done successfully."));
                        return $this->redirect()->toRoute('block-admin');
                    } else {
                        $this->flashMessenger()->addErrorMessage(t("No changes happend!"));
                        return $this->redirect()->toRoute('block-admin');
                    }
                }
            } else {
                $result = $blockForm->getMessages();
          
                foreach ($result as $dataError) {
                    foreach ($dataError as $key => $data) {
                        $this->flashMessenger()->addErrorMessage(t($data));
                    }
                } 
            }
        }
        
        $view['blockForm'] = $blockForm;
        
        return new ViewModel($view);
    }

    public function deleteAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $id = $this->params('id');
        $blockTable = new ApplicationBlockTable($this->getServiceLocator());
        $blockById = $blockTable->getBlockById($id);
        if ($this->tools->token('check', $this->params('token'))) {
            $isDeleted = $blockTable->deleteBlock($id); 
            if ($isDeleted) { 
                @unlink($uploadDir.$blockById['block_image']);
                $this->flashMessenger()->addSuccessMessage(t("Operation done successfully."));
            } else {
                $this->flashMessenger()->addErrorMessage(t("Operation failed!"));
            }
        }
        
        return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
    }
}
