<?php
namespace Block\Controller;
   
use Zend\View\Model\ViewModel;  
use Block\Model\BlockTable;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet; 
use Application\Service\BaseController;
use Block\Model\ApplicationBlockTable;

class IndexController  extends BaseController 
{  
	
	public function __construct($sm)
	{
	    $this->getServiceLocator = $sm; 
	}
	
	public function indexAction($params = false)
	{    
        $blockTable = new ApplicationBlockTable($this->getServiceLocator);
        $lang = 'fa';
        $blockType = $blockTable->getBlockType($params['position']);
        $view = array();
 
        if ($blockType ) {
           
            $view["blockData"] = $blockType;
            $view["blockContent"] = $blockType["block_content"];
         
        }  
     
        $viewModel = new ViewModel($view);
        $viewModel->setTemplate("block/index/index.phtml");
    
       
        return $viewModel;
	        	
	
	}
}
