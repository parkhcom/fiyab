<?php
namespace Block\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;

class ApplicationBlockTable
{

    public $tableGateway;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $dbAdapter = $this->serviceManager->get(AdapterInterface::class);
        $this->tableGateway = new TableGateway('application_block', $dbAdapter);
    }

    public function getRecords($id = false)
    {
        return $this->tableGateway->select()->toArray();
    }

    public function getAllBlock($groupId)
    {
        $select = new Select('application_block');
        $select->order('block_lang', 'id');
        if ($groupId != 1) {
            $select->where(array(
                'block.block_type' => 'Simple'
            ));
        }
        return $select;
    }

    public function getBlockByPostion($lang, $blockPosition)
    {
        $select = new Select('application_block');
        $select->where->equalTo('block_lang', $lang);
        $select->where->equalTo('block_position', $blockPosition);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getBlockByPostionEntity($blockPosition)
    {
        $select = new Select('application_block');
        $select->where->equalTo('block_position', $blockPosition);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getBlockType($blockPosition, $id = false)
    {
        $select = new Select('application_block');
        $select->where->equalTo('block_position', $blockPosition);
        if ($id) {
            $select->where->notEqualTo('id', $id);
        }
        return $this->tableGateway->selectWith($select)->current();
    }

    public function getBlockById($blockId)
    {
        $select = new Select('application_block');
        $select->where->equalTo('id', $blockId);
        return $this->tableGateway->selectWith($select)->current();
    }

    public function addBlock($data)
    {
        return $this->tableGateway->insert(array(
            'block_lang' => $data['block_lang'],
            'block_image' => $data['block_image'],
            'block_content' => $data['block_content'],
            'block_position' => $data['block_position'],
            'block_kind' => $data['block_kind']
        ));
    }

    public function editBlock($data, $blockId)
    {
        return $this->tableGateway->update(array(
            'block_lang' => $data['block_lang'],
            'block_image' => $data['block_image'],
            'block_content' => $data['block_content'],
            'block_position' => $data['block_position'],
            'block_kind' => $data['block_kind']
        ), array(
            'id' => $blockId
        ));
    }

    public function deleteBlock($blockId)
    {
        return $this->tableGateway->delete(array(
            'id' => $blockId
        ));
    }
}
