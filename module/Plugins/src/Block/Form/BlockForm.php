<?php
namespace Block\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class BlockForm extends Form
{

    public function __construct( $groupId = false)
    {
        parent::__construct('block_form');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'id' => 'form',
            'novalidate' => true
        ));
         
        $image = new Element\File('block_image');
        $image->setAttributes(array(
            'id' => 'fileUpload'
        ));
        $image->setLabel(t("Choose image"));
        
        $blockPosition = new Element\Text('block_position');
        $blockPosition->setAttributes(array(
            'class' => 'form-control  validate[required]',
            'required' => 'required',
            'placeholder' => t("Position")
        ));
        $blockPosition->setLabel(t("Position"));
        
        $blockContent = new Element\Textarea('block_content');
        $blockContent->setAttributes(array(
            'class' => 'form-control  ',
            'id' => 'block-content',
            'placeholder' => t("Content") 
        ));
        $blockContent->setLabel(t("Content"));
        
        $csrf = new Element\Csrf('csrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(t("Save and Close"));
        $submit->setAttributes(array(
            'class' => 'btn btn-info submitBtn',
            'id' => 'submit'
        ));
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(t("Save and New"));
        $submit2->setAttributes(array(
            'class' => 'btn btn-success submitBtn',
            'id' => 'submit'
        ));
        
        $this->add($blockPosition)
            ->add($blockContent) 
            ->add($image)   
            ->add($csrf)
            ->add($submit2)
            ->add($submit);
        $this->inputFilter();
    }

    public function inputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        
        $inputFilter->add($factory->createInput(array(
            'name' => 'block_position',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
