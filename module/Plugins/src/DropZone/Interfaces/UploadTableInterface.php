<?php
namespace DropZone\Interfaces;

use Application\Helper\BaseController;
use LikeButton\Validator\PostData;

interface UploadTableInterface
{  
	 /**
	  * Get array of data with given parameters which send by data table ajax.
	  * @param PostData $postData
	  */
	 public function addData($postData);
	 
	 public function deleteData($where);

	 public function getData($where);
	 /**
	  * Set options and columns for datatable
	  */
	
	 
}