<?php
namespace Block; 

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Helper\Tools;
$tools = new Tools();

return [
    'router' => [
        'routes' => [
            'block-home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/plugins/block[/:controller][/:action][/:id]',
                    'constraints' =>[ 
                        'id' => '[1-9][0-9]*'
                    ],
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'default'
                    ]
                ]
            ],
            'block-admin' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '[/:lang]/block-admin[/:action][/id/:id][/token/:token]',
                    'constraints' => [
                        'lang' => '[a-zA-Z]{2}',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ],
                    
                    'defaults' => [
                        'controller' => Controller\AdminController::class,
                        'action'        => 'list'
                        //TODO
                        // temporarily disabled to make dynamic breadcrumb working
                        // this may cause other urls no to work properly
                        // 'lang' => 'en'
                    ]
                ]
            ],
            
            'block-admin-ajax' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '[/:lang]/admin-block-ajax[/:action][/:id][/token/:token]',
                    'constraints' => [
                        'lang' => '[a-zA-Z]{2}',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ],
                    'defaults' => [
                        '__NAMESPACE__' => 'Block\Controller',
                        'controller'    => 'Ajax',
                        'action'        => 'delete-block'
                    ],
                ]
            ],
        ]
    ],
    
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\AdminController::class => InvokableFactory::class,
        ],
        'aliases' => [
            'admin' => Controller\AdminController::class,
            'index' => Controller\IndexController::class,
        ]
    ],
    
    
    'template_path_stack' => [
        'block' => __DIR__ . '/../view',
    ]
];
