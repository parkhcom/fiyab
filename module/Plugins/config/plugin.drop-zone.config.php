<?php
namespace DropZone;
 
use Zend\Router\Http\Segment; 
use Application\Helper\Tools;
use Zend\ServiceManager\Factory\InvokableFactory;
$tools = new Tools();
/**
 * Zend Framework (http://framework.zend.com/]
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c] 2005-2014 Zend Technologies USA Inc. (http://www.zend.com]
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return [
    'router' => [
        'routes' => [
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /plugins/:plugin/:controller/:action
            'drop-zone-home' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/plugins/drop-zone[/:controller][/:action][/configName/:configName][/extraColumnsValue/:extraColumnsValue]',
                    'constraints' => [
                        'configName' => '[0-9a-f]{32}',
                        'extraColumnsValue' => '.*'
                    ],
                    'defaults' => [
                        'controller' => Controller\IndexController::class, 
                        'action' => 'index'
                    ]
                ]
            ],
            'drop-zone-ajax' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/plugins/drop-zone/ajax[/:action][/configName/:configName][/extraColumnsValue/:extraColumnsValue]',
                    'constraints' => [
                        'configName' => '[0-9a-f]{32}',
                        'extraColumnsValue' => '.*'
                    ],
                    'defaults' => [
                        'controller' => Controller\AjaxController::class, 
                        'action' => 'index'
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\AjaxController::class => InvokableFactory::class,
        ],
        'aliases' => [
            'admin' => Controller\AjaxController::class,
            'index' => Controller\IndexController::class,
        ]
    ],
    
    
    'template_path_stack' => [
        'drop-zone' => __DIR__ . '/../view',
    ],
    'drop-zone-configs' => [ 
        'config_1' => [
            "dbTable" => "DropZone\\Model\\ApplicationProductFilesTable",
            "extraColumns" => [ 
                "product_id",
                "user_id",
            ],
            "format" => "Any",
            "uploadPath" => $_SERVER['DOCUMENT_ROOT'] . "/uploads/product/",
            "minSize" => 1,
            "maxSize" => 1024000,
            "canDeleteUploadedFile" => "true",
            "canSpecialUploadedFile" => "true",
            "extensions" => [
                '*',
            ],
            "front_acceptedFiles" => "",
            "front_maxFilesize" => 3024000, // mb
            "front_url" => "/plugins/drop-zone/ajax/index",
            "front_deleteUrl" => "/plugins/drop-zone/ajax/delete",
            "front_specialUrl" => "/plugins/drop-zone/ajax/special",
            "uploadPathThumb" => "/uploads/product/",
            "front_maxFiles" => 1000,
            "front_clickable" => "true"
        ],
         
    ],
 
  
  
];
