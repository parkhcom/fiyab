<?php
namespace Menu; 

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Helper\Tools;
$tools = new Tools();

return [
    'router' => [
        'routes' => [
            'menu-home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/plugins/menu[/:controller][/:action][/:id]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'menu-admin' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '[/:lang]/menu-admin[/:action][/:id][/token/:token]',
                    'constraints' => [
                        'lang' => '[a-zA-Z]{2}',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}' 
                    ],
                    
                    'defaults' => [
                        'controller' => Controller\AdminController::class,
                        'action'        => 'list-main-menus'
                        //TODO
                        // temporarily disabled to make dynamic breadcrumb working
                        // this may cause other urls no to work properly
                        // 'lang' => 'en'
                    ]
                ]
            ],
     
            'menu-admin-ajax' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '[/:lang]/admin-menu-ajax[/:action][/:id][/token/:token]',
                    'constraints' => [
                        'lang' => '[a-zA-Z]{2}',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ],
                    'defaults' => [
                        '__NAMESPACE__' => 'Menu\Controller',
                        'controller'    => 'Ajax',
                        'action'        => 'delete-menu'
                    ],
                ]
            ],
        ]
    ],
    
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\AdminController::class => InvokableFactory::class,
        ],
        'aliases' => [
            'admin' => Controller\AdminController::class,
            'index' => Controller\IndexController::class,
        ]
    ],
    
    
    'view_manager' => [
        
        // It automatically finds the view file but a little bit slower
        'template_path_stack' => [
            'menu' => __DIR__ . '/../view',
        ]
    ]
];
