/*
Navicat MySQL Data Transfer

Source Server         : localhost-php7
Source Server Version : 100131
Source Host           : localhost:3306
Source Database       : patrino

Target Server Type    : MYSQL
Target Server Version : 100131
File Encoding         : 65001

Date: 2018-06-21 13:16:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for application_attributes
-- ----------------------------
DROP TABLE IF EXISTS `application_attributes`;
CREATE TABLE `application_attributes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_set_id` bigint(20) DEFAULT NULL,
  `is_parent` tinyint(4) NOT NULL,
  `parent_id` bigint(20) unsigned NOT NULL,
  `shop_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL COMMENT '0 = ØºÙŠØ±ÙØ¹Ø§Ù„ | 1= ÙØ¹Ø§Ù„',
  `type` varchar(10) NOT NULL,
  `regex` varchar(50) NOT NULL,
  `latin_name` varchar(255) DEFAULT NULL,
  `assign_to` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_set_id` (`attribute_set_id`,`parent_id`,`name`),
  KEY `attribute_set_id_2` (`attribute_set_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of application_attributes
-- ----------------------------
INSERT INTO `application_attributes` VALUES ('12', '1', '1', '0', '1', 'ویژگی کلی اجناس', '1', 'text', '', 'Attribute Goods', 'all');
INSERT INTO `application_attributes` VALUES ('13', '1', '0', '12', '1', 'وزن', '1', 'text', '', 'weight', 'all');
INSERT INTO `application_attributes` VALUES ('14', '2', '0', '15', '1', 'رنگ مانیتور1', '1', 'select', '', 'Color monitor', '1');
INSERT INTO `application_attributes` VALUES ('15', '2', '1', '0', '1', 'ویژگی مانیتور های رنگی', '1', 'text', '', 'Color monitor features', 'all');
INSERT INTO `application_attributes` VALUES ('16', '2', '0', '15', '1', 'صفحه نمایشگر', '1', 'text', '', 'Screen', 'all');
INSERT INTO `application_attributes` VALUES ('17', '2', '0', '15', '1', 'دقت تصویر ', '1', 'text', '', 'Picture precision', 'all');
INSERT INTO `application_attributes` VALUES ('18', '2', '0', '15', '1', 'برق ورودی', '1', 'text', '', 'Input Power', 'all');
INSERT INTO `application_attributes` VALUES ('19', '2', '0', '15', '1', 'برق مصرفی', '1', 'text', '', 'Power consumed', 'all');
INSERT INTO `application_attributes` VALUES ('20', '2', '0', '15', '1', 'سیگنال ورودی', '1', 'text', '', 'Input signal', 'all');
INSERT INTO `application_attributes` VALUES ('21', '2', '0', '15', '1', 'درجه حرارت کارکرد', '1', 'text', '', 'Operating temperature', 'all');
INSERT INTO `application_attributes` VALUES ('22', '2', '0', '15', '1', 'حافظه داخلی', '1', 'text', '', 'internal memory', 'all');
INSERT INTO `application_attributes` VALUES ('23', '2', '0', '15', '1', 'حافظه خارجی', '1', 'text', '', 'External memory', 'all');
INSERT INTO `application_attributes` VALUES ('24', '2', '0', '15', '1', 'حد اکثر فاصله بین صفحه و مانیتور', '1', 'text', '', 'The maximum distance between the screen and the monitor', 'all');
INSERT INTO `application_attributes` VALUES ('25', '2', '0', '15', '1', 'کنترل', '1', 'text', '', 'Control', 'all');
INSERT INTO `application_attributes` VALUES ('26', '2', '0', '15', '1', 'ابعاد (میلیمتر)', '1', 'text', '', 'Dimensions (mm)', 'all');
INSERT INTO `application_attributes` VALUES ('27', '3', '1', '0', '1', 'ویژگی پنل های رنگی', '1', 'text', '', 'Color panel featurePage sex', 'all');
INSERT INTO `application_attributes` VALUES ('28', '3', '0', '27', '1', 'جنس صفحه', '1', 'text', '', 'Page sex', 'all');
INSERT INTO `application_attributes` VALUES ('29', '3', '0', '27', '1', 'قابلیت جدا سازی تصویر', '1', 'text', '', 'Image separability', 'all');
INSERT INTO `application_attributes` VALUES ('30', '3', '0', '27', '1', 'روشنایی جهت تشخیص اسمی واحدها', '1', 'select', '', 'Brightness for nominating units', 'all');
INSERT INTO `application_attributes` VALUES ('31', '3', '0', '27', '1', 'دوربین', '1', 'text', '', 'Camera', 'all');
INSERT INTO `application_attributes` VALUES ('32', '3', '0', '27', '1', 'قابلیت پخش صدای زنگ همزمان با داخل ساختمان', '1', 'select', '', 'The ability to play ringtones simultaneously with the interior', 'all');
INSERT INTO `application_attributes` VALUES ('33', '3', '0', '27', '1', 'حداقل روشنایی', '1', 'text', '', 'Minimum brightness', 'all');
INSERT INTO `application_attributes` VALUES ('34', '3', '0', '27', '1', 'سیستم رنگ و مرور تصویر', '1', 'text', '', 'Color system and image browsing', 'all');
INSERT INTO `application_attributes` VALUES ('35', '3', '0', '27', '1', 'خروجی تصویر', '1', 'text', '', 'Image output', 'all');
INSERT INTO `application_attributes` VALUES ('36', '3', '0', '27', '1', 'نسبت سیگنال به نویز', '1', 'text', '', 'Signal to Noise Ratio', 'all');
INSERT INTO `application_attributes` VALUES ('37', '3', '0', '27', '1', 'زاویه دید دوربین', '1', 'text', '', 'Viewing angle of the camera', 'all');
INSERT INTO `application_attributes` VALUES ('38', '3', '0', '27', '1', 'چرخش دوربین', '1', 'text', '', 'Rotate the camera', 'all');
INSERT INTO `application_attributes` VALUES ('39', '2', '0', '15', '1', 'ارتباط داخلی', '1', 'text', '', 'Intercom', 'all');
INSERT INTO `application_attributes` VALUES ('40', '2', '0', '15', '1', 'اتصال به خط تلفن', '1', 'text', '', 'Connect to phone line', 'all');
INSERT INTO `application_attributes` VALUES ('41', '2', '0', '15', '1', 'انتقال مکالمه', '1', 'text', '', 'Call transfer', 'all');
INSERT INTO `application_attributes` VALUES ('42', '2', '0', '15', '1', 'ضبط هوشمندانه', '1', 'text', '', 'Intelligent recording', 'all');
INSERT INTO `application_attributes` VALUES ('43', '2', '0', '15', '1', 'نشان دهنده درجه حرارت محیط', '1', 'text', '', 'Thermometer', 'all');
INSERT INTO `application_attributes` VALUES ('44', '4', '1', '0', '1', 'ویژگی ترانس آیفون', '1', 'text', '', 'Trans iPhone Video Feature', 'all');

-- ----------------------------
-- Table structure for application_attributes_option
-- ----------------------------
DROP TABLE IF EXISTS `application_attributes_option`;
CREATE TABLE `application_attributes_option` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` bigint(20) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `image` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of application_attributes_option
-- ----------------------------
INSERT INTO `application_attributes_option` VALUES ('90', '17', '1', 'دارد', '');
INSERT INTO `application_attributes_option` VALUES ('91', '17', '0', 'ندارد', '');
INSERT INTO `application_attributes_option` VALUES ('96', '18', '1', 'دارد', '');
INSERT INTO `application_attributes_option` VALUES ('97', '18', '0', 'ندارد', '');
INSERT INTO `application_attributes_option` VALUES ('98', '30', '0', 'دارد', '');
INSERT INTO `application_attributes_option` VALUES ('99', '30', '1', 'ندارد', '');
INSERT INTO `application_attributes_option` VALUES ('100', '32', '0', 'دارد', '');
INSERT INTO `application_attributes_option` VALUES ('101', '32', '1', 'ندارد', '');
INSERT INTO `application_attributes_option` VALUES ('111', '14', '1', 'سفید', '');
INSERT INTO `application_attributes_option` VALUES ('112', '14', '2', 'مشکی', '');
INSERT INTO `application_attributes_option` VALUES ('113', '14', '3', 'نوک مدادی', '');
INSERT INTO `application_attributes_option` VALUES ('114', '14', '4', 'نقره ای', '');

-- ----------------------------
-- Table structure for application_attributes_set
-- ----------------------------
DROP TABLE IF EXISTS `application_attributes_set`;
CREATE TABLE `application_attributes_set` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Set Id',
  `status` int(2) DEFAULT '1',
  `attribute_set_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Set Name',
  `is_general_set` int(1) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Eav Attribute Set';

-- ----------------------------
-- Records of application_attributes_set
-- ----------------------------

-- ----------------------------
-- Table structure for application_block
-- ----------------------------
DROP TABLE IF EXISTS `application_block`;
CREATE TABLE `application_block` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `block_lang` varchar(2) DEFAULT NULL,
  `block_content` text,
  `block_position` varchar(255) DEFAULT NULL,
  `layout` varchar(50) NOT NULL,
  `block_kind` varchar(50) DEFAULT NULL,
  `order_by` varchar(50) DEFAULT NULL,
  `block_type` varchar(50) NOT NULL,
  `block_limit` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_block
-- ----------------------------

-- ----------------------------
-- Table structure for application_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `application_group_permissions`;
CREATE TABLE `application_group_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` varchar(1000) DEFAULT NULL,
  `permission_id` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_group_permissions
-- ----------------------------
INSERT INTO `application_group_permissions` VALUES ('1', 'application-admin-dashboard', 'dashboard');
INSERT INTO `application_group_permissions` VALUES ('2', 'application-permission-update', 'update permission');
INSERT INTO `application_group_permissions` VALUES ('3', 'application-permission-add', 'add permission');

-- ----------------------------
-- Table structure for application_menus
-- ----------------------------
DROP TABLE IF EXISTS `application_menus`;
CREATE TABLE `application_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT 'fa',
  `position` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_menus
-- ----------------------------
INSERT INTO `application_menus` VALUES ('1', 'منوی صفحه اصلی', 'fa', 'mainindex');
INSERT INTO `application_menus` VALUES ('2', 'test menu', 'fa', 'test');

-- ----------------------------
-- Table structure for application_pages
-- ----------------------------
DROP TABLE IF EXISTS `application_pages`;
CREATE TABLE `application_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `lang` varchar(2) NOT NULL DEFAULT 'fa',
  `slug` varchar(255) NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gallery` int(255) unsigned DEFAULT NULL,
  `video_gallery` int(10) unsigned DEFAULT NULL,
  `slideshow` int(255) unsigned DEFAULT NULL,
  `user_ip` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `page_title` (`title`) USING BTREE,
  KEY `page_slug` (`slug`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_pages
-- ----------------------------
INSERT INTO `application_pages` VALUES ('31', '1', '', 'گالری تصاویر', '1', 'fa', 'گالری-تصاویر', 'ادمین سی ام اس شبدیز', '', '', '2016-06-08 16:37:46', '2016-07-27 14:30:20', '0', null, null, null);
INSERT INTO `application_pages` VALUES ('34', '0', '<p>عکس های مهمانی 95</p>', 'gallery', '1', 'fa', 'gallery', null, '', '', '2016-12-22 11:59:34', '2017-03-16 11:14:20', '23', '0', '0', null);
INSERT INTO `application_pages` VALUES ('35', '0', '<p><a href=\"/\" target=\"_blank\">ویدئو مهمانی </a></p>\r\n<p>95</p>\r\n<p> </p>\r\n<p><a href=\"/\" target=\"_blank\"> <strong>مهمانی دوم</strong></a></p>\r\n<table style=\"height: 115px;\" width=\"410\">\r\n<tbody>\r\n<tr>\r\n<td><img src=\"/source/pages/%3Cbr%20/%3E%0A%3Cfont%20size%3D\'1\'%3E%3Ctable%20class%3D\'xdebug-error%20xe-warning\'%20dir%3D\'ltr\'%20border%3D\'1\'%20cellspacing%3D\'0\'%20cellpadding%3D\'1\'%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23f57900\'%20colspan%3D%225%22%3E%3Cspan%20style%3D\'background-color%3A%20%23cc0000%3B%20color%3A%20%23fce94f%3B%20font-size%3A%20x-large%3B\'%3E(%20!%20)%3C/span%3E%20Warning%3A%20finfo_file()%3A%20Empty%20filename%20or%20path%20in%20C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cinclude%5Cmime_type_lib.php%20on%20line%20%3Ci%3E246%3C/i%3E%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23e9b96e\'%20colspan%3D\'5\'%3ECall%20Stack%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'center\'%20bgcolor%3D\'%23eeeeec\'%3E%23%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ETime%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EMemory%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EFunction%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ELocation%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E1%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.0013%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E169968%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%7Bmain%7D(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E0%3C/td%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E2%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.3650%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E5284312%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3Eget_file_mime_type(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E62%3C/td%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E3%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.3651%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E5284960%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%3Ca%20href%3D\'http%3A//www.php.net/function.finfo-file\'%20target%3D\'_new\'%3Efinfo_file%3C/a%3E%0A(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cinclude%5Cmime_type_lib.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cmime_type_lib.php%3Cb%3E%3A%3C/b%3E246%3C/td%3E%3C/tr%3E%0A%3C/table%3E%3C/font%3E%0A%3Cbr%20/%3E%0A%3Cfont%20size%3D\'1\'%3E%3Ctable%20class%3D\'xdebug-error%20xe-warning\'%20dir%3D\'ltr\'%20border%3D\'1\'%20cellspacing%3D\'0\'%20cellpadding%3D\'1\'%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23f57900\'%20colspan%3D%225%22%3E%3Cspan%20style%3D\'background-color%3A%20%23cc0000%3B%20color%3A%20%23fce94f%3B%20font-size%3A%20x-large%3B\'%3E(%20!%20)%3C/span%3E%20Warning%3A%20mime_content_type()%3A%20Empty%20filename%20or%20path%20in%20C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cinclude%5Cmime_type_lib.php%20on%20line%20%3Ci%3E261%3C/i%3E%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23e9b96e\'%20colspan%3D\'5\'%3ECall%20Stack%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'center\'%20bgcolor%3D\'%23eeeeec\'%3E%23%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ETime%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EMemory%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EFunction%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ELocation%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E1%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.0013%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E169968%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%7Bmain%7D(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E0%3C/td%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E2%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.3650%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E5284312%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3Eget_file_mime_type(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E62%3C/td%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E3%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.3997%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E5284952%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%3Ca%20href%3D\'http%3A//www.php.net/function.mime-content-type\'%20target%3D\'_new\'%3Emime_content_type%3C/a%3E%0A(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cinclude%5Cmime_type_lib.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cmime_type_lib.php%3Cb%3E%3A%3C/b%3E261%3C/td%3E%3C/tr%3E%0A%3C/table%3E%3C/font%3E%0A%3Cbr%20/%3E%0A%3Cfont%20size%3D\'1\'%3E%3Ctable%20class%3D\'xdebug-error%20xe-warning\'%20dir%3D\'ltr\'%20border%3D\'1\'%20cellspacing%3D\'0\'%20cellpadding%3D\'1\'%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23f57900\'%20colspan%3D%225%22%3E%3Cspan%20style%3D\'background-color%3A%20%23cc0000%3B%20color%3A%20%23fce94f%3B%20font-size%3A%20x-large%3B\'%3E(%20!%20)%3C/span%3E%20Warning%3A%20chmod()%3A%20No%20such%20file%20or%20directory%20in%20C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php%20on%20line%20%3Ci%3E106%3C/i%3E%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23e9b96e\'%20colspan%3D\'5\'%3ECall%20Stack%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'center\'%20bgcolor%3D\'%23eeeeec\'%3E%23%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ETime%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EMemory%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EFunction%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ELocation%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E1%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.0013%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E169968%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%7Bmain%7D(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E0%3C/td%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E2%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.4347%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E5286000%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%3Ca%20href%3D\'http%3A//www.php.net/function.chmod\'%20target%3D\'_new\'%3Echmod%3C/a%3E%0A(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E106%3C/td%3E%3C/tr%3E%0A%3C/table%3E%3C/font%3E%0Ac07fc2a8ff0822e2a1bc62e700eedabc.jpg?1502618274395\" alt=\"\" /><img src=\"/source/pages/c362520fc1451b62755e3ba0afc86964.jpg?1502618308685\" alt=\"\" width=\"107\" height=\"71\" /><img src=\"/source/pages/%3Cbr%20/%3E%0A%3Cfont%20size%3D\'1\'%3E%3Ctable%20class%3D\'xdebug-error%20xe-warning\'%20dir%3D\'ltr\'%20border%3D\'1\'%20cellspacing%3D\'0\'%20cellpadding%3D\'1\'%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23f57900\'%20colspan%3D%225%22%3E%3Cspan%20style%3D\'background-color%3A%20%23cc0000%3B%20color%3A%20%23fce94f%3B%20font-size%3A%20x-large%3B\'%3E(%20!%20)%3C/span%3E%20Warning%3A%20finfo_file()%3A%20Empty%20filename%20or%20path%20in%20C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cinclude%5Cmime_type_lib.php%20on%20line%20%3Ci%3E246%3C/i%3E%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23e9b96e\'%20colspan%3D\'5\'%3ECall%20Stack%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'center\'%20bgcolor%3D\'%23eeeeec\'%3E%23%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ETime%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EMemory%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EFunction%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ELocation%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E1%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.0013%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E169968%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%7Bmain%7D(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E0%3C/td%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E2%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.3627%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E5284312%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3Eget_file_mime_type(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E62%3C/td%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E3%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.3627%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E5284960%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%3Ca%20href%3D\'http%3A//www.php.net/function.finfo-file\'%20target%3D\'_new\'%3Efinfo_file%3C/a%3E%0A(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cinclude%5Cmime_type_lib.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cmime_type_lib.php%3Cb%3E%3A%3C/b%3E246%3C/td%3E%3C/tr%3E%0A%3C/table%3E%3C/font%3E%0A%3Cbr%20/%3E%0A%3Cfont%20size%3D\'1\'%3E%3Ctable%20class%3D\'xdebug-error%20xe-warning\'%20dir%3D\'ltr\'%20border%3D\'1\'%20cellspacing%3D\'0\'%20cellpadding%3D\'1\'%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23f57900\'%20colspan%3D%225%22%3E%3Cspan%20style%3D\'background-color%3A%20%23cc0000%3B%20color%3A%20%23fce94f%3B%20font-size%3A%20x-large%3B\'%3E(%20!%20)%3C/span%3E%20Warning%3A%20mime_content_type()%3A%20Empty%20filename%20or%20path%20in%20C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cinclude%5Cmime_type_lib.php%20on%20line%20%3Ci%3E261%3C/i%3E%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23e9b96e\'%20colspan%3D\'5\'%3ECall%20Stack%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'center\'%20bgcolor%3D\'%23eeeeec\'%3E%23%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ETime%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EMemory%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EFunction%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ELocation%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E1%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.0013%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E169968%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%7Bmain%7D(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E0%3C/td%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E2%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.3627%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E5284312%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3Eget_file_mime_type(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E62%3C/td%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E3%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.4084%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E5284952%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%3Ca%20href%3D\'http%3A//www.php.net/function.mime-content-type\'%20target%3D\'_new\'%3Emime_content_type%3C/a%3E%0A(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cinclude%5Cmime_type_lib.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cmime_type_lib.php%3Cb%3E%3A%3C/b%3E261%3C/td%3E%3C/tr%3E%0A%3C/table%3E%3C/font%3E%0A%3Cbr%20/%3E%0A%3Cfont%20size%3D\'1\'%3E%3Ctable%20class%3D\'xdebug-error%20xe-warning\'%20dir%3D\'ltr\'%20border%3D\'1\'%20cellspacing%3D\'0\'%20cellpadding%3D\'1\'%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23f57900\'%20colspan%3D%225%22%3E%3Cspan%20style%3D\'background-color%3A%20%23cc0000%3B%20color%3A%20%23fce94f%3B%20font-size%3A%20x-large%3B\'%3E(%20!%20)%3C/span%3E%20Warning%3A%20chmod()%3A%20No%20such%20file%20or%20directory%20in%20C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php%20on%20line%20%3Ci%3E106%3C/i%3E%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23e9b96e\'%20colspan%3D\'5\'%3ECall%20Stack%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Cth%20align%3D\'center\'%20bgcolor%3D\'%23eeeeec\'%3E%23%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ETime%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EMemory%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3EFunction%3C/th%3E%3Cth%20align%3D\'left\'%20bgcolor%3D\'%23eeeeec\'%3ELocation%3C/th%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E1%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.0013%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E169968%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%7Bmain%7D(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E0%3C/td%3E%3C/tr%3E%0A%3Ctr%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E2%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'center\'%3E0.4467%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%20align%3D\'right\'%3E5286000%3C/td%3E%3Ctd%20bgcolor%3D\'%23eeeeec\'%3E%3Ca%20href%3D\'http%3A//www.php.net/function.chmod\'%20target%3D\'_new\'%3Echmod%3C/a%3E%0A(%20%20)%3C/td%3E%3Ctd%20title%3D\'C%3A%5Cxampp%5Chtdocs%5CCMSUltimate%5Cpublic%5Cfilemanager%5Cupload.php\'%20bgcolor%3D\'%23eeeeec\'%3E..%5Cupload.php%3Cb%3E%3A%3C/b%3E106%3C/td%3E%3C/tr%3E%0A%3C/table%3E%3C/font%3E%0Aa3f62254b4e517b1e891164b1b23ed44.jpg?1502618256475\" alt=\"\" /></td>\r\n<td><a title=\"لابا\" href=\"/source/pages/065b5d58dfba5055949435ae116d473b.pdf\" target=\"_blank\">غاف</a></td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>', 'مهمانی', '1', 'fa', 'مهمانی', null, '', '', '2017-03-16 12:00:59', '2017-08-13 15:54:48', '0', '28', '12', null);
INSERT INTO `application_pages` VALUES ('36', '0', '<p><span style=\"font-size: 18pt;\">طراحی سایت خوب است.</span></p>\r\n<p>ما طراحی سایت انجام میدهیم</p>\r\n<p>ما طراحی سایت دوست داریم.</p>\r\n<p>ما خیلی خوب هستیم.</p>\r\n<p>صرفا جهت شوخی...<img src=\"/source/pages/2d0d6013c7b512d4a33f61a9f3846d1d.png?1491371257105\" alt=\"عکس شانسی\" width=\"453\" height=\"290\" /></p>', 'طراحی سایت', '1', 'fa', 'طراحی-سایت', null, 'طراحی سایت حرفه خوبی است. طراحی سایت حرفه ای بسیار مهم است.', 'طراحی سایت, طراحی سایت حرفه ای, طراحی وب سایت ', '2017-04-05 10:28:29', '2017-04-05 10:28:36', '0', '0', '0', null);
INSERT INTO `application_pages` VALUES ('37', '0', '<p>[plg]\'gallery/index/index\', array( \"latin_name\" => \"maingal\" , \"layout\" => \"listgallery\" , \"limit\" => \"10\")[/plg]</p>', 'گالری تصاویر کلی', '1', 'fa', 'گالری-تصاویر-کلی', null, '', '', '2017-04-05 14:50:57', '2017-04-05 14:50:47', '0', '0', '0', null);
INSERT INTO `application_pages` VALUES ('38', '0', '<p>[plg]\'faq/index/index\'[/plg]</p>', 'سوالات متداول', '1', 'fa', 'سوالات-متداول', null, '', '', '2017-04-08 13:25:16', '2017-07-11 11:56:55', '0', '0', '0', null);
INSERT INTO `application_pages` VALUES ('39', '0', '<p>[plg]\'video-gallery/index/page\'[/plg]</p>', 'گالری ویدئو', '1', 'fa', 'گالری-ویدئو', null, '', '', '2017-04-08 16:14:49', '2017-04-08 16:14:48', '0', '0', '0', null);
INSERT INTO `application_pages` VALUES ('40', '0', '', 'گالری ویدئو2', '1', 'fa', 'گالری-ویدئو2', null, '', '', '2017-04-08 16:27:51', '2017-04-08 16:27:50', '0', '29', '0', null);
INSERT INTO `application_pages` VALUES ('41', '0', '<p style=\"text-align: justify;\">شرکت شبکه ارتباط شبدیز سال ها است که زبده ترین کارشناسان حوزی برنامه نویسی و وب را برای پیاده سازی و توسعه وب سایت های کارآمد گرد هم آورده و اکنون هم با افتخار می توانیم ادعا کنیم که با پشت سر گذاشتن موفقیت آمیز چالش های فراوان در این حوزه، به یکی از موفق ترین شرکت های فعال در توسعه راه کارهای مبتنی بر وب در استان البرز و شهر کرج مبدل شده ایم.</p>\r\n<p style=\"text-align: justify;\">علاوه بر این، شرکت شبدیز با تمرکز بر طراحی و برنامه نویسی پرتال های بزرگ و متفاوت، توانسته نرم افزارهای تحت وب با قدرت، امنیت بالا و متناسب با نیازهای مشتریان تولید کند که این تنها نشان دهنده بخشی از توانایی های مهندسین شبدیز می باشد.</p>\r\n<p style=\"text-align: justify;\">آنچه سایت‌های طراحی شده توسط دپارتمان طراحی سایت شرکت شبدیز را از رقبا متمایز می‌سازد، در موارد ذیل خلاصه می گردد:</p>\r\n<ul>\r\n<li style=\"text-align: justify;\"><span style=\"color: #800000;\"><strong>امنیت بالا:</strong></span> تیم برنامه نویسی شرکت شبدیز با استفاده از آخرین متدهای برنامه نویسی و فرم ورک های برتر دنیا، اقدام به اجرای پروژه های مشتریان می نمایند و این در حالی است که امنیت در تمامی این پروژه ها حرف اول را می زند.</li>\r\n<li style=\"text-align: justify;\"><span style=\"color: #800000;\"><strong>پاسخگویی به نیازهای متغیر مشتریان:</strong></span> برنامه نویسان شرکت شبدیز با تجربه و تخصص بالایی که دارند، خواهند توانست رویای شما در حوزه وب را به واقعیت تبدیل کنند که این مهم جز با شناخت دقیق نیازهای مشتریان، تحلیل و پیاده‌سازی حرفه‌ای آن‌ها امکان‌پذیر نخواهد بود.</li>\r\n<li style=\"text-align: justify;\"><span style=\"color: #800000;\"><strong>سیستم مدیریت محتوای اختصاصی شما</strong>:</span> CMSهای آماده برای سایت های از پیش تعریف شده بسیار مناسب می باشند و ما منکر کارآیی آنها نیستیم ولی برای وب سایت های که قرار است کاری متفاوت انجام دهند به هیچ عنوان مناسب نمی باشد و این پروژه ها می بایست از ابتدا توسط برنامه نویسان مجرب با توجه به نیاز های شما تحلیل، طراحی و پیاده سازی شوند.</li>\r\n<li style=\"text-align: justify;\"><span style=\"color: #800000;\"><strong>انعطاف پذیری بالا:</strong></span> به دلیل تحلیل دقیق و پیاده سازی هوشمندانه متخصیص شبدیز، پروژه های پیاده سازی شده انعطاف بسیار بالایی دارند و به راحتی تغییرات احتمالی در آینده بسته به نیازهای مشتریان قابل اجرا است.</li>\r\n<li style=\"text-align: justify;\"><span style=\"color: #800000;\"><strong>کاربر پسند بودن:</strong></span> شرکت شبدیز با داشتن تیم حرفه ای R&D به صورت مداوم با تحقیق در مورد تجربه کاربری و رابط کاربری، نیازهای کاربران وب فارسی را تحلیل کرده و آن‌ها در اجرای پروژه های خود عملی می سازد.</li>\r\n</ul>', 'دربار ه ما', '1', 'fa', 'دربار-ه-ما', null, '', '', '2017-04-10 15:53:53', '2017-07-08 12:05:33', '21', '28', '12', null);

-- ----------------------------
-- Table structure for application_product
-- ----------------------------
DROP TABLE IF EXISTS `application_product`;
CREATE TABLE `application_product` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` int(10) unsigned NOT NULL,
  `pname` varchar(255) NOT NULL,
  `downloadable` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_accessory` tinyint(1) NOT NULL DEFAULT '0',
  `pname_id` int(11) unsigned DEFAULT '0' COMMENT 'if is accessory set product main name',
  `attribute_parrent_id` int(11) unsigned DEFAULT NULL,
  `group_id` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `shop_id` (`shop_id`) USING BTREE,
  KEY `name_index` (`pname`) USING BTREE,
  KEY `is_accessory_index` (`is_accessory`) USING BTREE,
  KEY `shop_id_name_index` (`shop_id`,`pname`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_product
-- ----------------------------

-- ----------------------------
-- Table structure for application_product_accessory
-- ----------------------------
DROP TABLE IF EXISTS `application_product_accessory`;
CREATE TABLE `application_product_accessory` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(20) unsigned NOT NULL,
  `product_accessory_id` int(20) unsigned NOT NULL,
  `accessory` tinyint(1) NOT NULL DEFAULT '0',
  `is_force` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:enable, 0:disable',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `product_main_id` (`product_accessory_id`) USING BTREE,
  KEY `product_view_id` (`product_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_product_accessory
-- ----------------------------

-- ----------------------------
-- Table structure for application_product_attributes
-- ----------------------------
DROP TABLE IF EXISTS `application_product_attributes`;
CREATE TABLE `application_product_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_main_id` bigint(20) unsigned NOT NULL COMMENT 'Product_Main ID',
  `attribute_set_id` int(11) NOT NULL,
  `attribute_id` bigint(20) unsigned NOT NULL COMMENT 'Attribute ID',
  `value` text NOT NULL,
  `attribute_option_id` varchar(20) DEFAULT NULL,
  `preview` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `product_main_id_2` (`product_main_id`,`attribute_id`) USING BTREE,
  KEY `product_main_id` (`product_main_id`) USING BTREE,
  KEY `attributes_id` (`attribute_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_product_attributes
-- ----------------------------

-- ----------------------------
-- Table structure for application_product_files
-- ----------------------------
DROP TABLE IF EXISTS `application_product_files`;
CREATE TABLE `application_product_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_main_id` bigint(20) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `format` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `is_special` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'آيا اين عكس ، ويژه مي باشد؟',
  `original_file_name` varchar(255) NOT NULL,
  `extention` varchar(10) NOT NULL,
  `mime_type` varchar(50) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `order` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_product_files
-- ----------------------------

-- ----------------------------
-- Table structure for application_product_group
-- ----------------------------
DROP TABLE IF EXISTS `application_product_group`;
CREATE TABLE `application_product_group` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `keywords` varchar(1024) DEFAULT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `is_parent` tinyint(1) NOT NULL,
  `parent_id` bigint(20) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT '2016-03-16 11:06:22' ON UPDATE CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT ' ',
  `attribute_set_id` int(11) NOT NULL,
  `order` int(11) DEFAULT '0',
  `compareable` int(1) unsigned NOT NULL DEFAULT '1',
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_product_group
-- ----------------------------
INSERT INTO `application_product_group` VALUES ('1', '1', 'موبایل', 'آیفون , اف اف , مانیتور , ایفون', '', '1', '0', '1', '2018-03-10 14:41:23', '0000-00-00 00:00:00', '1', '1', '1', null);
INSERT INTO `application_product_group` VALUES ('2', '1', 'اپل', 'آیفون , اف اف , مانیتور , ایفون', '', '1', '1', '1', '2018-03-10 14:41:17', '2018-01-30 08:01:57', '1', '2', '1', null);
INSERT INTO `application_product_group` VALUES ('3', '1', 'سامسونگ', 'آیفون , اف اف , مانیتور , ایفون', '', '1', '1', '1', '2018-03-10 14:41:15', '2018-01-30 08:01:46', '1', '3', '1', null);
INSERT INTO `application_product_group` VALUES ('4', '1', 'نکسوس', 'آیفون , اف اف , مانیتور , ایفون', '', '1', '1', '1', '2018-03-10 14:41:13', '2018-01-30 08:01:31', '1', '4', '1', null);
INSERT INTO `application_product_group` VALUES ('5', '1', 'نوکیا', 'آیفون , اف اف , مانیتور , ایفون', '', '1', '1', '1', '2018-03-10 14:41:12', '2018-01-30 08:01:25', '1', '5', '1', null);
INSERT INTO `application_product_group` VALUES ('6', '2', 'گروه لوازم برقی', 'لوازم برقی خانگی', 'لوازم برقی خانگیلوازم برقی خانگیلوازم برقی خانگی', '1', '0', '1', '2018-06-20 12:10:22', '2018-06-16 10:17:18', '0', '2', '0', null);
INSERT INTO `application_product_group` VALUES ('8', '2', 'asdasd', '', '', '0', '0', '1', '2016-03-16 11:06:22', '0000-00-00 00:00:00', '0', '0', '0', null);
INSERT INTO `application_product_group` VALUES ('10', '2', 'گروه لوازم برقی11', 'لوازم برقی خانگی', 'qwee', '0', '0', '1', '2016-03-16 11:06:22', '0000-00-00 00:00:00', '2', '2', '0', null);
INSERT INTO `application_product_group` VALUES ('11', '2', 'fdgdfgdfg', 'sdfdfs', 'sdf', '0', '0', '1', '2016-03-16 11:06:22', '0000-00-00 00:00:00', '2', '4', '0', null);
INSERT INTO `application_product_group` VALUES ('12', '2', 'sadasd33', '', '3', '0', '0', '1', '2016-03-16 11:06:22', '0000-00-00 00:00:00', '2', '4', '0', null);
INSERT INTO `application_product_group` VALUES ('13', '2', 'sdfsdf', 'sdf', 'dsf', '0', '0', '1', '2016-03-16 11:06:22', '0000-00-00 00:00:00', '2', '0', '0', null);
INSERT INTO `application_product_group` VALUES ('14', '2', 'sdfsdfpp', ',,.', '', '0', '0', '1', '2016-03-16 11:06:22', '0000-00-00 00:00:00', '2', '0', '0', null);

-- ----------------------------
-- Table structure for application_product_group_files
-- ----------------------------
DROP TABLE IF EXISTS `application_product_group_files`;
CREATE TABLE `application_product_group_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_group_id` bigint(20) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `format` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `is_special` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ø¢ÙŠØ§ Ø§ÙŠÙ† Ø¹ÙƒØ³ ØŒ ÙˆÙŠÚ˜Ù‡ Ù…ÙŠ Ø¨Ø§Ø´Ø¯ØŸ',
  `position` int(10) unsigned NOT NULL,
  `original_file_name` varchar(255) NOT NULL,
  `extention` varchar(10) NOT NULL,
  `mime_type` varchar(50) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_product_group_files
-- ----------------------------

-- ----------------------------
-- Table structure for application_product_group_product
-- ----------------------------
DROP TABLE IF EXISTS `application_product_group_product`;
CREATE TABLE `application_product_group_product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) unsigned NOT NULL COMMENT 'GroupID',
  `product_id` bigint(20) unsigned NOT NULL COMMENT 'Product_MainID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pmid` (`product_id`) USING BTREE,
  KEY `gid` (`group_id`) USING BTREE,
  KEY `gid_and_pmid` (`group_id`,`product_id`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_product_group_product
-- ----------------------------

-- ----------------------------
-- Table structure for application_product_price
-- ----------------------------
DROP TABLE IF EXISTS `application_product_price`;
CREATE TABLE `application_product_price` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_main_product_view_id` int(20) unsigned NOT NULL,
  `price` int(20) NOT NULL,
  `from_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `to_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `product_main_product_view_id` (`product_main_product_view_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_product_price
-- ----------------------------

-- ----------------------------
-- Table structure for application_product_scores
-- ----------------------------
DROP TABLE IF EXISTS `application_product_scores`;
CREATE TABLE `application_product_scores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_main_id` bigint(20) unsigned NOT NULL,
  `from_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `to_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scores` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `product_main_id` (`product_main_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_product_scores
-- ----------------------------

-- ----------------------------
-- Table structure for application_shop
-- ----------------------------
DROP TABLE IF EXISTS `application_shop`;
CREATE TABLE `application_shop` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 's',
  `address` longtext,
  `telf` varchar(11) DEFAULT NULL,
  `fax` varchar(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tels` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` int(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_shop
-- ----------------------------
INSERT INTO `application_shop` VALUES ('3', 'فروشگاه رنگارنگ', 'asd sad asdsa', '021-1233', '234234', null, '1212123', 'http://shop.netshahr.com/', '1');
INSERT INTO `application_shop` VALUES ('4', 'فروشگاه اینترنتی فیاب', 'تهران میدان تجربش جنب فروشگاه شماره دو ارتش شماره 12', '021-1233456', '3424', null, '325555', 'http://patrino.netshahr.comm', '1');
INSERT INTO `application_shop` VALUES ('6', 'فروشگاه اینترنتی فیاب3', 'weqwe', '23434', '343', null, '324234', 'http://shop.netshahr.com/', '1');

-- ----------------------------
-- Table structure for application_shop_owners
-- ----------------------------
DROP TABLE IF EXISTS `application_shop_owners`;
CREATE TABLE `application_shop_owners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL COMMENT 'shop keeper',
  `shop_id` int(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_shop_owners
-- ----------------------------
INSERT INTO `application_shop_owners` VALUES ('2', '5', '4', '1');
INSERT INTO `application_shop_owners` VALUES ('3', '3', '3', '1');
INSERT INTO `application_shop_owners` VALUES ('4', '3', '6', '1');
INSERT INTO `application_shop_owners` VALUES ('5', '3', '5', '1');
INSERT INTO `application_shop_owners` VALUES ('6', '3', '7', '1');

-- ----------------------------
-- Table structure for application_sub_menus
-- ----------------------------
DROP TABLE IF EXISTS `application_sub_menus`;
CREATE TABLE `application_sub_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_menu_id` int(11) NOT NULL,
  `parent_id` varchar(255) NOT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `external_url` varchar(255) DEFAULT NULL,
  `internal_url` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `order` int(10) unsigned DEFAULT NULL,
  `class_name_ul` varchar(255) DEFAULT NULL,
  `class_name_li` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `width` varchar(20) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `main_menu_id` (`main_menu_id`) USING BTREE,
  CONSTRAINT `application_sub_menus_ibfk_1` FOREIGN KEY (`main_menu_id`) REFERENCES `application_menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_sub_menus
-- ----------------------------
INSERT INTO `application_sub_menus` VALUES ('1', '1', '0', 'خانه', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', 'fa fa-home');
INSERT INTO `application_sub_menus` VALUES ('2', '1', '0', 'درباره ما', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('3', '1', '0', 'خدمات ما', 'asdsd', 'گالری', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('4', '1', '0', 'گالری تصاویر', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('5', '1', '0', 'سوالات متداول', 'asdsd', 'سوالات', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('6', '1', '0', 'خدمات ما', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('7', '1', '6', 'خدمات شرکت', '', 'مهمانی', null, '2', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('8', '1', '7', 'خدمات باشگاهی', '', 'گالری', null, '3', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('9', '1', '7', 'Opcion7-1-1', 'asdsd', '0', null, '3', null, null, '0', null, 'dropdown', 'fa fa-filter');
INSERT INTO `application_sub_menus` VALUES ('10', '1', '0', 'خدمات آزاد', '', 'گالری', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('11', '1', '0', 'خانه', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', 'fa fa-home');
INSERT INTO `application_sub_menus` VALUES ('12', '1', '0', 'درباره ما', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('13', '1', '0', 'خدمات ما', 'asdsd', 'گالری', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('14', '1', '0', 'گالری تصاویر', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('15', '1', '0', 'سوالات متداول', 'asdsd', 'سوالات', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('16', '1', '0', 'خدمات ما', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('17', '1', '16', 'خدمات شرکت', '', 'مهمانی', null, '2', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('18', '1', '17', 'خدمات باشگاهی', '', 'گالری', null, '3', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('19', '1', '18', 'خدمات آزاد', '', 'گالری', null, '4', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('20', '1', '17', 'Opcion7-1-1', 'asdsd', '0', null, '3', null, null, '0', null, 'dropdown', 'fa fa-filter');
INSERT INTO `application_sub_menus` VALUES ('21', '1', '0', 'خانه', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', 'fa fa-home');
INSERT INTO `application_sub_menus` VALUES ('22', '1', '21', 'خدمات ما', 'asdsd', 'گالری', null, '2', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('23', '1', '22', 'درباره ما', 'asdsd', '0', null, '3', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('24', '1', '0', 'گالری تصاویر', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('25', '1', '0', 'سوالات متداول', 'asdsd', 'سوالات', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('26', '1', '0', 'خدمات ما', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('27', '1', '26', 'خدمات شرکت', '', 'مهمانی', null, '2', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('28', '1', '27', 'خدمات باشگاهی', '', 'گالری', null, '3', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('29', '1', '27', 'Opcion7-1-1', 'asdsd', '0', null, '3', null, null, '0', null, 'dropdown', 'fa fa-filter');
INSERT INTO `application_sub_menus` VALUES ('30', '1', '0', 'خدمات آزاد', '', 'گالری', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('31', '1', '0', 'خانه', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', 'fa fa-home');
INSERT INTO `application_sub_menus` VALUES ('32', '1', '0', 'درباره ما', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('33', '1', '0', 'گالری تصاویر', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('34', '1', '33', 'خدمات ما', 'asdsd', 'گالری', null, '2', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('35', '1', '0', 'سوالات متداول', 'asdsd', 'سوالات', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('36', '1', '0', 'خدمات ما', 'asdsd', '0', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('37', '1', '36', 'خدمات شرکت', '', 'مهمانی', null, '2', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('38', '1', '37', 'خدمات باشگاهی', '', 'گالری', null, '3', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('39', '1', '38', 'زیر مجموعه خدمات ', 'asdsd', '0', null, '4', null, null, '0', null, 'dropdown', 'fa fa-filter');
INSERT INTO `application_sub_menus` VALUES ('40', '1', '37', 'خدمات آزاد', '', 'گالری', null, '3', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('41', '1', '0', 'خانه', 'http://cmsultimate.netshahr.com/fa/admin-menu/show-menu/1', 'دربار', null, '1', null, null, '0', null, 'dropdown', 'fa fa-home');
INSERT INTO `application_sub_menus` VALUES ('42', '1', '0', 'درباره ما', '_self', '_self', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('43', '1', '0', 'گالری تصاویر', '_self', '_self', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('44', '1', '43', 'خدمات ما', '_self', '_self', null, '2', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('45', '1', '0', 'سوالات متداول', '_self', '_self', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('46', '1', '0', 'خدمات ما', '_self', '_self', null, '1', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('47', '1', '46', 'خدمات شرکت', '_self', '_self', null, '2', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('48', '1', '47', 'خدمات باشگاهی', '_self', '_self', null, '3', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('49', '1', '48', 'زیر مجموعه خدمات ', '_self', '_self', null, '4', null, null, '0', null, 'dropdown', 'fa fa-filter');
INSERT INTO `application_sub_menus` VALUES ('50', '1', '47', 'خدمات آزاد', '_self', '_self', null, '3', null, null, '0', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('51', '1', '0', 'خانه', '_self', '_self', null, '1', null, null, '1', null, 'dropdown', 'fa fa-home');
INSERT INTO `application_sub_menus` VALUES ('52', '1', '0', 'درباره ما', '_self', '_self', null, '1', null, null, '1', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('53', '1', '0', 'گالری تصاویر', '_self', '_self', null, '1', null, null, '1', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('54', '1', '53', 'خدمات ما', '_self', '_self', null, '2', null, null, '1', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('55', '1', '54', 'خدمات ما', '_self', '_self', null, '3', null, null, '1', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('56', '1', '55', 'خدمات شرکت', '_self', '_self', null, '4', null, null, '1', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('57', '1', '56', 'خدمات باشگاهی', '_self', '_self', null, '5', null, null, '1', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('58', '1', '57', 'زیر مجموعه خدمات ', '_self', '_self', null, '6', null, null, '1', null, 'dropdown', 'fa fa-filter');
INSERT INTO `application_sub_menus` VALUES ('59', '1', '56', 'خدمات آزاد', '_self', '_self', null, '5', null, null, '1', null, 'dropdown', '');
INSERT INTO `application_sub_menus` VALUES ('60', '1', '0', 'سوالات متداول', '_self', '_self', null, '1', null, null, '1', null, 'dropdown', '');

-- ----------------------------
-- Table structure for application_users
-- ----------------------------
DROP TABLE IF EXISTS `application_users`;
CREATE TABLE `application_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `status` int(1) unsigned DEFAULT '1',
  `user_group_id` int(11) unsigned DEFAULT NULL,
  `expiry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_login` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_users
-- ----------------------------
INSERT INTO `application_users` VALUES ('1', 'parkhcom@gmail.com', '32c3a7b43f6e65fe94304eda1d5a0ddd', 'parkhcom@gmail.com', null, '5626953', '1', '1', '2018-06-20 12:11:15', '1529480475');
INSERT INTO `application_users` VALUES ('2', 'supperadmin@gmail.com', '32c3a7b43f6e65fe94304eda1d5a0ddd', 'saminkhcom@gmail.com', null, '5626953', '1', '2', '2018-06-15 12:28:27', '1528996206');
INSERT INTO `application_users` VALUES ('3', 'shopkeeper@gmail.com', '32c3a7b43f6e65fe94304eda1d5a0ddd', 'shopkeeper@gmail.com', null, '5626953', '1', '3', '2018-06-15 12:29:23', '1528996206');
INSERT INTO `application_users` VALUES ('4', 'customer@gmail.com', '32c3a7b43f6e65fe94304eda1d5a0ddd', 'saminkhcom@gmail.com', null, '5626953', '1', '4', '2018-06-15 12:28:41', '1528996206');
INSERT INTO `application_users` VALUES ('5', 'shopkeeper1@gmail.com', '32c3a7b43f6e65fe94304eda1d5a0ddd', 'shopkeeper1@gmail.com', null, '5626953', '1', '3', '2018-06-15 12:29:19', '1528996206');

-- ----------------------------
-- Table structure for application_users_block_ip
-- ----------------------------
DROP TABLE IF EXISTS `application_users_block_ip`;
CREATE TABLE `application_users_block_ip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expiry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_users_block_ip
-- ----------------------------

-- ----------------------------
-- Table structure for application_users_groups
-- ----------------------------
DROP TABLE IF EXISTS `application_users_groups`;
CREATE TABLE `application_users_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_users_groups
-- ----------------------------
INSERT INTO `application_users_groups` VALUES ('1', 'programmer');
INSERT INTO `application_users_groups` VALUES ('2', 'supperadmin');
INSERT INTO `application_users_groups` VALUES ('3', 'shopkeeper');
INSERT INTO `application_users_groups` VALUES ('4', 'customers');

-- ----------------------------
-- Table structure for application_users_groups_permissions
-- ----------------------------
DROP TABLE IF EXISTS `application_users_groups_permissions`;
CREATE TABLE `application_users_groups_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned DEFAULT NULL,
  `user_group_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `permission_id` (`permission_id`) USING BTREE,
  KEY `application_users_groups_permissions_ibfk_2` (`user_group_id`) USING BTREE,
  CONSTRAINT `application_users_groups_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `application_users_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `application_users_groups_permissions_ibfk_2` FOREIGN KEY (`user_group_id`) REFERENCES `application_users_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=497 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_users_groups_permissions
-- ----------------------------
INSERT INTO `application_users_groups_permissions` VALUES ('410', '3', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('411', '3', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('412', '4', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('413', '4', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('414', '86', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('415', '27', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('416', '27', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('417', '29', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('418', '29', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('419', '30', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('420', '30', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('421', '26', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('422', '26', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('423', '28', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('424', '28', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('425', '32', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('426', '32', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('427', '36', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('428', '36', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('429', '35', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('430', '35', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('431', '37', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('432', '37', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('433', '31', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('434', '31', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('435', '41', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('436', '34', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('437', '34', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('438', '33', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('439', '33', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('440', '43', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('441', '47', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('442', '45', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('443', '49', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('444', '44', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('445', '48', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('446', '50', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('447', '42', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('448', '46', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('449', '54', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('450', '56', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('451', '55', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('452', '57', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('453', '53', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('454', '51', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('455', '52', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('456', '58', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('457', '59', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('458', '85', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('459', '85', '2');
INSERT INTO `application_users_groups_permissions` VALUES ('460', '99', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('461', '103', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('462', '98', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('463', '102', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('464', '95', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('465', '93', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('466', '100', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('467', '101', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('468', '91', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('469', '89', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('470', '92', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('471', '88', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('472', '87', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('473', '97', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('474', '90', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('475', '94', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('476', '96', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('477', '71', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('478', '73', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('479', '72', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('480', '74', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('481', '70', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('482', '66', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('483', '68', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('484', '67', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('485', '69', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('486', '65', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('487', '105', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('488', '107', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('489', '104', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('490', '108', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('491', '106', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('492', '81', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('493', '83', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('494', '82', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('495', '84', '1');
INSERT INTO `application_users_groups_permissions` VALUES ('496', '80', '1');

-- ----------------------------
-- Table structure for application_users_permissions
-- ----------------------------
DROP TABLE IF EXISTS `application_users_permissions`;
CREATE TABLE `application_users_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exception` varchar(1000) DEFAULT NULL,
  `permission_name` varchar(1000) DEFAULT NULL,
  `permission_label` varchar(1000) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of application_users_permissions
-- ----------------------------
INSERT INTO `application_users_permissions` VALUES ('3', '1', 'application-admin-dashboard', 'dashboard', 'application');
INSERT INTO `application_users_permissions` VALUES ('4', '1', 'application-admin-index', 'index', 'application');
INSERT INTO `application_users_permissions` VALUES ('26', '1', 'application-page-list', 'list', 'application');
INSERT INTO `application_users_permissions` VALUES ('27', '1', 'application-page-add', 'add', 'application');
INSERT INTO `application_users_permissions` VALUES ('28', '1', 'application-page-update', 'update', 'application');
INSERT INTO `application_users_permissions` VALUES ('29', '1', 'application-page-delete', 'delete', 'application');
INSERT INTO `application_users_permissions` VALUES ('30', '1', 'application-page-index', 'index', 'application');
INSERT INTO `application_users_permissions` VALUES ('31', '1', 'application-permission-list', 'list', 'application');
INSERT INTO `application_users_permissions` VALUES ('32', '1', 'application-permission-add', 'add', 'application');
INSERT INTO `application_users_permissions` VALUES ('33', '1', 'application-permission-update-acl', 'update', 'application');
INSERT INTO `application_users_permissions` VALUES ('34', '1', 'application-permission-update', 'update', 'application');
INSERT INTO `application_users_permissions` VALUES ('35', '1', 'application-permission-delete', 'delete', 'application');
INSERT INTO `application_users_permissions` VALUES ('36', '1', 'application-permission-assign-permission-to-user-group', 'assign', 'application');
INSERT INTO `application_users_permissions` VALUES ('37', '1', 'application-permission-index', 'index', 'application');
INSERT INTO `application_users_permissions` VALUES ('41', '1', 'application-permission-lists', 'lists', 'application');
INSERT INTO `application_users_permissions` VALUES ('42', '1', 'block-admin-list-blocks', 'list', 'block');
INSERT INTO `application_users_permissions` VALUES ('43', '1', 'block-admin-add-block', 'add', 'block');
INSERT INTO `application_users_permissions` VALUES ('44', '1', 'block-admin-edit-block', 'edit', 'block');
INSERT INTO `application_users_permissions` VALUES ('45', '1', 'block-admin-delete-block', 'delete', 'block');
INSERT INTO `application_users_permissions` VALUES ('46', '1', 'block-admin-lists-layout', 'lists', 'block');
INSERT INTO `application_users_permissions` VALUES ('47', '1', 'block-admin-add-layout', 'add', 'block');
INSERT INTO `application_users_permissions` VALUES ('48', '1', 'block-admin-edit-layout', 'edit', 'block');
INSERT INTO `application_users_permissions` VALUES ('49', '1', 'block-admin-delete-layout', 'delete', 'block');
INSERT INTO `application_users_permissions` VALUES ('50', '1', 'block-admin-index', 'index', 'block');
INSERT INTO `application_users_permissions` VALUES ('51', '1', 'menu-admin-show-menu', 'show', 'menu');
INSERT INTO `application_users_permissions` VALUES ('52', '1', 'menu-admin-show-menu-old', 'show', 'menu');
INSERT INTO `application_users_permissions` VALUES ('53', '1', 'menu-admin-list-main-menus', 'list', 'menu');
INSERT INTO `application_users_permissions` VALUES ('54', '1', 'menu-admin-add-main-menu', 'add', 'menu');
INSERT INTO `application_users_permissions` VALUES ('55', '1', 'menu-admin-edit-main-menu', 'edit', 'menu');
INSERT INTO `application_users_permissions` VALUES ('56', '1', 'menu-admin-delete-main-menu', 'delete', 'menu');
INSERT INTO `application_users_permissions` VALUES ('57', '1', 'menu-admin-index', 'index', 'menu');
INSERT INTO `application_users_permissions` VALUES ('58', '1', 'menu-ajax-delete-menu', 'delete', 'menu');
INSERT INTO `application_users_permissions` VALUES ('59', '1', 'menu-ajax-index', 'index', 'menu');
INSERT INTO `application_users_permissions` VALUES ('65', '1', 'products-product-list', 'list', 'products');
INSERT INTO `application_users_permissions` VALUES ('66', '1', 'products-product-add', 'add', 'products');
INSERT INTO `application_users_permissions` VALUES ('67', '1', 'products-product-edit', 'edit', 'products');
INSERT INTO `application_users_permissions` VALUES ('68', '1', 'products-product-delete', 'delete', 'products');
INSERT INTO `application_users_permissions` VALUES ('69', '1', 'products-product-index', 'index', 'products');
INSERT INTO `application_users_permissions` VALUES ('70', '1', 'products-attribute-list', 'list', 'products');
INSERT INTO `application_users_permissions` VALUES ('71', '1', 'products-attribute-add', 'add', 'products');
INSERT INTO `application_users_permissions` VALUES ('72', '1', 'products-attribute-edit', 'edit', 'products');
INSERT INTO `application_users_permissions` VALUES ('73', '1', 'products-attribute-delete', 'delete', 'products');
INSERT INTO `application_users_permissions` VALUES ('74', '1', 'products-attribute-index', 'index', 'products');
INSERT INTO `application_users_permissions` VALUES ('80', '1', 'products-shop-list', 'list', 'products');
INSERT INTO `application_users_permissions` VALUES ('81', '1', 'products-shop-add', 'add', 'products');
INSERT INTO `application_users_permissions` VALUES ('82', '1', 'products-shop-edit', 'edit', 'products');
INSERT INTO `application_users_permissions` VALUES ('83', '1', 'products-shop-delete', 'delete', 'products');
INSERT INTO `application_users_permissions` VALUES ('84', '1', 'products-shop-index', 'index', 'products');
INSERT INTO `application_users_permissions` VALUES ('85', '1', 'products-ajax-check-name-exist', 'check', 'products');
INSERT INTO `application_users_permissions` VALUES ('86', '1', 'application-baseajax-index', 'index', 'application');
INSERT INTO `application_users_permissions` VALUES ('87', '1', 'products-ajax-search-product', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('88', '1', 'products-ajax-search-parrent', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('89', '1', 'products-ajax-search-group', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('90', '1', 'products-ajax-search-products', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('91', '1', 'products-ajax-search-attributes', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('92', '1', 'products-ajax-search-other-attributes', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('93', '1', 'products-ajax-search-all-product-main', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('94', '1', 'products-ajax-search-same-attributes', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('95', '1', 'products-ajax-search-accessories', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('96', '1', 'products-ajax-search-selected-products', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('97', '1', 'products-ajax-search-product-main-product-view', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('98', '1', 'products-ajax-register-new-product', 'register', 'products');
INSERT INTO `application_users_permissions` VALUES ('99', '1', 'products-ajax-get-attribute-child', 'get', 'products');
INSERT INTO `application_users_permissions` VALUES ('100', '1', 'products-ajax-search-all-product-main-banner', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('101', '1', 'products-ajax-search-attribute-by-parrent-set', 'search', 'products');
INSERT INTO `application_users_permissions` VALUES ('102', '1', 'products-ajax-register-order', 'register', 'products');
INSERT INTO `application_users_permissions` VALUES ('103', '1', 'products-ajax-index', 'index', 'products');
INSERT INTO `application_users_permissions` VALUES ('104', '1', 'products-productgroup-edit', 'edit', 'products');
INSERT INTO `application_users_permissions` VALUES ('105', '1', 'products-productgroup-add', 'add', 'products');
INSERT INTO `application_users_permissions` VALUES ('106', '1', 'products-productgroup-list', 'list', 'products');
INSERT INTO `application_users_permissions` VALUES ('107', '1', 'products-productgroup-delete', 'delete', 'products');
INSERT INTO `application_users_permissions` VALUES ('108', '1', 'products-productgroup-index', 'index', 'products');
