<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
use Zend\Session\Storage\SessionArrayStorage;
use Zend\Session\Validator\RemoteAddr;
use Zend\Session\Validator\HttpUserAgent;

return [
    
    'db' => [
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=patrino;host=localhost',
        'username' => 'root',
        'password' => '',
        'driver_options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8"
        ]
    ],
    
    // Session configuration.
    'session_config' => [
        // Session cookie will expire in 1 hour.
        'cookie_lifetime' => 60 * 60 * 1,
        
        // Session data will be stored on server maximum for 30 days.
        'gc_maxlifetime' => 60 * 60 * 24 * 30
    ],
    'base_route' => $_SERVER['DOCUMENT_ROOT'],
    // Session storage configuration.
    'session_storage' => [
        'type' => SessionArrayStorage::class
    ],
    
    'content_types' => [
        'article' => 1,
        'question' => 2,
        'podcast' => 3
    ],
    
    'zfcItemsPerPage' => 50,
    
    'groups_config' => [
        'programmer' => 1,
        'supperadmin' => 2,
        'shopkeeper' => 3,
        'customer' => 4,
    ]
];