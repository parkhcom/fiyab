<?php
namespace ZfcDatagrid\Column\Style;

class Padding extends AbstractStyle
{
    /**
     *
     * @var string
     */
    public static $LEFT = '25 px';

    /**
     *
     * @var string
     */
    public static $RIGHT = '25 px';

    /**
     *
     * @var string
     */
    public static $CENTER = '25 px';

    /**
     *
     * @var string
     */
    public static $JUSTIFY = '25 px';

    /**
     *
     * @var string
     */
    protected $padding;

    public function __construct($padding = self::LEFT)
    {
        $this->setPadding($padding);
    }

    /**
     *
     * @param  string $padding
     * @return self
     */
    public function setPadding($padding)
    {
        $this->padding = $padding;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPadding()
    {
        return $this->padding;
    }
}
